module Assembler.Decompiler where

import Control.Monad.Trans.Except
import Control.Monad.Trans.State.Strict
import Control.Monad.Trans.Class

import Trifle.Rename_Except

import Assembler.Links.UntrustedExpression.UntrustedExpression
import Tapeworm.RuntimeStoreOperations
import Assembler.Links.DroppedMobile_to_Fragment.Fragment
import Assembler.Links.DroppedMobile_to_Fragment.RuntimeStore
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor





decompile_object_to_term
  :: Fragment
  -> Either
      Error_Decompile
      UntrustedExpression
decompile_object_to_term (Fragment reentrance store)
  = evalState (runExceptT (decompileReentrance reentrance)) store



data Error_Decompile
  = Error_WhileLookingUpConstrutorPayload
      (Error_Lookup Pointer_To_Children)
  | Error_WhileLookingUpApplicationHeader
      (Error_Lookup Pointer_To_Application)
  | Error_WhileLookingUpLambdaPayload
      (Error_Lookup Pointer_To_LambdaPayload)
  | Error_WhileLookingUpVariableName
      Error_LookingUpSoloVariable
  deriving Show

decompileReentrance
  :: TypedReentranceIntoTheStore
  -> ExceptT
      Error_Decompile
      (State RuntimeStore)
      UntrustedExpression


decompileReentrance (CombHeader Constructor variableName _arity pointer_To_Children)
  = do
  ChildHeaders children
    <- wrapException Error_WhileLookingUpConstrutorPayload
    $ lookupConstructorPayload
    $ pointer_To_Children

  treated_children <- mapM decompileReentrance children

  return $ Comb Constructor variableName treated_children


decompileReentrance (ConstructorLeafHeader variableName)
  = return
  $ Comb Constructor variableName []


decompileReentrance (ApplicationHeader pointer_To_Application)
  = do
  ApplicationPayload
    (TheExpressionToApply f)
    (TheArgument x)
      <- wrapException Error_WhileLookingUpApplicationHeader
      $ lookupApplicationPayload
      $ pointer_To_Application

  f_as_term <- decompileReentrance f
  x_as_term <- decompileReentrance x

  return $ Application f_as_term x_as_term


decompileReentrance (LambdaHeader pointer_To_LambdaPayload)
  = do
  lambdaPayload
    <- wrapException Error_WhileLookingUpLambdaPayload
      $ lookupLambdaPayload
      $ pointer_To_LambdaPayload

  case lambdaPayload of
    TermWithPlaceholderVariable
      boundedSoloVariableOccurence
      (WhereIsTheLambdaBody reentrance)
        -> do
            body_term
              <- decompileReentrance
                    reentrance
            variableName
              <- wrapException Error_WhileLookingUpVariableName
              $ lookupBoundedSoloVariableOccurence_VariableName
                  boundedSoloVariableOccurence
            return $ Lambda variableName body_term
    _ -> error "todo"



decompileReentrance (SoloVariableOccurence variableName _lambda_it_is_bound_to)
  = do
  return $ PlaceholderVariable variableName


decompileReentrance (TwinVariableOccurence _twinCase variableName (Pointer_To_Duplication _) _duplicationLabel)
  = do
  return $ PlaceholderVariable variableName


decompileReentrance (IntrinsicNumberHeader the_number)
  = do
  return $ IntrinsicNumber the_number


decompileReentrance reentrance = do
  store <- lift get
  error $ "decompile case missing: " ++ show (reentrance, store)
