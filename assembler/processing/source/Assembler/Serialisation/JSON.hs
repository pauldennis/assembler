module Assembler.Serialisation.JSON where

import GHC.Generics
import Data.Aeson

import Word.Word24
import Word.Word28

import Assembler.Links.UntrustedExpression.WellKnownName
import Assembler.Links.BitPointerArrayFragment_to_LocalInterchangeableFragment.LocalInterchangeableFragment
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.WellKnownNameLists
import Assembler.Links.DroppedMobile_to_Fragment.IndexOffsetting
import Assembler.Weft.LocalInterchangeableModule
import Assembler.LinkableFragment

import qualified Data.ByteString.Lazy


from_LocalInterchangeableModule_to_JSON
  :: LocalInterchangeableModule
  -> Data.ByteString.Lazy.ByteString
from_LocalInterchangeableModule_to_JSON = encode

putStr_of_LocalInterchangeableModule
  :: LocalInterchangeableModule
  -> IO ()
putStr_of_LocalInterchangeableModule
  linkable_module
  = do
  Data.ByteString.Lazy.putStr $ from_LocalInterchangeableModule_to_JSON linkable_module



------




algebraicDataTypesEncoding :: Options
algebraicDataTypesEncoding
  = defaultOptions
    { sumEncoding = ObjectWithSingleField
    , allNullaryToStringTag = False
    , tagSingleConstructors = True
    }

removeFluffEncoding :: Options
removeFluffEncoding
  = defaultOptions
    { sumEncoding = ObjectWithSingleField
    , allNullaryToStringTag = True
    , tagSingleConstructors = False
    }

data Person = Person String Integer
  deriving (Generic, Show)


instance ToJSON Word24 where
  toJSON word24 = toJSON $ fromEnum word24
instance ToJSON Word28 where
  toJSON word28 = toJSON $ fromEnum word28

instance ToJSON Finger where toEncoding = genericToEncoding removeFluffEncoding

instance ToJSON LocalInterchangeableModule where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON ExternalProvidedRules where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON RootFragment where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON LocalRuleBook where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON RuleDeclaration where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON LocalInterchangeableFragment where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON LocalRule where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON LocalRuleIdentification where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON DebugSymbol where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON RootFinger where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON Melee where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON RuleBranch where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON RuleIdentity where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON WellKnownName where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON PatternMatchSignature where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON OffsetTranslation where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON ConstructorIdentity where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON NumberOfDuplicationNodes where toEncoding = genericToEncoding algebraicDataTypesEncoding
instance ToJSON LinkableFragment where toEncoding = genericToEncoding algebraicDataTypesEncoding

