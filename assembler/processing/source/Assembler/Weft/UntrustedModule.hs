{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DerivingStrategies #-}

module Assembler.Weft.UntrustedModule where


import GHC.Generics

import Numeric.Natural
import Data.Tree
import Data.List

import Trifle.Ramificable
import Data.List.NonEmpty( NonEmpty( (:|) ), toList )
import qualified Data.List.NonEmpty as NonEmptyList


import Assembler.Links.UntrustedExpression.UntrustedExpression
import Assembler.Links.UntrustedExpression.WellKnownName
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.WellKnownNameLists
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.Arity


-- | An instance of such a module can be user provided. The encoded code is not to be trusted. Inorder to safely use such a module a compiler needs to verify the module. A compiler frontend might try to never emit invalid modules as a supplementary ambition.
-- TODO rename to VerifiedModule?
data UntrustedModule
  = UntrustedModule
      RootRule
      Rulebook
  deriving (Show, Read)

instance Ramificable UntrustedModule where
  -- ramify :: a -> Tree String
  ramify = treeShow_module



treeShow_module :: UntrustedModule -> Tree String
treeShow_module (UntrustedModule rootRule rulebook) = result
  where
    result = Node header [body_rootRule, body_rulebook]

    header :: String
    header = "UntrustedModule"

    body_rootRule :: Tree String
    body_rootRule = ramify rootRule

    body_rulebook :: Tree String
    body_rulebook = ramify rulebook



--------


newtype ConstructorName
  = ConstructorName
      WellKnownName
  deriving (Generic, Show, Read, Eq, Ord)
  deriving anyclass Ramificable



data PatternMatch
  = PatternMatch
      ConstructorName
  deriving (Show, Read, Eq, Ord)

data UntrustedRule
  = UntrustedRule
      WellKnownName
      (NonEmptyList.NonEmpty PatternBranch)
  deriving (Show, Read)

instance Ramificable UntrustedRule where
  -- ramify :: a -> Tree String
  ramify (UntrustedRule (WellKnownName wellKnownName) patterns)
    = Node ""
    $ (++ emptyLine)
    $ toList
    $ fmap (ramifyPatternBranch wellKnownName) patterns



data PatternBranch
  = PatternBranch
      PatternMatch
      UntrustedExpression
  deriving (Show, Read)


ramifyPatternBranch :: String -> PatternBranch -> Tree String
ramifyPatternBranch
  rule_name
  (PatternBranch patternMatch b_p_untrustedExpression)
  = Node (rule_name ++ " (" ++ constructorName ++ " "++ put bindings ++ ") "++ put additionalParameters ++" := ")
  $ [ramify untrustedExpression]
  where
    put = intercalate " "


    PatternMatch (ConstructorName (WellKnownName constructorName)) = patternMatch
    arity = lookup_ConstructorArity_of_WellKnownName (WellKnownName constructorName)

    (bindings, p_untrustedExpression) = takeArityManyLambdasApart numberOfMatchedVariables b_p_untrustedExpression
    (additionalParameters, untrustedExpression) = takeConsecutiveManyLambdasApart p_untrustedExpression

    takeArityManyLambdasApart 0 expression = ([], expression)
    takeArityManyLambdasApart n (Lambda (WellKnownName name) expression) = (name : rest_variables, rest_expression)
      where
        (rest_variables, rest_expression) = takeArityManyLambdasApart (n-1) expression

    takeConsecutiveManyLambdasApart (Lambda (WellKnownName name) expression) = (name : rest_variables, rest_expression)
      where
        (rest_variables, rest_expression) = takeConsecutiveManyLambdasApart expression
    takeConsecutiveManyLambdasApart non_lambda = ([], non_lambda)


    -- TODO refactor
    numberOfMatchedVariables :: Natural
    numberOfMatchedVariables
      = case arity of
          Arity_0 -> 0
          Arity_1 -> 1
          Arity_2 -> 2
          Arity_3 -> 3
          Arity_4 -> 4
          Arity_5 -> 5
          Arity_6 -> 6
          Arity_7 -> 7
          Arity_8 -> 8




get_wellKnownName_of_UntrustedRule :: UntrustedRule -> WellKnownName
get_wellKnownName_of_UntrustedRule (UntrustedRule wellKnownName _) = wellKnownName


-- TODO what happens to the constructor identifications that are matched here?
get_expression_of_UntrustedRule :: UntrustedRule -> [UntrustedExpression]
get_expression_of_UntrustedRule (UntrustedRule _ (PatternBranch _ untrustedExpression :| untrustedExpressions))
  = untrustedExpression : map (\(PatternBranch _ x)->x) untrustedExpressions


newtype RootRule
  = RootRule UntrustedExpression
  deriving (Show, Read)

instance Ramificable RootRule where
  ramify (RootRule untrustedExpression) = Node "RootRule" [ramify untrustedExpression]


newtype Rulebook
  = Rulebook
      [UntrustedRule]
  deriving (Show, Read)

instance Ramificable Rulebook where
  -- ramify :: a -> Tree String
  ramify (Rulebook rulebook) = Node "Rulebook" $ map ramify rulebook

