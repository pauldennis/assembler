module Assembler.Links.UntrustedExpression_to_OxygenatedTerm.PointerAddress where

import Numeric.Natural

newtype Address
  = Address Natural
  deriving (Show, Eq)

newtype OuterPointer
  = OuterPointer Address
  deriving (Show, Eq)

newtype InnerPointer
  = InnerPointer Address
  deriving (Show, Eq)

-- TODO should split up in compiler address and type for evaluation in runtime store?
data SubPointer
  = SubPointer OuterPointer InnerPointer
  | LooseBackReference
  deriving (Show, Eq)
