module Assembler.Links.UntrustedExpression_to_OxygenatedTerm.OxygenatedTerm where

import Data.Tree
import Data.Word

import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.IdentificationNumber
import Assembler.Links.UntrustedExpression.WellKnownName
import Trifle.Ramificable
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.PointerAddress
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.Bounding



newtype Intermediate_ToBeDuplicated
  = Intermediate_ToBeDuplicated OxygenatedTerm
  deriving (Show, Eq)

newtype Intermediate_UseDuplicationInThisBody
  = Intermediate_UseDuplicationInThisBody OxygenatedTerm
  deriving (Show, Eq)


data BoundedVariableName
  = BoundedVariableName Bound WellKnownName
  deriving (Show, Eq)

data Occurence
  = Occurence NodeIdentificationNumber InnerPointer
  deriving (Show, Eq)

data WhereIsTheHereBoundVariable
  = UsedVariable Occurence WellKnownName
  | UnusedVariable WellKnownName
  deriving (Show, Eq)

newtype LeftTwinWhereabout
  = LeftTwinWhereabout WhereIsTheHereBoundVariable
  deriving (Show, Eq)

newtype RightTwinWhereabout
  = RightTwinWhereabout WhereIsTheHereBoundVariable
  deriving (Show, Eq)

data OxygenatedTerm
  = Removees_meee_latterr

  | Intermediate_Comb
      IsItARuleOrConstructor
      NodeIdentificationNumber
      WellKnownName
      [OxygenatedTerm]

  | Intermediate_Lambda
      NodeIdentificationNumber
      WhereIsTheHereBoundVariable
      OxygenatedTerm

  | Intermediate_DuplicationIn
      NodeIdentificationNumber
      LeftTwinWhereabout
      RightTwinWhereabout
      Intermediate_ToBeDuplicated
      Intermediate_UseDuplicationInThisBody

  | Intermediate_Application
      NodeIdentificationNumber
      OxygenatedTerm
      OxygenatedTerm

  | Intermediate_IntrinsicNumber
      NodeIdentificationNumber
      Word32

  | Intermediate_IntrinsicOperator
      NodeIdentificationNumber
      OxygenatedTerm
      OxygenatedTerm

  | Intermediate_PlaceholderVariable
      NodeIdentificationNumber
      BoundedVariableName

  deriving (Show, Eq)


instance Ramificable OxygenatedTerm where
  ramify
    (Intermediate_Lambda (NodeIdentificationNumber number) (UsedVariable (Occurence (NodeIdentificationNumber embedder_identification) (InnerPointer (Address birth_number))) (WellKnownName variableName)) subterm)
    = result
    where
      result = Node header [body]

      header :: String
      header = (\x -> "("++show number++") "++ x ++ "("++show embedder_identification++":"++show birth_number++") |->") variableName
      body = ramify subterm


  ramify (Intermediate_Comb Constructor (NodeIdentificationNumber number) (WellKnownName constructorName) children) = result
    where
      result = Node header body

      header
        = "("
        ++ show number
        ++ ") "
        ++ "\""
        ++ constructorName
        ++ "\""
      body = attachment

      attachment = fmap ramify children


  ramify (Intermediate_Comb Rule (NodeIdentificationNumber number) (WellKnownName ruleName) children) = result
    where
      result = Node header body

      header
        = "("
        ++ show number
        ++ ") "
        ++ ruleName
        ++ "()"
      body = attachment

      attachment = fmap ramify children


  ramify (Intermediate_PlaceholderVariable (NodeIdentificationNumber number) (BoundedVariableName (bounding) (WellKnownName variableName))) = result
    where
      result = Node ("("++show number++") " ++ variableName ++ " ~> " ++ show bounding) []


  ramify (Intermediate_IntrinsicOperator (NodeIdentificationNumber number) leftOperand rightOperand) = result
    where
      result = Node ("+ (" ++ show number ++ ")") [left, right]

      left = ramify leftOperand
      right = ramify rightOperand


  ramify (Intermediate_IntrinsicNumber (NodeIdentificationNumber number) n) = Node ("IntrinsicNumber: " ++ show n ++ " ("++show number++")") []

  ramify (Intermediate_Application (NodeIdentificationNumber number) lambda argument ) = Node ("(" ++ show number ++ ") " ++ "Application") [ramify lambda, ramify argument]

  ramify
    (Intermediate_DuplicationIn
      (NodeIdentificationNumber number)
      _LeftTwinWhereabout
      _RightTwinWhereabout
      (Intermediate_ToBeDuplicated intermediate_ToBeDuplicated)
      (Intermediate_UseDuplicationInThisBody intermediate_UseDuplicationInThisBody))
    = Node ("(" ++ show number ++ ") " ++ "Duplication") body
    where
      body = [Node "{TO_BE_DUPLICATED}" [ramify intermediate_ToBeDuplicated], Node "{IN}" [ramify intermediate_UseDuplicationInThisBody]]


  ramify x = error $ "ramify not implemented for: " ++ show x


