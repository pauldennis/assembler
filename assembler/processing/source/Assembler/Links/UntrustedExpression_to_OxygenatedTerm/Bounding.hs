module Assembler.Links.UntrustedExpression_to_OxygenatedTerm.Bounding where

import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.IdentificationNumber
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.TwinCase

data Bound
  = LambdaBound NodeIdentificationNumber
  | DuplicationBound
      TwinCase
      NodeIdentificationNumber
      DuplicationIdentificationNumber
  deriving (Show, Eq)
