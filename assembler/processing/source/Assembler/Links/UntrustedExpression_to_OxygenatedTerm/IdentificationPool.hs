module Assembler.Links.UntrustedExpression_to_OxygenatedTerm.IdentificationPool where

import Control.Monad.Trans.State.Strict

import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.IdentificationNumber


data IdentificationPool
  = IdentificationPool
      [NodeIdentificationNumber]
      [DuplicationIdentificationNumber]


popNodeIdentificationNumber
  :: State IdentificationPool NodeIdentificationNumber
popNodeIdentificationNumber = do
  IdentificationPool
    identification_pool
    duplicationLabels
      <- get

  (next_number, remaining_numbers)
    <- case identification_pool of
          (x:xs)
            -> return (x,xs)
          []
            -> error "ran out of identification numbers"

  put
    $ IdentificationPool
        remaining_numbers
        duplicationLabels

  return next_number



popDuplicationIdentificationNumber
  :: State IdentificationPool DuplicationIdentificationNumber
popDuplicationIdentificationNumber = do
  IdentificationPool
    xs
    duplicationLabels
      <- get

  (next_number, remaining_duplicationLabels)
    <- case duplicationLabels of
          (next:rest)
            -> return (next,rest)
          []
            -> error "ran out of duplication labels"

  put
    $ IdentificationPool
        xs
        remaining_duplicationLabels

  return next_number

