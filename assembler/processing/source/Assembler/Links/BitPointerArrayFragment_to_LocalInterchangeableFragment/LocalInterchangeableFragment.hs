module Assembler.Links.BitPointerArrayFragment_to_LocalInterchangeableFragment.LocalInterchangeableFragment where

import GHC.Generics

import Data.Tree
import Data.Word

import Trifle.Ramificable
import Trifle.BitList

import Assembler.Links.DroppedMobile_to_Fragment.IndexOffsetting


newtype Finger
  = Finger
      Word64
  deriving (Generic, Show)

instance Ramificable Finger where
  ramify (Finger word64) = return $ f $ showFiniteBits word64
    where
      f xs = take 32 xs ++ " " ++ drop 32 xs

newtype RootFinger
  = RootFinger
      Finger
  deriving (Generic, Show)

instance Ramificable RootFinger where
  ramify (RootFinger finger) = Node "{RootFinger}" [ramify finger]

data Melee
  = Melee
      OffsetTranslation
      [Finger]
  deriving (Generic, Show)

instance Ramificable Melee where
  ramify (Melee offset fingers)
    = Node "{Melee}"
    $ ([ramify offset] ++)
    $ map ramify $ fingers

data LocalInterchangeableFragment
  = LocalInterchangeableFragment
      RootFinger
      Melee
  deriving (Generic, Show)

instance Ramificable LocalInterchangeableFragment where
  ramify
    (LocalInterchangeableFragment
      rootFinger
      melee
    )
    = Node "{LocalInterchangeableFragment}"
    $
    [ ramify rootFinger
    , ramify melee
    ]
