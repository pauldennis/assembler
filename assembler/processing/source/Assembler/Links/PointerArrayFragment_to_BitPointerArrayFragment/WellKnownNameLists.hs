module Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.WellKnownNameLists where

import GHC.Stack
import GHC.Generics

import Trifle.Missing
import Trifle.Ramificable

import TermBuildingBlocks.MagicStrings
import TermBuildingBlocks.MagicRules
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor
import Assembler.Links.UntrustedExpression.WellKnownName
import Word.Word24
import Word.Word28
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.CombSignature
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.Arity


lookup_Constructor_identification :: String -> Word24
lookup_Constructor_identification string
  = (\(ConstructorIdentity x) -> x)
  $ lookup_ConstructorIdentity_of_WellKnownName
      (WellKnownName string)



----




newtype ConstructorIdentity
  = ConstructorIdentity Word24
  deriving (Generic, Eq, Ord, Show, Bounded)

instance Ramificable ConstructorIdentity where
  ramify (ConstructorIdentity word24)
    = return $ ("ConstructorIdentity " ++) $ show $ fromEnum $ word24

instance Enum ConstructorIdentity where
  toEnum int = ConstructorIdentity $ toEnum int
  fromEnum (ConstructorIdentity label) = fromEnum label

newtype RuleIdentity
  = RuleIdentity Word28
  deriving (Generic, Eq, Ord, Show, Bounded)

instance Enum RuleIdentity where
  toEnum int = RuleIdentity $ toEnum int
  fromEnum (RuleIdentity label) = fromEnum label

instance Ramificable RuleIdentity where
  ramify x = return $ "RuleIdentity " ++ show (fromEnum x)


-- TODO this function is derived from lookup_ConstructorIdentity_of_WellKnownName
lookup_ConstructorArity_of_WellKnownName
  :: WellKnownName
  -> Arity
lookup_ConstructorArity_of_WellKnownName
  name
  = result
  where
    result
      = interpretResult
      $ lookups name
      $ map remap
      $ listOfConstructorWellKnownNames
      where
        remap
          (_identity, CombSignature Constructor wellKnownName arity)
          = (wellKnownName, arity)

    interpretResult :: [Arity] -> Arity
    interpretResult [x] = x
    interpretResult []
      = error
      $ "\"unsupportet\" Constructor name: "
      ++ (show $ (\(WellKnownName string)->string) name)
      ++ "(TODO 68dca40d64b6fa383a2c388642306346)"
    interpretResult xs = error $ "not supposed to happen: " ++ show xs


-- TODO wrap wellKnownName type with ConstructorName
lookup_ConstructorIdentity_of_WellKnownName
  :: WellKnownName
  -> ConstructorIdentity
lookup_ConstructorIdentity_of_WellKnownName
  name
  = result
  where
    result
      = interpretResult
      $ lookups name
      $ map remap
      $ listOfConstructorWellKnownNames
      where
        remap (identity, CombSignature Constructor wellKnownName _arity)
          = (wellKnownName, identity)

    interpretResult :: [ConstructorIdentity] -> ConstructorIdentity
    interpretResult [x] = x
    interpretResult []
      = error
      $ "\"unsupportet\" Constructor name: "
      ++ (show $ (\(WellKnownName string)->string) name)
      ++ " "
      ++ "(TODO 68dca40d64b6fa383a2c388642306346)"
    interpretResult xs = error $ "not supposed to happen: " ++ show xs




--




lookup_RuleIdentity_of_WellKnownName
  :: HasCallStack
  => WellKnownName
  -> RuleIdentity
lookup_RuleIdentity_of_WellKnownName
  name
  = withFrozenCallStack
  $ result
  where
    result
      = interpretResult
      $ lookups name
      $ map remap
      $ listOfRuleWellKnownNames
      where
        remap
          ( identity
          , CombSignature Rule wellKnownName _arity
          )
          = (wellKnownName, identity)

    interpretResult :: [RuleIdentity] -> RuleIdentity
    interpretResult [x] = x
    interpretResult []
      = error
      $ "cannot find out RuleIdentity: \"unsupportet\" Rule Name: "
      ++ (show $ (\(WellKnownName string)->string) name)
      ++ " "
      ++ "(TODO eb5a51a60f82abdd0cff09ab1f849fd3)"
    interpretResult xs = error $ "not supposed to happen: " ++ show xs


-- TODO this function is derived from lookup_RuleIdentity_of_WellKnownName
lookup_RuleArity_of_Rule
  :: WellKnownName
  -> Arity
lookup_RuleArity_of_Rule
  name
  = result
  where
    result
      = interpretSearchResults
      $ lookups name
      $ map new_mapping
      $ listOfRuleWellKnownNames
      where
        new_mapping
          ( _identity
          , CombSignature Rule wellKnownName arity
          )
          = (wellKnownName, arity)

    interpretSearchResults :: [Arity] -> Arity
    interpretSearchResults [x] = x
    interpretSearchResults []
      = error
      $ "Cannot find out Rule Arity: \"unsupportet\" Rule Name: "
      ++ (show $ (\(WellKnownName string)->string) name)
      ++ " "
      ++ "(TODO eb5a51a60f82abdd0cff09ab1f849fd3)"
    interpretSearchResults xs
      = error
      $ "not supposed to happen: "
      ++ show xs


--




listOfConstructorWellKnownNames
  :: [(ConstructorIdentity, CombSignature)]
listOfConstructorWellKnownNames
  = id
  $ checkThatNothingIsDoubled off
  $ zip [(minBound :: ConstructorIdentity)..]
  $ map (constructSignature Constructor)
  $ arityAndNameList
  where
    off = extract . represant

    extract (CombSignature Constructor (WellKnownName x) _) = x


listOfRuleWellKnownNames
  :: [(RuleIdentity, CombSignature)]
listOfRuleWellKnownNames
  = id
  $ checkThatNothingIsDoubled off
  $ zip [(minBound :: RuleIdentity)..]
  $ map (constructSignature Rule)
  $ []
  ++ arityAndMagicRulesList
  ++ arityAndHardCodedRulesList
  where
    off = extract . represant

    extract (CombSignature Rule (WellKnownName x) _) = x



checkThatNothingIsDoubled
  :: (Ord key, Ord value, Show proof)
  => (EquivalenceClass value -> proof)
  -> [(key, value)]
  -> [(key, value)]
checkThatNothingIsDoubled
  offenderPrinter
  tuples
  = result
  where
    result = case (lefts, rights) of
      ([],[]) -> tuples
      ([],_) -> error $ complainMessage
      _ -> error "not supposed to happen"

    (lefts, rights) = problems tuples

    complainMessage
      = "some things are not assigned unique: "
      ++ show (map offenderPrinter rights)
--          (show $ map  rights)


    problems
      :: (Ord a, Ord b)
      => [(a,b)]
      -> ([EquivalenceClass a], [EquivalenceClass b])
    problems mappings = classifications
      where
        sides whichSide
          = duplicatesOrd
          $ map whichSide
          $ mappings

        classifications = ((sides fst), (sides snd))




getArity :: IsItARuleOrConstructor -> WellKnownName -> Arity
getArity Rule wellKnownName = lookup_RuleArity_of_Rule wellKnownName
getArity Constructor wellKnownName = lookup_ConstructorArity_of_WellKnownName wellKnownName
