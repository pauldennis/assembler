module Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.CombSignature where

import Numeric.Natural

import Assembler.Links.UntrustedExpression.WellKnownName
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.Arity
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor


data CombSignature
  = CombSignature
      IsItARuleOrConstructor
      WellKnownName
      Arity
  deriving (Eq, Ord)

constructSignature
  :: IsItARuleOrConstructor
  -> (Natural, String)
  -> CombSignature
constructSignature
  isItARuleOrConstructor
  (number,string)
  = CombSignature
      isItARuleOrConstructor
      (WellKnownName string)
      (convertToArity number)


