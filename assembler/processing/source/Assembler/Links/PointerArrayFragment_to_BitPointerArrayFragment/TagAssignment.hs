module Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.TagAssignment where

{-
  semantic data types are mapped to low level bit encodings
-}

import Data.Tuple
import Data.List.Extra
import Data.Data
import GHC.Stack
import Safe.Partial

import Trifle.Missing

import Assembler.Links.Fragment_to_PointerArrayFragment.PointerArrayStore
import Word.Word2
import Word.Word4


-- TODO e1134212d0b0f7b2c3952f5151c440fda819ba694389e2d3297b0c208e1cb204 is in sync with this assignmnent. make it depend on this
tag_bits_assign_map
  :: Partial
  => [(Word4, Constr)]
tag_bits_assign_map
  = withFrozenCallStack
  $ tail
  $ map (\(x,y)->(x,toConstr y))
  $ [ (undefined

  ),( Word_0000,
        LeftTwinOccurence __ __
  ),( Word_1000,
        RightTwinOccurence __ __
  ),( Word_0100,
        PointerOfSoloVariableOccurence __ __

  ),( Word_0010,
        PointerToLambdaNode __
  ),( Word_1010,
        PointerSuperposition
  ),( Word_0110,
        PointerToConstructorLeaf __
  ),( Word_0110,
        PointerToConstructor __ __ __

  -- it might be benficial to just inrement a ruleCall pointer by one and gain an OpaqueObject by it. This way the magic EcapsulateRule is quit easy to implement
  -- PointerOpaqueObject = PointerToRuleCall + 1
  ),( Word_0001,
        PointerToRuleCall __ __ __
  ),( Word_1001,
        PointerOpaqueObject

  ),( Word_0101,
        PointerToApplicationNode __
  ),( Word_1011,
        PointerIntrinsicBinaryOperation __
  ),( Word_0111,
        PointerOfIntrinsicNumber __

  ),( Word_0011,
        error "unused bit/tag pattern"

  ),( Word_1100,
        error "Free00"
  ),( Word_1101,
        error "Free10"
  ),( Word_1110,
        error "Free01"
  ),( Word_1111,
        error "Free11"

  ) ]
  where
    __ = undefined



lookup_Tag_to_Pointer
  :: Word4
  -> [Constr]
lookup_Tag_to_Pointer
  key
  = lookups
      key
      tag_bits_assign_map


lookup_Pointer_to_tag
  :: Pointer
  -> Word4
lookup_Pointer_to_tag
  pointer
  = result
  where
    result
      = case looked_up_entries of
          x:_ -> x
          xs -> error $ "lookup not considered: " ++ show xs


    key = toConstr pointer

    looked_up_entries = lookups key lookupTable

    lookupTable
      = id
      $ map swap
      $ tag_bits_assign_map





test_tag_assignment_consistency :: (String, Bool)
test_tag_assignment_consistency
  = (,) "test_tag_assignment_consistency"
  $ result
  where
    result = True
      && all_bits_assigned_at_least_once
      && no_unforsee_double_assignment

    all_bits_assigned_at_least_once
      = id
      $ (number_of_all_word4s==)
      $ length
      $ nub
      $ sort
      $ map fst
      $ tag_bits_assign_map

    number_of_all_word4s
      = genericLength
      $ all_word4s


    no_unforsee_double_assignment
      = (number_of_all_word4s==)
      $ length
      $ map allow_constructors_to_appear_twice
      $ map lookup_Tag_to_Pointer all_word4s
      where
        allow_constructors_to_appear_twice [x] = x
        allow_constructors_to_appear_twice [x,y]
          = if x == toConstr (PointerToConstructorLeaf undefined)
               && y == toConstr (PointerToConstructor undefined undefined undefined)
               then toConstr (PointerToConstructorLeaf undefined)
               else error "unforseen case"
        allow_constructors_to_appear_twice _ = undefined





lookup_Pointer_to_Word2
  :: Pointer
  -> Word2

lookup_Pointer_to_Word2
  (VariableBounding _ _)
  = Word2_0

lookup_Pointer_to_Word2
  pointer
  = error $ show "unforseen case: " ++ show pointer
