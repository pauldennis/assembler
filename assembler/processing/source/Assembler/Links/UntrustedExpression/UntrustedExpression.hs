module Assembler.Links.UntrustedExpression.UntrustedExpression where

import Data.Tree

import Trifle.Ramificable
import Assembler.Links.UntrustedExpression.WellKnownName
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor
import Data.Word


newtype ToBeDuplicated = ToBeDuplicated UntrustedExpression
  deriving (Show, Eq, Read)


-- TODO find better name?
data UntrustedExpression
  = REMOVE_ME_LATEr --TODO remove me

  | Comb
      IsItARuleOrConstructor
      WellKnownName
      [UntrustedExpression]

  | Lambda
      WellKnownName
      UntrustedExpression

  | PlaceholderVariable
      WellKnownName

  | Application
      UntrustedExpression
      UntrustedExpression

  | DuplicationIn
      WellKnownName
      WellKnownName
      ToBeDuplicated
      UntrustedExpression

  | IntrinsicNumber
      Word32

  | IntrinsicOperator
      UntrustedExpression
      UntrustedExpression


  deriving (Show, Eq, Read)


instance Ramificable UntrustedExpression where
  ramify = treeShow_term

-- TODO is there some deriving strategy for this?
-- TODO is this like some kind of antipattern?
skipVerification
  :: (UntrustedExpression -> UntrustedExpression)
  -> (UntrustedExpression -> UntrustedExpression)
skipVerification verify (Comb x y recursion) = (Comb x y (map verify recursion))
skipVerification verify (Lambda x recursion) = (Lambda x $ verify recursion)
skipVerification _      (PlaceholderVariable x) = (PlaceholderVariable x)
skipVerification verify (Application left right) = (Application (verify left) (verify right))
skipVerification verify (DuplicationIn x y (ToBeDuplicated left) right) = (DuplicationIn x y (ToBeDuplicated (verify left)) (verify right))
skipVerification _      (IntrinsicNumber x) = (IntrinsicNumber x)
skipVerification verify (IntrinsicOperator left right) = (IntrinsicOperator (verify left) (verify right))




treeShow_term :: UntrustedExpression -> Tree String
treeShow_term (Lambda (WellKnownName variableName) subterm) = result
  where
    result = Node header [body]

    header :: String
    header = (\x -> x ++ " |->") variableName
    body = ramify subterm


treeShow_term (Comb Constructor (WellKnownName variableName) children) = result
  where
    result = Node header body

    header = variableName
    body = attachment

    attachment = fmap treeShow_term children


treeShow_term (PlaceholderVariable (WellKnownName variableName)) = result
  where
    result = Node variableName []


treeShow_term (DuplicationIn (WellKnownName leftTwin) (WellKnownName rightTwin) (ToBeDuplicated replacement) body_term) = result
  where
    result = Node header body

    header = (\x y -> "DUPLICATE " ++ show (x,y) ++ " :=") leftTwin rightTwin

    body = [replacement_body, Node "IN" [], body_tree]

    replacement_body = treeShow_term replacement
    body_tree = treeShow_term body_term


treeShow_term (IntrinsicOperator leftOperand rightOperand) = result
  where
    result = Node "+" [left, right]

    left = treeShow_term leftOperand
    right = treeShow_term rightOperand


treeShow_term (IntrinsicNumber n) = Node (show n) []


treeShow_term (Application lambda argument) = Node "APPLICATION" [treeShow_term lambda, treeShow_term argument]


treeShow_term (Comb Rule (WellKnownName ruleName) parameters)
  = Node head_decorator
  $ map treeShow_term parameters
  where
    head_decorator = ruleName ++ "()"

treeShow_term x = error $ "treeShow_term not implemented for: " ++ show x



printLines :: [String] -> IO ()
printLines = mapM_ putStrLn
