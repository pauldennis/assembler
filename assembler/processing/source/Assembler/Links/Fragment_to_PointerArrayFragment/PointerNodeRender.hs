module Assembler.Links.Fragment_to_PointerArrayFragment.PointerNodeRender where

import Data.List


import Assembler.Links.DroppedMobile_to_Fragment.Fragment
import Assembler.Links.DroppedMobile_to_Fragment.RuntimeStore
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.PointerAddress
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.TwinCase
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor
import Assembler.Links.Fragment_to_PointerArrayFragment.PointerArrayStore
import Assembler.Links.Fragment_to_PointerArrayFragment.RuntimeLayout

render_pointer_adresses
  :: Fragment
  -> PointerArrayFragment
render_pointer_adresses
  (Fragment
    typedReentranceIntoTheStore
    (RuntimeStore offset storeEntries)
  )
  = PointerArrayFragment rootPointer pointerNodes
  where
    rootPointer
      = RootPointer
      $ render_reentrance_to_pointer
      $ typedReentranceIntoTheStore

    pointerNodes
      = PointerNodes offset
      $ map render_entry storeEntries



---




render_reentrance_to_pointer
  :: TypedReentranceIntoTheStore
  -> Pointer


render_reentrance_to_pointer
  (LambdaHeader (Pointer_To_LambdaPayload outerPointer))
  = PointerToLambdaNode newAddress
  where
    newAddress = equidistant_layout_node_address outerPointer


render_reentrance_to_pointer
  (CombHeader
    Constructor
    wellKnownName
    arity
    (Pointer_To_Children pointer_To_Children)
  )
  = PointerToConstructor wellKnownName arity
  $ equidistant_layout_node_address
  $ pointer_To_Children


render_reentrance_to_pointer
  (CombHeader
    Rule
    wellKnownName
    arity
    (Pointer_To_Children pointer_To_Children)
  )
  = PointerToRuleCall wellKnownName arity
  $ equidistant_layout_node_address
  $ pointer_To_Children


render_reentrance_to_pointer
  (TwinVariableOccurence
    twinCase
    variableName
    (Pointer_To_Duplication pointer_To_Duplication)
    (Label_of_Duplication duplicationIdentity)
  )
  = case twinCase of
      LeftTwin
        -> LeftTwinOccurence variableName whereIsMyDuplicatinNode
      RightTwin
        -> RightTwinOccurence variableName whereIsMyDuplicatinNode
      where
        whereIsMyDuplicatinNode
          = MyDuplicatinNodeInfo address duplicationIdentity

        address
          = equidistant_layout_node_address
          $ pointer_To_Duplication


render_reentrance_to_pointer
  (SoloVariableOccurence
    variableName
    (WhereIsTheVariableBound
      (Pointer_To_LambdaPayload
        outerPointer
      )
    )
  )
  = result
  where
    result
      = PointerOfSoloVariableOccurence variableName
      $ WhereIsMyLambdaThatBoundsMe
      $ equidistant_layout_node_address
      $ outerPointer


render_reentrance_to_pointer
  (ConstructorLeafHeader wellKnownName)
  = result
  where
    result
      = PointerToConstructorLeaf wellKnownName



render_reentrance_to_pointer
  (ApplicationHeader (Pointer_To_Application pointer_To_Application))
  = result
  where
    result
      = PointerToApplicationNode
      $ equidistant_layout_node_address
      $ pointer_To_Application



render_reentrance_to_pointer
  (IntrinsicNumberHeader word60)
  = result
  where
    result
      = PointerOfIntrinsicNumber word60

render_reentrance_to_pointer
  (IntrinsicOperationHeader
    (Pointer_To_IntrinsicOperation pointer_to_ArgumentBundle)
  )
  = result
  where
    result
      = PointerIntrinsicBinaryOperation
      $ equidistant_layout_node_address
      $ pointer_to_ArgumentBundle

render_reentrance_to_pointer
  reentrance
  = error
  $ "render_reentrance_to_pointer not implemented for: "
  ++ show reentrance



--




equidistant_layout_node_address
  :: OuterPointer
  -> PointerFingerAddress
equidistant_layout_node_address
  (OuterPointer (Address number))
  = PointerFingerAddress
  $ number
  * number_of_fingers_within_node

offset_with_InnerPointer
  :: PointerFingerAddress
  -> InnerPointer
  -> PointerFingerAddress
offset_with_InnerPointer
  (PointerFingerAddress node_position)
  (InnerPointer (Address subpointer_number))
  = PointerFingerAddress
  $ node_position
  + subpointer_number


equidistant_layout_node_subpointer_address
  :: SubPointer
  -> PointerFingerAddress
equidistant_layout_node_subpointer_address
  (SubPointer outerPointer innerPointer)
  = offset_with_InnerPointer
      (equidistant_layout_node_address outerPointer)
      innerPointer


---




render_entry
  :: SclerotizedEntry
  -> PointerNode


render_entry
  (LambdaEntry
    (TermWithPlaceholderVariable
      (WhereIsMyBoundedSoloVariable subpointer variableName)
      (WhereIsTheLambdaBody whereIsTheLambdaBody)
    )
  )
  = result
  where
    result
      = PointerNode DebugNodeType_Lambda
      $ fillupWithFreeSpace
      [ pointerSoloVariableOccurence
      , pointerToTheLambdaBody
      ]

    pointerSoloVariableOccurence :: Pointer
    pointerSoloVariableOccurence
      = VariableBounding variableName
      $ WhereIsTheVariableUsed
      $ address_of_solo_variable

    address_of_solo_variable
      = equidistant_layout_node_subpointer_address
      $ subpointer

    pointerToTheLambdaBody :: Pointer
    pointerToTheLambdaBody
      = render_reentrance_to_pointer
      $ whereIsTheLambdaBody




render_entry
  (DuplicationEntry
    (DuplicationPayload
      (Debug_TwinNames leftVariableName rightVariableName)
      usageOfVariables
      (ExpressionToBeDuplicated expressionToBeDuplicated)
    )
  )
  = result
  where
    result
      = PointerNode DebugNodeType_Duplication
      $ fillupWithFreeSpace
      [ leftLeash
      , rightLeash
      , toBeDuplicated
      ]

    (leftLeash, rightLeash, toBeDuplicated)
      = case usageOfVariables of
          BothUsed
            leftSubPointer
            rightSubPointer
              ->
              ( VariableBounding leftVariableName left
              , VariableBounding rightVariableName right
              , render_reentrance_to_pointer expressionToBeDuplicated
              )
              where
                left
                  = WhereIsTheVariableUsed
                  $ equidistant_layout_node_subpointer_address
                  $ leftSubPointer
                right
                  = WhereIsTheVariableUsed
                  $ equidistant_layout_node_subpointer_address
                  $ rightSubPointer




render_entry
  (CombEntry Constructor _wellKnownName (ChildHeaders pointers))
  = result
  where
    result
      = PointerNode DebugNodeType_Constructor
      $ fillupWithFreeSpace
      $ map render_reentrance_to_pointer
      $ pointers


render_entry
  (CombEntry Rule _wellKnownName (ChildHeaders pointers))
  = result
  where
    result
      = PointerNode DebugNodeType_Rule
      $ fillupWithFreeSpace
      $ map render_reentrance_to_pointer
      $ pointers


render_entry
  (ApplicationEntry
    (ApplicationPayload
      (TheExpressionToApply theExpressionToApply)
      (TheArgument theArgument)
    )
  )
  = result
  where
    result
      = PointerNode DebugNodeType_Application
      $ fillupWithFreeSpace
      $ map render_reentrance_to_pointer
      $ [theExpressionToApply, theArgument]


render_entry
  (IntrinsicOperationEntry
    leftArgument
    rightArgument
  )
  = result
  where
    result
      = PointerNode DebugNodeType_IntrinsicOperatorArgumentBundle
      $ fillupWithFreeSpace
      $ map render_reentrance_to_pointer
      $ [leftArgument, rightArgument]



render_entry
  storeEntry
  = error
  $ "render_entry not implemented for: "
  ++ show storeEntry






fillupWithFreeSpace
  :: [Pointer]
  -> [Pointer]
fillupWithFreeSpace
  pointers
  = if genericLength pointers <= number_of_fingers_within_node
      then genericTake number_of_fingers_within_node
        $ pointers
        ++ (repeat Pointer_UnusedSpace)
      else error $ "unforseen number of pointers in one node, expected less or equal than: " ++ show number_of_fingers_within_node


lazytranslateLetters :: String
lazytranslateLetters = "eariodnslgubmhfw"
