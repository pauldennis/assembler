{-# LANGUAGE DeriveDataTypeable #-}

module Assembler.Links.Fragment_to_PointerArrayFragment.PointerArrayStore where

import Numeric.Natural
import Data.Tree
import Data.List
import Data.Data
import Data.Word

import Trifle.Ramificable
import Assembler.Links.UntrustedExpression.WellKnownName
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.IdentificationNumber
import Assembler.Links.DroppedMobile_to_Fragment.IndexOffsetting
import Assembler.Links.Fragment_to_PointerArrayFragment.RuntimeLayout

import Assembler.Links.DroppedMobile_to_Fragment.RuntimeStore


newtype RootPointer
  = RootPointer Pointer
  deriving Show

instance Ramificable RootPointer where
  ramify (RootPointer rootPointer)
    = Node "{RootPointer}"
    $ [ramify rootPointer]


data DebugNodeType
  = DebugNodeType_Lambda
  | DebugNodeType_Application
  | DebugNodeType_Duplication
  | DebugNodeType_Constructor
  | DebugNodeType_Rule
  | DebugNodeType_IntrinsicOperatorArgumentBundle
  deriving Show

show_node_type :: DebugNodeType -> String
show_node_type DebugNodeType_Lambda = "LAMBDA NODE"
show_node_type DebugNodeType_Application = "APPLICATION NODE"
show_node_type DebugNodeType_Duplication = "FLOATING DUPLICATION NODE"
show_node_type DebugNodeType_Constructor = "CONSTRUCTOR NODE (Only Children)"
show_node_type DebugNodeType_Rule = "RULE"
show_node_type DebugNodeType_IntrinsicOperatorArgumentBundle = "INTRINSIC OPERATOR ARGUMENT BUNDLE"


data PointerNode
  = PointerNode
      DebugNodeType
      [Pointer]
  deriving Show

instance Ramificable PointerNode where
  ramify (PointerNode node_type pointer)
    = Node (show_node_type node_type)
    $ map ramify
    $ pointer

data PointerNodes
  = PointerNodes
      OffsetTranslation
      [PointerNode]
  deriving Show

instance Ramificable PointerNodes where
  ramify (PointerNodes offset pointerNodes)
    = Node "{PointerNodes}"
    $ ([ramify offset] ++)
    $ add_line_numbers_to_groups first_address
    $ map ramify
    $ pointerNodes
    where
      -- TODO use state monad!!!

      OffsetTranslation first_node_index = offset

      first_address
        = number_of_fingers_within_node
        * first_node_index

      add_line_numbers_to_groups
        :: Natural
        -> [Tree String]
        -> [Tree String]
      add_line_numbers_to_groups _ [] = []
      add_line_numbers_to_groups
        next_line_number
        (Node groupname pointers : rest_nodes)
        = Node groupname result : rest_results
        where
          (sub_next_number, result)
            = add_line_numbers_to_pointers next_line_number
            $ pointers

          rest_results
            = add_line_numbers_to_groups sub_next_number
            $ rest_nodes

          add_line_numbers_to_pointers
            :: Natural
            -> [Tree String]
            -> (Natural, [Tree String])
          add_line_numbers_to_pointers
            next_number
            []
            = (next_number, [])
          add_line_numbers_to_pointers
            next_number
            (x:xs)
            = (last_next_number, first_result ++ rest_result)
            where
              (last_next_number, rest_result)
                = add_line_numbers_to_pointers (next_number + 1)
                $ xs

              first_result = add_number_to_root x

              add_number_to_root (Node title restchildren)
                = if is_to_be_hidden
                    then []
                    else [Node (show_number next_number ++ " | " ++ title) restchildren]
                where
                  -- TODO fragile hack!!!
                  is_to_be_hidden = isInfixOf (show Pointer_UnusedSpace) title

                  show_number n
                    = if (length showed <= leading_zeros_length)
                        then reverse $ take leading_zeros_length $ (++ (repeat '0')) $ reverse showed
                        else showed
                    where
                      leading_zeros_length = 3
                      showed = show n






data PointerArrayFragment
  = PointerArrayFragment
      RootPointer
      PointerNodes
  deriving Show


instance Ramificable PointerArrayFragment where
  ramify (PointerArrayFragment rootPointer pointerNodes)
    = Node "{PointerArrayFragment}" [ramify rootPointer, ramify pointerNodes]


----



data PointerFingerAddress
  = PointerFingerAddress Natural
  deriving (Show, Typeable, Data)


newtype WhereIsTheVariableUsed
  = WhereIsTheVariableUsed PointerFingerAddress
  deriving (Show, Typeable, Data)

newtype WhereIsMyLambdaThatBoundsMe
  = WhereIsMyLambdaThatBoundsMe PointerFingerAddress
  deriving (Show, Typeable, Data)

data MyDuplicatinNodeInfo
  = MyDuplicatinNodeInfo
      PointerFingerAddress
      DuplicationIdentificationNumber
  deriving (Show, Typeable, Data)


data Pointer
  = Pointer_UnusedSpace

  | LeftTwinOccurence
      WellKnownName
      MyDuplicatinNodeInfo

  | RightTwinOccurence
      WellKnownName
      MyDuplicatinNodeInfo

  | PointerOfSoloVariableOccurence
      WellKnownName
      WhereIsMyLambdaThatBoundsMe

  -- TODO rename to VariableLeash
  | VariableBounding
      WellKnownName
      WhereIsTheVariableUsed

  | PointerTheHereBoundVariableIsNotUsedAnywhere

  | PointerToLambdaNode
      PointerFingerAddress

  | PointerOpaqueObject -- TODO find marketing name an replace this here

  | PointerToApplicationNode
      PointerFingerAddress

  | PointerSuperposition

  | PointerToConstructorLeaf
      WellKnownName

  | PointerToConstructor
      WellKnownName
      ActualUtilizedArity
      PointerFingerAddress

  | PointerToRuleCall
      WellKnownName
      ActualUtilizedArity
      PointerFingerAddress

  | PointerOfIntrinsicNumber
      Word32

  | PointerIntrinsicBinaryOperation
      PointerFingerAddress

  deriving (Show, Typeable, Data)

instance Ramificable Pointer where
  ramify (PointerToLambdaNode (PointerFingerAddress number))
    = return $ "Lambda @" ++ show number

  ramify
    (PointerOfSoloVariableOccurence
      (WellKnownName variableName)
      (WhereIsMyLambdaThatBoundsMe (PointerFingerAddress number))
    )
    = return $ "SoloVariableOccurence "++(show variableName)++" @" ++ show number

  ramify
    (VariableBounding
      (WellKnownName variableName)
      (WhereIsTheVariableUsed (PointerFingerAddress number))
    )
    = return $ "VariableBounding "++(show variableName)++" @" ++ show number

  ramify
    (Pointer_UnusedSpace
    )
    = return $ "Pointer_UnusedSpace"

  ramify
    (PointerToConstructor
      (WellKnownName name)
      (ActualUtilizedArity _)
      (PointerFingerAddress address)
    )
    = return
    $ "PointerToConstructor "
    ++ show name
    ++ " @"
    ++ show address


  ramify
    (LeftTwinOccurence
      (WellKnownName name)
      (MyDuplicatinNodeInfo (PointerFingerAddress address) (DuplicationIdentificationNumber duplicationId))
    )
    = return $ "LeftTwinOccurence " ++ show (("variableName:", name), ("bound@", address), ("duplicationID:", duplicationId))


  ramify
    (RightTwinOccurence
      (WellKnownName name)
      (MyDuplicatinNodeInfo (PointerFingerAddress address) (DuplicationIdentificationNumber duplicationId))
    )
    = return $ "RightTwinOccurence " ++ show (("variableName:", name), ("bound@", address), ("duplicationID:", duplicationId)) --TODO duplicated from LeftTwinOccurence


  ramify
    (PointerToRuleCall
      (WellKnownName name)
      (ActualUtilizedArity _)
      (PointerFingerAddress address)
    )
    = return
    $ "PointerToRuleCall "
    ++ show name
    ++ " @"
    ++ show address


  ramify
    (PointerToConstructorLeaf (WellKnownName name)
    )
    = return $ "PointerToConstructorLeaf " ++ show name



  ramify
    (PointerToApplicationNode
      (PointerFingerAddress address)
    )
    = return $ "PointerToApplicationNode @" ++ show address



  ramify
    (PointerOfIntrinsicNumber number
    )
    = return $ "PointerIntrinsicNumber " ++ show number



  ramify
    (PointerIntrinsicBinaryOperation address
    )
    = return $ "PointerIntrinsicBinaryOperation @" ++ show address



  ramify unknown
    = error
    $ "ramify not implemented for: "
    ++ show unknown
