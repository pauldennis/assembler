module Assembler.Links.Fragment_to_PointerArrayFragment.RuntimeLayout where

import Numeric.Natural

-- TODO find other occurences of the same thing

number_of_bytes_depicting_finger :: Natural
number_of_bytes_depicting_finger = 8

-- TODO nodes could be packed more dense. Would that make sense?
number_of_fingers_within_node :: Natural
number_of_fingers_within_node = 8

node_length_in_bytes :: Natural
node_length_in_bytes
  = number_of_fingers_within_node
  * number_of_bytes_depicting_finger

