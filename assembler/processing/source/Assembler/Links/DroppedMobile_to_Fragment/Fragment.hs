module Assembler.Links.DroppedMobile_to_Fragment.Fragment where

import Data.Tree

import Assembler.Links.DroppedMobile_to_Fragment.RuntimeStore
import Trifle.Ramificable


-- TODO what is this? The word Fragment might have become ambigous
data Fragment
  = Fragment TypedReentranceIntoTheStore RuntimeStore
  deriving (Show, Eq)

instance Ramificable Fragment where
  ramify (Fragment root store) = Node "{Fragment}" [Node "{Root}" [ramify root], ramify store]
