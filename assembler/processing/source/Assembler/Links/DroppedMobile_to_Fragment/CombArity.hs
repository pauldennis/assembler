{-# LANGUAGE DeriveDataTypeable #-}

module Assembler.Links.DroppedMobile_to_Fragment.CombArity where

import Numeric.Natural
import Data.Data


data CombArity
  = CombArity_1
  | CombArity_2
  | CombArity_3
  | CombArity_4
  | CombArity_5
  | CombArity_6
  | CombArity_7
  | CombArity_8
  deriving (Show, Eq, Ord, Data)

convertToCombArity
  :: Natural
  -> CombArity

convertToCombArity 1 = CombArity_1
convertToCombArity 2 = CombArity_2
convertToCombArity 3 = CombArity_3
convertToCombArity 4 = CombArity_4
convertToCombArity 5 = CombArity_5
convertToCombArity 6 = CombArity_6
convertToCombArity 7 = CombArity_7
convertToCombArity 8 = CombArity_8

convertToCombArity 0
  = error
  $ "arity cannot be zero"

convertToCombArity arity
  = error
  $ "arity cannot be more than 8 but is: "
  ++ show arity


getArityAsNumber
  :: CombArity
  -> Int
getArityAsNumber CombArity_1 = 1
getArityAsNumber CombArity_2 = 2
getArityAsNumber CombArity_3 = 3
getArityAsNumber CombArity_4 = 4
getArityAsNumber CombArity_5 = 5
getArityAsNumber CombArity_6 = 6
getArityAsNumber CombArity_7 = 7
getArityAsNumber CombArity_8 = 8
