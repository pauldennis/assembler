{-# LANGUAGE DeriveDataTypeable #-}

module Assembler.Links.DroppedMobile_to_Fragment.RuntimeStore where

import Numeric.Natural
import Data.Tree
import Data.Data
import Data.Word


import Trifle.Ramificable
import Assembler.Links.UntrustedExpression.WellKnownName
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.PointerAddress
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.TwinCase
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.IdentificationNumber
import Assembler.Links.DroppedMobile_to_Fragment.IndexOffsetting

import Assembler.Links.DroppedMobile_to_Fragment.CombArity



----




newtype DuplicationLabel
  = DuplicationLabel WellKnownName
  --TODO what is this?
  deriving (Show, Eq)

newtype Pointer_To_Duplication
  = Pointer_To_Duplication OuterPointer
  deriving (Show, Eq)

newtype Label_of_Duplication
  = Label_of_Duplication DuplicationIdentificationNumber
  deriving (Show, Eq)

newtype Pointer_To_Children
  = Pointer_To_Children OuterPointer
  deriving (Show, Eq)

newtype Pointer_To_LambdaPayload --TODO remove Payload?
  = Pointer_To_LambdaPayload OuterPointer
  deriving (Show, Eq)

newtype Pointer_To_Application
  = Pointer_To_Application OuterPointer
  deriving (Show, Eq)

newtype Pointer_To_IntrinsicOperation
  = Pointer_To_IntrinsicOperation OuterPointer
  deriving (Show, Eq)

newtype Pointer_To_Superposition
  = Pointer_To_Superposition OuterPointer
  deriving (Show, Eq)

-- TODO remove this one wrapping?
newtype WhereIsTheVariableBound
  = WhereIsTheVariableBound Pointer_To_LambdaPayload
  deriving (Show, Eq)

newtype ActualUtilizedArity
  = ActualUtilizedArity CombArity
  deriving (Show, Eq, Data)

data TypedReentranceIntoTheStore
  = Deletede_me_lattter -- TODO

  | ConstructorLeafHeader
      WellKnownName

  | CombHeader
      IsItARuleOrConstructor
      WellKnownName
      ActualUtilizedArity
      Pointer_To_Children

  | LambdaHeader
      Pointer_To_LambdaPayload

  | TwinVariableOccurence
      TwinCase
      WellKnownName
      Pointer_To_Duplication
      Label_of_Duplication

  | SoloVariableOccurence
      WellKnownName
      WhereIsTheVariableBound

  | ApplicationHeader
      Pointer_To_Application

  | IntrinsicOperationHeader
      Pointer_To_IntrinsicOperation

  | IntrinsicNumberHeader
      Word32
      -- here can the unboxed phenomenon be observed

  | SuperpositionHeader
      Pointer_To_Superposition

  -- NOTE: there is no such thing as a DuplicationHeader

  deriving (Show, Eq)



instance Ramificable TypedReentranceIntoTheStore where

  ramify (ConstructorLeafHeader (WellKnownName variableName))
    = Node (show variableName) []

  ramify
    (TwinVariableOccurence
      twinCase
      (WellKnownName name)
      (Pointer_To_Duplication (OuterPointer (Address number)))
      (Label_of_Duplication (DuplicationIdentificationNumber duplicationIdentity))
    )
    = Node ("twin: "++ name ++ " -> " ++ show number ++ ":" ++ show twinCase ++ show (DuplicationIdentificationNumber duplicationIdentity)) []

  ramify (ApplicationHeader (Pointer_To_Application (OuterPointer (Address number))))
    = Node ("application @" ++ show number) []

  ramify (LambdaHeader (Pointer_To_LambdaPayload (OuterPointer (Address number))))
    = Node ("lambda @" ++ show number) []

  ramify (SoloVariableOccurence (WellKnownName variableName) (WhereIsTheVariableBound (Pointer_To_LambdaPayload (OuterPointer (Address address)))))
    = Node (variableName ++ " -λ> " ++ show address) []

  ramify (CombHeader Constructor (WellKnownName variableName) (ActualUtilizedArity _combArity) (Pointer_To_Children (OuterPointer (Address number))))
    = Node (variableName ++ " @" ++ show number) []
    -- TODO should print _combArity?

  ramify (CombHeader Rule (WellKnownName variableName) (ActualUtilizedArity _combArity) (Pointer_To_Children (OuterPointer (Address number))))
    = Node (variableName ++ "() @" ++ show number) []

  ramify (IntrinsicOperationHeader (Pointer_To_IntrinsicOperation (OuterPointer (Address number))))
    = Node ("("++ ['+'] ++") @" ++ show number) []

  ramify (IntrinsicNumberHeader the_number)
    = Node ("IntrinsicNumberHeader: " ++ show the_number) []

  ramify (SuperpositionHeader (Pointer_To_Superposition (OuterPointer (Address number))))
    = Node ("Superposition @" ++ show number) []

  ramify x = error $ "no ramify for: " ++ show x



-----



newtype WhereIsTheLambdaBody
  = WhereIsTheLambdaBody TypedReentranceIntoTheStore
  deriving (Show, Eq)

newtype ExpressionToBeDuplicated
  = ExpressionToBeDuplicated TypedReentranceIntoTheStore
  deriving (Show, Eq)

data UsageOfVariables
  = BothUsed SubPointer SubPointer
  | OnlyLeft_IsUsed SubPointer
  | OnlyRightIsUsed SubPointer
  | NowhereUsed
  deriving (Show, Eq)

newtype TheExpressionToApply
  = TheExpressionToApply TypedReentranceIntoTheStore
  deriving (Show, Eq)

newtype TheArgument
  = TheArgument TypedReentranceIntoTheStore
  deriving (Show, Eq)

data ApplicationPayload
  = ApplicationPayload
      TheExpressionToApply
      TheArgument
  deriving (Show, Eq)

data DuplicationPayload
  = DuplicationPayload
      Debug_TwinNames
      UsageOfVariables
      ExpressionToBeDuplicated
  deriving (Show, Eq)

data WhereIsMyBoundedSoloVariable
  = WhereIsMyBoundedSoloVariable SubPointer WellKnownName
  deriving (Show, Eq)

data LambdaPayload
  = ConstantLambdaPayload TypedReentranceIntoTheStore
  | TermWithPlaceholderVariable
      WhereIsMyBoundedSoloVariable
      WhereIsTheLambdaBody
  deriving (Show, Eq)

data Debug_TwinNames
  = Debug_TwinNames WellKnownName WellKnownName
  deriving (Show, Eq)

newtype ChildHeaders
  = ChildHeaders [TypedReentranceIntoTheStore]
  deriving (Show, Eq)

data SclerotizedEntry
  = EmptyEntryBecauseGotDeleted

  | FreshlyAllocatedEntry --TODO actually used?

  | CombEntry
      IsItARuleOrConstructor
      WellKnownName
      ChildHeaders

  | LambdaEntry
      LambdaPayload

  | ApplicationEntry
      ApplicationPayload

  | SuperpositionEntry
      TypedReentranceIntoTheStore
      TypedReentranceIntoTheStore --TODO payload?

  | FunctionEntry
      [TypedReentranceIntoTheStore]
  -- Label is saved in the TypedReentranceIntoTheStore pointer payload

  | IntrinsicOperationEntry
      TypedReentranceIntoTheStore
      TypedReentranceIntoTheStore

  | DuplicationEntry
      DuplicationPayload

  deriving (Show, Eq)







data RuntimeStore
  = RuntimeStore
      OffsetTranslation
      [SclerotizedEntry]
  deriving (Show, Eq)



instance Ramificable RuntimeStore where
  ramify (RuntimeStore offset store) = Node header body
    where
      header = "{RuntimeStore}"

      OffsetTranslation first_index = offset

      body = [ramify offset] ++ entries

      entries
        = id
        $ map (uncurry addItemNumber)
        $ map (\(x,y) ->(x,ramify y))
        $ filter (\(_,y) -> y /= EmptyEntryBecauseGotDeleted)
        $ zip [first_index..]
        $ store

      addItemNumber :: Natural -> Tree String -> Tree String
      addItemNumber number (Node label children) = Node (show number ++ "." ++ label) children



instance Ramificable SclerotizedEntry where
  ramify EmptyEntryBecauseGotDeleted = Node (show EmptyEntryBecauseGotDeleted) []

  ramify FreshlyAllocatedEntry = Node (show FreshlyAllocatedEntry) []

  ramify (CombEntry Constructor (WellKnownName variable) (ChildHeaders children)) = Node header body
    where
      header = show variable ++ " Constructor"
      body = map ramify children

  ramify (CombEntry Rule (WellKnownName variable) (ChildHeaders children)) = Node header body
    where
      header = show variable ++ "() " ++ "Rule"
      body = map ramify children


  ramify
    (DuplicationEntry
      (DuplicationPayload
        (Debug_TwinNames
          (WellKnownName left_VariableName)
          (WellKnownName right_variableName))
        usageOfVariables
        (ExpressionToBeDuplicated typedReentranceIntoTheStore)))
    = Node header body
    where
      header = "Duplication"
      (left, right)
        = case usageOfVariables of
            BothUsed
              (SubPointer
                (OuterPointer (Address left__outer_number))
                (InnerPointer (Address left__inner_number)))
              (SubPointer
                (OuterPointer (Address right_outer_number))
                (InnerPointer (Address right_inner_number)))
              ->
                ( arrowing left__outer_number left__inner_number left_VariableName
                , arrowing right_outer_number right_inner_number right_variableName
                )
            BothUsed --TODO refactors
              LooseBackReference
              (SubPointer
                (OuterPointer (Address right_outer_number))
                (InnerPointer (Address right_inner_number)))
              ->
                ( loosing  left_VariableName
                , arrowing right_outer_number right_inner_number right_variableName
                )
            x -> error $ "ramify not implemented for: " ++ show x

      arrowing n m name = show n ++ ":" ++ show m ++ " <- " ++ name ++ " :="
      loosing name = "loose <- " ++ name

      body = [return left, Node right [toBeDuplicatedOnDemand]]
      toBeDuplicatedOnDemand = ramify typedReentranceIntoTheStore


  ramify (ApplicationEntry (ApplicationPayload (TheExpressionToApply f) (TheArgument x)))
    = Node header body
    where
      header = "Application"

      body = [ramify f, ramify x]


  ramify (LambdaEntry (TermWithPlaceholderVariable (WhereIsMyBoundedSoloVariable (SubPointer (OuterPointer (Address outerAdress)) (InnerPointer (Address innerAdress))) (WellKnownName variableName) ) (WhereIsTheLambdaBody the_body)))
    = Node header body
    where
      header = "Lambda"

      body = [Node whereIsTheVariable [], ramify the_body]

      whereIsTheVariable
        = variableName
        ++" @"
        ++ show outerAdress
        ++ ":"
        ++ show innerAdress
        ++ " |->"


  ramify (IntrinsicOperationEntry leftArgument rightArgument)
    = Node header body
    where
      header = "(+)"

      body = [ramify leftArgument, ramify rightArgument]


  ramify (SuperpositionEntry leftTerm rightTerm)
    = Node header body
    where
      header = "Superposition {..}"

      body = [ramify leftTerm, ramify rightTerm]


  ramify x = error $ "ramify not implemented for: " ++ show x

