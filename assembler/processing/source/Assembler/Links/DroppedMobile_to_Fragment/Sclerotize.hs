module Assembler.Links.DroppedMobile_to_Fragment.Sclerotize where

import Data.Maybe
import Data.List

import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.IdentificationNumber
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.Bounding

import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.OxygenatedTerm
import Assembler.Links.OxygenatedTerm_to_DroppedMobile.DroppedMobile
import Assembler.Links.DroppedMobile_to_Fragment.Fragment
import Assembler.Links.DroppedMobile_to_Fragment.RuntimeStore
import Assembler.Links.DroppedMobile_to_Fragment.LookupTable
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.PointerAddress
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor

import Assembler.Links.DroppedMobile_to_Fragment.IndexOffsetting

import Assembler.Links.DroppedMobile_to_Fragment.CombArity


-- TODO offset appears to be deprecated
-- TODO position independence might be more interesting since it could allow applying rules by just copying some unmodified byte array?

sclerotize_to_zero :: DroppedMobile -> Fragment
sclerotize_to_zero = sclerotize offsetTranslation_of_zero



sclerotize
  :: OffsetTranslation
  -> DroppedMobile
  -> Fragment
sclerotize
  offset
  (DroppedMobile
    (EntranceIdentification root_identity)
    loose_entries
  )
  = result
  where
    LookupLayout
      (SortedEntries addressableEntries)
      lookupTable
      = generateLookupLayout offset
      $ loose_entries

    --

    result = Fragment root (RuntimeStore offset store)

    root = entrance_header

    --

    store
      = map
          (render_two_level_addresses lookupTable)
          addressableEntries
      -- here we assume that the loose_entries are sorted the same as the lookupTable.

    loose_entries_entrance_entry
      :: LooseEntry
    loose_entries_entrance_entry
      = id
      $ fromJustErrorMessage
      $ lookupLooseEntry lookupTable
      $ root_identity

    fromJustErrorMessage (Just x) = x
    fromJustErrorMessage Nothing = error "could not lookup Entrance"

    entrance_header
      = extractHeaderInformation
          lookupTable
          loose_entries_entrance_entry



render_two_level_addresses
  :: LookupTables
  -> LooseEntry
  -> SclerotizedEntry


-- TODO remove in Release build
    -- TODO why? performance? probably not
render_two_level_addresses
  _lookupTable
  entry@(
    Loose_CombEntry
      Constructor
      _identification
      _variableName
      (Children [])
  )
  = error $ "leafs do not have a store entry representation: " ++ show entry

render_two_level_addresses
  _lookupTable
  entry@(
    Loose_CombEntry
      Rule
      _identification
      _variableName
      (Children [])
  )
  = error $ "rule without arguments, what now: " ++ show entry


render_two_level_addresses
  lookupTable
  (Loose_CombEntry
    rule_or_constructor
    _identification
    variableName
    (Children childIds)
  )
  = result
  where
    result = CombEntry rule_or_constructor variableName subReentrances

    subReentrances
      = ChildHeaders
      $ map determineSubReentrance childIds

    determineSubReentrance
      :: NodeIdentificationNumber
      -> TypedReentranceIntoTheStore
    determineSubReentrance
      identificationNumber
      = id
      $ extractHeaderInformation lookupTable
      $ assumeJust
      $ lookupLooseEntry lookupTable
      $ identificationNumber
      where
        assumeJust (Just x) = x
        assumeJust Nothing
          = error
          $ "could not lookup identity"


render_two_level_addresses
  lookupTable
  (Loose_ApplicationEntry _identification (Loose_ToBeApplied f) (Loose_TheArgument x))
  = result
  where
    result
      = ApplicationEntry
          (ApplicationPayload
            (TheExpressionToApply f_reentrance)
            (TheArgument x_reentrance)
          )

    f_reentrance :: TypedReentranceIntoTheStore
    f_reentrance = id
      $ extractHeaderInformation lookupTable
      $ assumeJust
      $ lookupLooseEntry lookupTable
      $ f

    x_reentrance :: TypedReentranceIntoTheStore
    x_reentrance = id
      $ extractHeaderInformation lookupTable
      $ assumeJust
      $ lookupLooseEntry lookupTable
      $ x

    assumeJust (Just q) = q
    assumeJust Nothing = error $ "could not lookup identity"


render_two_level_addresses
  lookupTable
  (Loose_LambdaEntry
    _identification
    (UsedVariable
      (Occurence whoHasTheVariable what_is_the_birth_number)
      variableName)
    (Loose_LambdaBody body))
  = result
  where
    result
      = LambdaEntry
          (TermWithPlaceholderVariable
            where_is_the_here_bound_variable_used
            (WhereIsTheLambdaBody body_reentrance)
          )

    body_reentrance :: TypedReentranceIntoTheStore
    body_reentrance = id
      $ extractHeaderInformation lookupTable
      $ assumeJust
      $ lookupLooseEntry lookupTable
      $ body

    assumeJust (Just x) = x
    assumeJust Nothing = error $ "could not lookup identity"

    where_is_the_here_bound_variable_used
      = WhereIsMyBoundedSoloVariable
          (SubPointer
            outerPointer
            what_is_the_birth_number)
          variableName
      where
        outerPointer
          = case lookupEntryAdress lookupTable whoHasTheVariable of
              Just x -> x
              Nothing -> error $ show lookupTable


render_two_level_addresses
  lookupTable
  (Loose_DuplicationEntry
    _identificationNumber
    whereIsLeftTwin
    whereIsRightTwin
    (Loose_ToBeDuplicated to_be_duplicated_identity))
  = result
  where
    result
      = DuplicationEntry
          (DuplicationPayload
            (Debug_TwinNames left_VariableName right_VariableName)
            usageOfVariables
            (ExpressionToBeDuplicated to_be_duplicated_reentrance))


    --TODO are there ever other cases than BothUsed?
    usageOfVariables
      = BothUsed
          (SubPointer outer_pointer_to_left_twin left_twin_birth_number)
          (SubPointer outer_pointer_to_right_twin right_twin_birth_number)

    to_be_duplicated_reentrance :: TypedReentranceIntoTheStore
    to_be_duplicated_reentrance = id
      $ extractHeaderInformation lookupTable
      $ assumeJust
      $ lookupLooseEntry lookupTable
      $ to_be_duplicated_identity

    assumeJust (Just x) = x
    assumeJust Nothing = error $ "could not lookup to be duplicated"

    --
    -- TODO clean up these

    (whoHasTheLeftTwin, left_twin_birth_number, left_VariableName)
      = case whereIsLeftTwin of
          LeftTwinWhereabout
            (UsedVariable
              (Occurence the_whoHasTheLeftTwin the_left_twin_birth_number)
              the_left_VariableName)
            -> (the_whoHasTheLeftTwin, the_left_twin_birth_number, the_left_VariableName)

    outer_pointer_to_left_twin
      = assumeJust_left
      $ lookupEntryAdress lookupTable
      $ whoHasTheLeftTwin
      :: OuterPointer

    assumeJust_left (Just x) = x
    assumeJust_left Nothing = error $ "could not lookup left twin identity"

    --

    (whoHasTheRightTwin, right_twin_birth_number, right_VariableName)
      = case whereIsRightTwin of
          RightTwinWhereabout
            (UsedVariable
              (Occurence the_whoHasTheRightTwin the_right_twin_birth_number)
              the_right_VariableName)
            -> (the_whoHasTheRightTwin, the_right_twin_birth_number, the_right_VariableName)

    outer_pointer_to_right_twin
      = assumeJust_right
      $ lookupEntryAdress lookupTable
      $ whoHasTheRightTwin
      :: OuterPointer

    assumeJust_right (Just x) = x
    assumeJust_right Nothing = error $ "could not lookup left twin identity"


render_two_level_addresses
  lookupTable
  (Loose_OperationEntry
    _identificationNumber
    (Loose_LeftOperatorArgument left_argument)
    (Loose_RightOperatorArgument right_argument))
  = result
  where
    result
      = IntrinsicOperationEntry
          left_argument_reentrance
          right_argument_reentrance

    left_argument_reentrance :: TypedReentranceIntoTheStore
    left_argument_reentrance = id
      $ extractHeaderInformation lookupTable
      $ assumeJust_left
      $ lookupLooseEntry lookupTable
      $ left_argument

    right_argument_reentrance :: TypedReentranceIntoTheStore
    right_argument_reentrance = id
      $ extractHeaderInformation lookupTable
      $ assumeJust_right
      $ lookupLooseEntry lookupTable
      $ right_argument

    assumeJust_right (Just x) = x
    assumeJust_right Nothing = error $ "could not lookup right argument of intrinsic operator"

    assumeJust_left (Just x) = x
    assumeJust_left Nothing = error $ "could not lookup left argument of intrinsic operator"



render_two_level_addresses x y = error $ "render_two_level_addresses not implemented for: " ++ show (y,x)












extractHeaderInformation
  :: LookupTables
  -> LooseEntry
  -> TypedReentranceIntoTheStore

extractHeaderInformation
  _lookupTables
  (Loose_CombEntry
    Constructor
    _identificationNumber
    variableName
    (Children []))
  = ConstructorLeafHeader variableName

extractHeaderInformation
  _lookupTables
  (Loose_CombEntry
    Rule
    _identificationNumber
    _variableName
    (Children []))
  = error $ "rule without parameter"

extractHeaderInformation
  lookupTables
  (Loose_CombEntry
    rule_or_constructor
    identificationNumber
    variableName
    (Children child_identities)
  )
  = result
  where
    where_will_i_be_placed :: OuterPointer
    where_will_i_be_placed
      = fromMaybe (error errorCase)
      $ lookupEntryAdress lookupTables
      $ identificationNumber

    errorCase
      = "could not find adress of entry identified with: "
      ++ show identificationNumber
      --TODO better error messages

    result
      = CombHeader
          rule_or_constructor
          variableName
          (ActualUtilizedArity arity)
          (Pointer_To_Children where_will_i_be_placed)

    arity
      = convertToCombArity
      $ genericLength
      $ child_identities


extractHeaderInformation
  lookupTables
  (Loose_ApplicationEntry
    identificationNumber
    (Loose_ToBeApplied _)
    (Loose_TheArgument _)
  )
  = result
  where
    where_will_i_be_placed :: OuterPointer
    where_will_i_be_placed
      = fromJust
      $ lookupEntryAdress lookupTables
      $ identificationNumber


    result
      = ApplicationHeader
          (Pointer_To_Application where_will_i_be_placed)



extractHeaderInformation
  lookupTables
  (Loose_LambdaEntry
    identificationNumber
    _variableName
    (Loose_LambdaBody _body_identification_number)
  )
  = result
  where
    where_will_i_be_placed :: OuterPointer
    where_will_i_be_placed
      = fromJust
      $ lookupEntryAdress lookupTables
      $ identificationNumber

    result
      = LambdaHeader
          (Pointer_To_LambdaPayload where_will_i_be_placed)


extractHeaderInformation
  lookupTables
  (Loose_OperationEntry identificationNumber _ _)
  = result
  where
    result = IntrinsicOperationHeader (Pointer_To_IntrinsicOperation where_will_i_be_placed)

    where_will_i_be_placed :: OuterPointer
    where_will_i_be_placed
      = fromJust
      $ lookupEntryAdress lookupTables
      $ identificationNumber


extractHeaderInformation
  _lookupTables
  (Loose_IntrinsicNumberEntry _identificationNumber the_number)
  = result
  where
    result = IntrinsicNumberHeader the_number


extractHeaderInformation
  lookupTables
  (Loose_PlaceholderVariable
    _identificationNumber
    (BoundedVariableName
      (LambdaBound bounder_identification)
      variableName
    )
  )
  = result
  where
    where_am_i_bound :: OuterPointer
    where_am_i_bound
      = fromJust
      $ lookupEntryAdress lookupTables
      $ bounder_identification

    result
      = SoloVariableOccurence
          variableName
          (WhereIsTheVariableBound
            $ Pointer_To_LambdaPayload
            $ where_am_i_bound)

extractHeaderInformation
  lookupTables
  (Loose_PlaceholderVariable
    _identificationNumber
    (BoundedVariableName
      (DuplicationBound
        twinCase
        bounder_node_identification
        duplicationIdentificationNumber
      )
      variableName
    )
  )
  = result
  where
    where_am_i_bound :: OuterPointer
    where_am_i_bound
      = fromJust
      $ lookupEntryAdress lookupTables
      $ bounder_node_identification

    result
      = TwinVariableOccurence
          twinCase
          variableName
          (Pointer_To_Duplication
            where_am_i_bound
          )
          (Label_of_Duplication
            duplicationIdentificationNumber
          )



extractHeaderInformation x y = error $ "extractHeaderInformation not implemented for: " ++ show (y,x)

