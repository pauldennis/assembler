module Assembler.Compiler where

import Control.DeepSeq
import Data.List

import Numeric.Natural

import Trifle.Missing

import Assembler.Links.UntrustedExpression.WellKnownName

import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.OxygenatedTerm
import Assembler.Links.OxygenatedTerm_to_DroppedMobile.DroppedMobile
import Assembler.Links.UntrustedExpression.UntrustedExpression
import Assembler.Links.DroppedMobile_to_Fragment.Fragment
import Assembler.Links.DroppedMobile_to_Fragment.RuntimeStore
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.PointerAddress
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.Oxygenate
import Assembler.Links.OxygenatedTerm_to_DroppedMobile.FlattenMobile
import Assembler.Links.DroppedMobile_to_Fragment.Sclerotize


import Assembler.Links.Fragment_to_PointerArrayFragment.PointerArrayStore
import Assembler.Links.Fragment_to_PointerArrayFragment.PointerNodeRender
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.BitPointerArrayFragment
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.BitPointerArrayRender
import Assembler.Links.BitPointerArrayFragment_to_LocalInterchangeableFragment.LocalInterchangeableFragment
import Assembler.Links.BitPointerArrayFragment_to_LocalInterchangeableFragment.LocalInterchangeableFragmentRender
import Assembler.Links.DroppedMobile_to_Fragment.HardCodedLinkLocation
import Assembler.Links.DroppedMobile_to_Fragment.IndexOffsetting
import Assembler.Links.DroppedMobile_to_Fragment.CombArity
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.WellKnownNameLists

import Assembler.Weft.UntrustedModule
import Assembler.Weft.LocalInterchangeableModule

import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.Arity
import Data.List.NonEmpty( NonEmpty( (:|) ) )

import Assembler.LinkableFragment

-----




test_compile_module_ball :: (String, Bool)
test_compile_module_ball
  = (,) "test_compile_module" $ result
  where
    -- TODO library function?
    result = (\x -> deepseq x True) $ show $ compile_module ball

    ball :: UntrustedModule
    ball =
      UntrustedModule
        (RootRule
          (Comb Rule (WellKnownName "¿Encapsulate")
            [Comb Constructor (WellKnownName "Tuple0")
              []
            ,Lambda (WellKnownName "privateData")
              (Lambda (WellKnownName "coordinate")
                (Comb Constructor (WellKnownName "ReturnNewPrivateDataAndSystemRequest")
                  [PlaceholderVariable (WellKnownName "privateData")
                  ,Comb Rule (WellKnownName "IsWithinUnitBall")
                    [PlaceholderVariable (WellKnownName "coordinate")]
                  ]
                )
              )
            ]
          )
        )
        (Rulebook
          [UntrustedRule
            (WellKnownName "IsWithinUnitBall")
            (
              (PatternBranch
                (PatternMatch (ConstructorName (WellKnownName "Vector3")))
                (Lambda (WellKnownName "x")
                  (Lambda (WellKnownName "y")
                    (Lambda (WellKnownName "z")
                      (Comb Rule (WellKnownName "¿Float32_IsLessThan")
                        [Comb Rule (WellKnownName "¿Float32_Addition_RoundingDown")
                          [Comb Rule (WellKnownName "¿Float32_Addition_RoundingDown")
                            [Comb Rule (WellKnownName "¿Float32_Square_RoundingDown")
                              [PlaceholderVariable (WellKnownName "x")]
                            ,Comb Rule (WellKnownName "¿Float32_Square_RoundingDown")
                              [PlaceholderVariable (WellKnownName "y")]
                            ]
                          ,Comb Rule (WellKnownName "¿Float32_Square_RoundingDown")
                            [PlaceholderVariable (WellKnownName "z")]
                          ]
                        ,IntrinsicNumber 1065353216
                        ]
                      )
                    )
                  )
                )
              )
              :|
              []
            )
          ]
        )

test_compile_module_squircle :: (String, Bool)
test_compile_module_squircle
  = (,) "test_compile_module_squircle" $ result
  where
    -- TODO library function?
    result = (\x -> deepseq x True) $ show $ compile_module squircle

    squircle :: UntrustedModule
    squircle =
      UntrustedModule
        (RootRule
          (Comb Rule (WellKnownName "¿Encapsulate")
            [Comb Constructor (WellKnownName "Tuple0")
              []
            ,Lambda (WellKnownName "privateData")
              (Lambda (WellKnownName "coordinate")
                (Comb Constructor (WellKnownName "ReturnNewPrivateDataAndSystemRequest")
                  [PlaceholderVariable (WellKnownName "privateData")
                  ,Comb Rule (WellKnownName "IsWithinUnitBall")
                    [PlaceholderVariable (WellKnownName "coordinate")]
                  ]
                )
              )
            ]
          )
        )
        (Rulebook
          [UntrustedRule
            (WellKnownName "IsWithinUnitBall")
            (
              (PatternBranch
                (PatternMatch (ConstructorName(WellKnownName "Vector3")))
                (Lambda (WellKnownName "x") (Lambda (WellKnownName "y") (Lambda (WellKnownName "z") (Comb Rule (WellKnownName "\191Float32_IsLessThan") [Comb Rule (WellKnownName "\191Float32_Addition_RoundingDown") [Comb Rule (WellKnownName "\191Float32_Addition_RoundingDown") [Comb Rule (WellKnownName "\191Float32_Square_RoundingDown") [Comb Rule (WellKnownName "\191Float32_Square_RoundingDown") [PlaceholderVariable (WellKnownName "x")]],Comb Rule (WellKnownName "\191Float32_Square_RoundingDown") [Comb Rule (WellKnownName "\191Float32_Square_RoundingDown") [PlaceholderVariable (WellKnownName "y")]]],Comb Rule (WellKnownName "\191Float32_Square_RoundingDown") [Comb Rule (WellKnownName "\191Float32_Square_RoundingDown") [PlaceholderVariable (WellKnownName "z")]]],IntrinsicNumber 1065353216]))))
              )
              :|
              []
            )
          ]
        )






----------------------------------------------------------







compileRuleDeclaration :: WellKnownName -> RuleDeclaration
compileRuleDeclaration
  wellKnownName
  = (RuleDeclaration
      (LocalRuleIdentification ruleIdentity)
      (DebugHint wellKnownName)
    )
  where
    ruleIdentity = lookup_RuleIdentity_of_WellKnownName wellKnownName



-- TODO do we need/want some kind of type system and constrain the matched constructors to a type?
-- TODO provide testcases for all the certify cases
compile_rule :: UntrustedRule -> LocalRule
compile_rule
  (UntrustedRule
    ruleName_wellKnownName -- TODO wrap into RuleName?
    branches
  )
  = check_for_ambiguous_branch_definitions
  $ LocalRule
      (compileRuleDeclaration ruleName_wellKnownName)
      (fmap certifyArityMatch branches)
  where
    check_for_ambiguous_branch_definitions
      x
      = if isNubbed branch_constructor_identification_patternMatchings
          then x
          else error $ "there are ambiguous branches"

    certifyArityMatch
      (PatternBranch patternMatch untrustedExpression)
      = verifyMatchingOfAuxiliaryLambdas
          ruleName_wellKnownName
          patternMatch
          untrustedExpression

    branch_constructor_identification_patternMatchings
      = (\(x:|xs)->x:xs)
      $ fmap (\(PatternBranch x _)->x) branches



{-
  TODO do we actually need to check for the arity of the rule fragment?
  Instead of:
  ```
  f (C x y t) = g x y t
  ```

  One could have:
  ```
  f (C) = g
  ```

  For now lets forbit it because this might allow some optimizations later on?

  TODO i do not understand this anymore. Probably too complicated to consider?
-}
verifyMatchingOfAuxiliaryLambdas
  :: WellKnownName -- TODO wrap into RuleName?
  -> PatternMatch
  -> UntrustedExpression
  -> RuleBranch
verifyMatchingOfAuxiliaryLambdas
  rule_name_wellKnownName
  (PatternMatch matchedConstructor)
  fragmentSupposedlyWrapedInAuxiliaryLambdas
  = result
  where
    result = RuleBranch patternMatchSignature verifiedRuleFragment

    patternMatchSignature = PatternMatchSignature identificationMatch

    identificationMatch = lookup_ConstructorIdentity_of_WellKnownName $ (\(ConstructorName x) -> x) matchedConstructor --TODO make lookup more type safe

    untrustedRuleFragment = verify_for_n_lambdas numberOfMatchedVariables fragmentSupposedlyWrapedInAuxiliaryLambdas

    verifiedRuleFragment = compile_fragment untrustedRuleFragment

    -- TODO wrong result type!?
    arityOfMatchedVariables :: Arity
    arityOfMatchedVariables = lookup_ConstructorArity_of_WellKnownName $ (\(ConstructorName x) -> x) matchedConstructor --TODO

    numberOfMatchedVariables
      = case arityOfMatchedVariables of
          Arity_0 -> 0
          Arity_1 -> 1
          Arity_2 -> 2
          Arity_3 -> 3
          Arity_4 -> 4
          Arity_5 -> 5
          Arity_6 -> 6
          Arity_7 -> 7
          Arity_8 -> 8

    verify_for_n_lambdas :: Natural -> UntrustedExpression -> UntrustedExpression
    verify_for_n_lambdas 0 expression = expression
    verify_for_n_lambdas n (Lambda x xs) = Lambda x (verify_for_n_lambdas (n-1) xs)
    verify_for_n_lambdas n xs = error $ show
      ("did not find enough auxiliary lambdas. There are x lambdas missing: "
      , n
      , "the overall required arity is at least: "
      , arityOfMatchedVariables
      , "try accepting more arguments by providing a fragment with additional lambdas, or declaring the rule to take less arguments"
      , xs
      , rule_name_wellKnownName
      , "not enough auxiliary lambdas"
      )



-- TODO either remove the representation of user provided rules with arity other than 1
-- TODO or let the runtime handle this cases
-- NOTE: the intrinsic binary operators have proper arity of two (or one?)
verify_call_count_is_arity_of_rules
  :: UntrustedModule
  -> UntrustedModule
verify_call_count_is_arity_of_rules
  (UntrustedModule (RootRule expression) (Rulebook untrustedRules))
  = (UntrustedModule (RootRule $ check expression) (Rulebook $ map checkRule untrustedRules))
  where
    check x@(Comb isItARuleOrConstructor wellKnownName untrustedExpressions)
      = if callCount == arity_to_Natural arity
          then skipVerification check x
          else error $ "arity missmatch: " ++ show (callCount, arity)
      where
        callCount = genericLength untrustedExpressions
        arity = getArity isItARuleOrConstructor wellKnownName

    check x = skipVerification check x

    checkRule (UntrustedRule wellKnownName patternBranches)
      = (UntrustedRule wellKnownName $ fmap checkBranches patternBranches)

    checkBranches (PatternBranch patternMatch recursiveExpression)
      = (PatternBranch patternMatch $ check recursiveExpression)



compile_module :: UntrustedModule -> LocalInterchangeableModule
compile_module
  = package_module
  . verify_call_count_is_arity_of_rules


-- | every untrusted user code needs to pass this function by definition inorder to be safely linked into the runtime
package_module
  :: UntrustedModule
  -> LocalInterchangeableModule
package_module
  the_whole_module@(UntrustedModule
    (RootRule untrustedExpression)
    (Rulebook rules)
  )
  = check_for_ambiguous_rule_definitions
  $ LocalInterchangeableModule
      (ExternalProvidedRules rules_missing_definition)
      (RootFragment compiled_root)
      (LocalRuleBook compiled_book)
  where
    -- TODO check for unused rules?

    check_for_ambiguous_rule_definitions
      x
      = if isNubbed rule_definitions
          then x
          else error $ "there are ambiguous rules"

    compiled_root = compile_fragment untrustedExpression

    compiled_book = map compile_rule rules

    -- TODO properly aggregate all occurences of Rules, give them an id and then remove the rules that are defined
    rules_missing_definition :: [RuleDeclaration]

    --TODO performance. use an alternative to \\ that exploits Ord
    rules_missing_definition = rule_occurences \\ rule_definitions

    rule_definitions :: [RuleDeclaration]
    rule_definitions = map (compileRuleDeclaration . get_wellKnownName_of_UntrustedRule) rules
    rule_occurences :: [RuleDeclaration]
    rule_occurences = extract_all_rule_call_occurences_of_module the_whole_module




-- TODO have a proper rule aggregation with some state monad for free identifications. since the rule usage in a module is local it does not matter that the rules identifications are reeused between modules. When the module are linked the embedder provides the link process with some mapping to use. That way the modules are sandboxed from one another?
extract_all_rule_call_occurences_of_module :: UntrustedModule -> [RuleDeclaration]
extract_all_rule_call_occurences_of_module
  (UntrustedModule
    (RootRule expression)
    (Rulebook rules)
  )
  = result
  where
    expressions = rules >>= get_expression_of_UntrustedRule

    allExpressions = expression : expressions

    result = allExpressions >>= extract_all_rule_call_occurences_of_rule



extract_all_rule_call_occurences_of_rule :: UntrustedExpression -> [RuleDeclaration]
extract_all_rule_call_occurences_of_rule = nub . extract
  where
    extract (Comb Rule wellKnownName recursives) = (compileRuleDeclaration wellKnownName) : (recursives >>= extract)

    extract (Comb Constructor _ recursives) = recursives >>= extract

    extract (Lambda _ recursive) = extract recursive

    extract (PlaceholderVariable _) = []
    extract (IntrinsicNumber _) = []

    extract (DuplicationIn _ _ (ToBeDuplicated toBeDuplicated) body) = [toBeDuplicated, body] >>= extract

    extract (Application f x) = [f, x] >>= extract


    extract x = error $ "extract_all_rule_call_occurences_of_rule: " ++ show x


