module Tapeworm.RuntimeStoreOperations where

import Control.Monad.Trans.State.Strict
import Control.Monad.Trans.Except
import Control.Monad.Trans.Class
import Data.List
import Numeric.Natural
import Safe
import Safe.Partial
import GHC.Stack

import Trifle.Rename_Except

import Assembler.Links.DroppedMobile_to_Fragment.RuntimeStore
import Assembler.Links.UntrustedExpression.WellKnownName
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.PointerAddress
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.TwinCase
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor
import Assembler.Links.DroppedMobile_to_Fragment.IndexOffsetting
import Assembler.Links.UntrustedExpression_to_OxygenatedTerm.RuntimeLayout


genericSetAt :: Natural -> a -> [a] -> [a]
genericSetAt i replacement list = go i list
  where
    go 0 (_:xs) = replacement : xs
    go n (x:xs) = x : go (n-1) xs
    go _ [] = []

genericAtIndexSafe
  :: a
  -> Natural
  -> [a]
  -> a
genericAtIndexSafe defaultValue address list = result
  where
    result
      = if address < listLength
           then genericIndex list address
           else defaultValue

    listLength = genericLength list





---





data Error_RawLookup
  = Error_store_has_items_beginning_from_x_but_got_index_x
      Natural
      Natural
  | Error_store_has_items_ending_at_x_but_got_index_x
      OuterPointer
      OuterPointer
  deriving Show


lookupStoreEntry
  :: OuterPointer
  -> ExceptT Error_RawLookup (State RuntimeStore) SclerotizedEntry
lookupStoreEntry
  outerPointer
  = do

  RuntimeStore offset entries <- lift get

  InnerViewAdresss number_inside_view
    <- case undergo_translation offset outerPointer of
        Right number
          -> return number
        Left (ErrorInnerPointerWouldBeNegative x y)
          -> throwException
              $ Error_store_has_items_beginning_from_x_but_got_index_x
                  x
                  y

  let storeLength = genericLength entries

  if number_inside_view < storeLength
    then return ()
    else throwException
          $ Error_store_has_items_ending_at_x_but_got_index_x
              (bedizen_translation offset storeLength)
              outerPointer

  let lookedUpItem = genericIndex entries number_inside_view
  return lookedUpItem






data Error_LookupRawSubEntry
  = Error_WhileLookingUpOuterAdress Error_RawLookup
  | Error_SubIndexOutOfBounds_exclusive_max_is_x_but_got_x Natural Natural
  | Error_InnerAdressMustBeOneInorderToGetTheOnlyEntryInAnLambda
  deriving Show

lookupStoreSubEntry
  :: Partial
  => SubPointer
  -> ExceptT
      Error_LookupRawSubEntry
      (State RuntimeStore)
      TypedReentranceIntoTheStore
lookupStoreSubEntry
  pointer@(SubPointer outerPointer (InnerPointer (Address innerAdress)))
  = withFrozenCallStack $ do
  entry
    <- wrapException Error_WhileLookingUpOuterAdress
    $ lookupStoreEntry outerPointer

  case entry of
    CombEntry Constructor _debug_variable (ChildHeaders subEntries) -> do
      let payloadLength = genericLength subEntries
      if innerAdress < payloadLength
        then return $ genericIndex subEntries innerAdress
        else throwException
                $ Error_SubIndexOutOfBounds_exclusive_max_is_x_but_got_x
                    payloadLength
                    innerAdress
    LambdaEntry (TermWithPlaceholderVariable _ (WhereIsTheLambdaBody subEntry))
      -> do
          if layout_position_lambda_body == (Address innerAdress)
            then return $ subEntry
            else throwException Error_InnerAdressMustBeOneInorderToGetTheOnlyEntryInAnLambda

    x -> error $ "no lookupStoreSubEntry at " ++ show pointer ++ " defined for: " ++ show x

lookupStoreSubEntry LooseBackReference = error "LooseBackReference"


data Error_LookingUpSoloVariable
  = Error_WhileLookinUpSubEntry Error_LookupRawSubEntry
  | Error_LookingUpSoloVariable TypedReentranceIntoTheStore
  deriving Show

lookupBoundedSoloVariableOccurence_VariableName
  :: Partial
  => WhereIsMyBoundedSoloVariable
  -> ExceptT
      Error_LookingUpSoloVariable
      (State RuntimeStore)
      WellKnownName
lookupBoundedSoloVariableOccurence_VariableName
  (WhereIsMyBoundedSoloVariable subPointer variableName)
  = withFrozenCallStack $ do
  reentrance
    <- wrapException Error_WhileLookinUpSubEntry
    $ lookupStoreSubEntry
    $ subPointer

  case reentrance of
    SoloVariableOccurence foundVariableName _pointer_To_LambdaPayload
      -> return
          $ assertNote
              "panic: variable name mixup"
              (foundVariableName==variableName)
              foundVariableName
    unexpected_header
      -> throwException
      $ Error_LookingUpSoloVariable
      $ unexpected_header


--



placeNewEntry_InStore
  :: SclerotizedEntry
  -> RuntimeStore
  -> (OuterPointer, RuntimeStore)
placeNewEntry_InStore
  entry
  (RuntimeStore offset entries)
  = result
  where
    result = (newIndex, newStore)

    formerLength = genericLength entries

    newIndex = bedizen_translation offset formerLength

    newStore
      = RuntimeStore offset
      $ entries
      ++ [entry]

placeNewEntry
  :: SclerotizedEntry
  -> State RuntimeStore OuterPointer
placeNewEntry entry = state $ placeNewEntry_InStore entry



--



data Error_PlaceEntry
  = Error_PlaceEntry_IndexOutOfBounds_exlusive_max_is_x_but_got_x Natural Natural
  | Error_Expeded_to_place_on_freshly_Allocated_space_but_got SclerotizedEntry
  | Error_CannotPlaceBeforeIndex OffsetTranslation OuterPointer
  deriving Show

placeEntry
  :: OuterPointer
  -> SclerotizedEntry
  -> ExceptT Error_PlaceEntry (State RuntimeStore) ()
placeEntry
  outerPointer
  storeEntry
  = do
  RuntimeStore offset entries <- lift get

  InnerViewAdresss outerPosition
    <- case undergo_translation offset outerPointer of
        Right innerViewAdresss
          -> return innerViewAdresss
        Left (ErrorInnerPointerWouldBeNegative _ _)
          -> throwException
              $ Error_CannotPlaceBeforeIndex offset outerPointer

  let entriesLength = genericLength entries

  newEntries
    <- if outerPosition < entriesLength
        then do
          let to_be_over_placed = genericIndex entries outerPosition
          case to_be_over_placed of
            FreshlyAllocatedEntry
              -> return ()
            existing_data
              -> throwException
              $ Error_Expeded_to_place_on_freshly_Allocated_space_but_got existing_data
          return $ genericSetAt outerPosition storeEntry entries
        else throwException
              $ Error_PlaceEntry_IndexOutOfBounds_exlusive_max_is_x_but_got_x
                  entriesLength
                  outerPosition

  lift $ put $ RuntimeStore offset newEntries


data Error_ReplaceEntry
  = Error_IndexOutOfBounds_exlusive_max_is_x_but_got_x Natural Natural
  | Error_CannotReplaceBeforeIndex OffsetTranslation OuterPointer
  deriving Show

rawReplaceEntry
  :: OuterPointer
  -> SclerotizedEntry
  -> ExceptT Error_ReplaceEntry (State RuntimeStore) ()
rawReplaceEntry
  outerPointer
  storeEntry
  = do
  RuntimeStore offset entries <- lift get

  InnerViewAdresss outerAdress
    <- case undergo_translation offset outerPointer of
        Right innerViewAdresss
          -> return innerViewAdresss
        Left (ErrorInnerPointerWouldBeNegative _ _)
          -> throwException
              $ Error_CannotReplaceBeforeIndex offset outerPointer

  let entriesLength = genericLength entries

  newEntries
    <- if outerAdress < entriesLength
        then return $ genericSetAt outerAdress storeEntry entries
        else throwException
              $ Error_IndexOutOfBounds_exlusive_max_is_x_but_got_x
                  entriesLength
                  outerAdress

  lift $ put $ RuntimeStore offset newEntries


data Error_ReplacingSubEntry
  = Error_WhileGettingEntry Error_RawLookup
  | Error_WhileReplacingChangedEntry Error_ReplaceEntry
  deriving Show

replaceTypedReentranceIntoTheStoreOccurence
  :: Partial
  => SubPointer
  -> TypedReentranceIntoTheStore
  -> ExceptT
      Error_ReplacingSubEntry
      (State RuntimeStore)
      ()
replaceTypedReentranceIntoTheStoreOccurence
  (SubPointer outerAdress (InnerPointer (Address innerAdress))) --TODO replace with algebraic subaddress
  replacement
  = withFrozenCallStack $ do

  storeEntry
    <- wrapException Error_WhileGettingEntry
    $ lookupStoreEntry outerAdress

  let withReplacedSubEntry
        = case storeEntry of
            CombEntry Constructor _debug_variable (ChildHeaders payloads)
              -> CombEntry Constructor _debug_variable
              $ ChildHeaders
              $ genericSetAt innerAdress replacement payloads

            LambdaEntry
                    (TermWithPlaceholderVariable boundedSoloVariableOccurence (WhereIsTheLambdaBody _old_reentrance))
              -> if layout_position_lambda_body == (Address innerAdress)
                    then LambdaEntry (TermWithPlaceholderVariable boundedSoloVariableOccurence (WhereIsTheLambdaBody replacement))
                    else error $ "invalid index in lambda, only accept "++show layout_position_lambda_body++" but got: " ++ show innerAdress

            DuplicationEntry
                    (DuplicationPayload debug_TwinNames usageOfVariables (ExpressionToBeDuplicated _old_reentrance))
              -> case innerAdress of
                    2 -> DuplicationEntry (DuplicationPayload debug_TwinNames usageOfVariables (ExpressionToBeDuplicated replacement))
                    m -> error $ "invalid index in duplication, only accept 2 but got: " ++ show m

            IntrinsicOperationEntry left_argument right_argument
              -> case innerAdress of
                    0 -> IntrinsicOperationEntry replacement right_argument --TODO use constants from RuntimeStore.hs
                    1 -> IntrinsicOperationEntry left_argument replacement
                    m -> error $ "invalid operant argument index x. I only know 0 or 1 for left and right argument" ++ show m --TODO should have specialized addresses?

            ApplicationEntry (ApplicationPayload (TheExpressionToApply f) (TheArgument x))
              -> case innerAdress of
                    0 -> ApplicationEntry (ApplicationPayload (TheExpressionToApply replacement) (TheArgument x))
                    1 -> ApplicationEntry (ApplicationPayload (TheExpressionToApply f) (TheArgument replacement))
                    m -> error $ "invalid operant argument index x. I only know 0 or 1 for f and x:" ++ show m --TODO should have specialized addresses?

            SuperpositionEntry left_superposed_term right_superposed_term
              -> case innerAdress of
                    0 -> SuperpositionEntry replacement right_superposed_term
                    1 -> SuperpositionEntry left_superposed_term replacement
                    _ -> undefined

            x
              -> error $ "replace subentry not implemented for: " ++ show (x, outerAdress, innerAdress, replacement)

  wrapException Error_WhileReplacingChangedEntry
    $ rawReplaceEntry outerAdress
    $ withReplacedSubEntry

replaceTypedReentranceIntoTheStoreOccurence
  x
  y
  = withFrozenCallStack
  $ error $ "replaceTypedReentranceIntoTheStoreOccurence not implemented for: " ++ show (x,y)


--------

data Error_makeBackReferenceOfTwinLoose
  = Error_WhileGettingDuplicationEntry Error_RawLookup
  | Error_WhileReplcaingWithDuplicationEntry Error_ReplaceEntry
  deriving Show

makeBackReferenceOfTwinLoose
  :: Partial
  => Pointer_To_Duplication
  -> TwinCase
  -> ExceptT
      Error_makeBackReferenceOfTwinLoose
      (State RuntimeStore)
      ()
makeBackReferenceOfTwinLoose
  (Pointer_To_Duplication outerAdress)
  LeftTwin
  = withFrozenCallStack $ do

  storeEntry
    <- wrapException Error_WhileGettingDuplicationEntry
    $ lookupStoreEntry outerAdress

  let withReplacedBackReference
        = case storeEntry of
            DuplicationEntry (DuplicationPayload debug_TwinNames (BothUsed _leftSubpointer rightSubpointer) expressionToBeDuplicated)
              -> DuplicationEntry (DuplicationPayload debug_TwinNames (BothUsed LooseBackReference rightSubpointer) expressionToBeDuplicated)

            x
              -> error $ "not a DuplicationEntry" ++ show (x, outerAdress)

  wrapException Error_WhileReplcaingWithDuplicationEntry
    $ rawReplaceEntry outerAdress
    $ withReplacedBackReference

makeBackReferenceOfTwinLoose _ _ = error "todo"

--------



data Error_Lookup pointerType
  = Error_Expected_certailType_for_x_but_got_x pointerType SclerotizedEntry
  | Error_WhileLookingUpRawEntry Error_RawLookup
  | Error_EmptyEntryBecauseGotDeleted pointerType
  deriving Show


lookupDuplicationPayload
  :: Pointer_To_Duplication
  -> ExceptT
      (Error_Lookup Pointer_To_Duplication)
      (State RuntimeStore)
      DuplicationPayload
lookupDuplicationPayload
  pointer@(Pointer_To_Duplication entryAdress)
  = do
  payload
    <- wrapException Error_WhileLookingUpRawEntry
    $ lookupStoreEntry
    $ entryAdress

  children
    <- case payload of
          DuplicationEntry to_be_duplicated
            -> return to_be_duplicated

          EmptyEntryBecauseGotDeleted
            -> throwException
            $ Error_EmptyEntryBecauseGotDeleted
                pointer

          unexpectedEntry
            -> throwException
            $ Error_Expected_certailType_for_x_but_got_x
                pointer
                unexpectedEntry

  return children


lookupApplicationPayload
  :: Pointer_To_Application
  -> ExceptT
      (Error_Lookup Pointer_To_Application)
      (State RuntimeStore)
      ApplicationPayload
lookupApplicationPayload pointer@(Pointer_To_Application address) = do
  payload
    <- wrapException Error_WhileLookingUpRawEntry
    $ lookupStoreEntry
    $ address

  applicationEntry
    <- case payload of
          ApplicationEntry x
            -> return x
          EmptyEntryBecauseGotDeleted
            -> throwException
            $ Error_EmptyEntryBecauseGotDeleted
                pointer
          unexpectedEntry
            -> throwException
            $ Error_Expected_certailType_for_x_but_got_x
                pointer
                unexpectedEntry

  return applicationEntry


lookupLambdaPayload
  :: Pointer_To_LambdaPayload
  -> ExceptT
      (Error_Lookup Pointer_To_LambdaPayload)
      (State RuntimeStore)
      LambdaPayload
lookupLambdaPayload pointer@(Pointer_To_LambdaPayload address) = do
  storeEntry
    <- wrapException Error_WhileLookingUpRawEntry
    $ lookupStoreEntry
    $ address

  wanted_payload
    <- case storeEntry of
          LambdaEntry payload
            -> return payload
          EmptyEntryBecauseGotDeleted
            -> throwException
            $ Error_EmptyEntryBecauseGotDeleted
                pointer
          unexpectedEntry
            -> throwException
            $ Error_Expected_certailType_for_x_but_got_x
                pointer
                unexpectedEntry

  return wanted_payload


lookupConstructorPayload
  :: Pointer_To_Children
  -> ExceptT
      (Error_Lookup Pointer_To_Children)
      (State RuntimeStore)
      ChildHeaders
lookupConstructorPayload pointer@(Pointer_To_Children address) = do
  storeEntry
    <- wrapException Error_WhileLookingUpRawEntry
    $ lookupStoreEntry
    $ address

  wanted_payload
    <- case storeEntry of
          CombEntry Constructor _debug_variable payload
            -> return payload
          EmptyEntryBecauseGotDeleted
            -> throwException
            $ Error_EmptyEntryBecauseGotDeleted
                pointer
          unexpectedEntry
            -> throwException
            $ Error_Expected_certailType_for_x_but_got_x
                pointer
                unexpectedEntry

  return wanted_payload


lookupIntrinsicOperationPayload
  :: Pointer_To_IntrinsicOperation
  -> ExceptT
      (Error_Lookup Pointer_To_IntrinsicOperation)
      (State RuntimeStore)
      (TypedReentranceIntoTheStore, TypedReentranceIntoTheStore) --TODO wrap like the others?
lookupIntrinsicOperationPayload pointer@(Pointer_To_IntrinsicOperation address) = do
  storeEntry
    <- wrapException Error_WhileLookingUpRawEntry
    $ lookupStoreEntry
    $ address

  wanted_payload
    <- case storeEntry of
          IntrinsicOperationEntry left_argument right_argument
            -> return (left_argument, right_argument)
          EmptyEntryBecauseGotDeleted
            -> throwException
            $ Error_EmptyEntryBecauseGotDeleted
                pointer
          unexpectedEntry
            -> throwException
            $ Error_Expected_certailType_for_x_but_got_x
                pointer
                unexpectedEntry

  return wanted_payload



lookupSuperpositionPayload
  :: Pointer_To_Superposition
  -> ExceptT
      (Error_Lookup Pointer_To_Superposition)
      (State RuntimeStore)
      (TypedReentranceIntoTheStore, TypedReentranceIntoTheStore) --TODO wrap like the others?
lookupSuperpositionPayload pointer@(Pointer_To_Superposition address) = do
  storeEntry
    <- wrapException Error_WhileLookingUpRawEntry
    $ lookupStoreEntry
    $ address

  wanted_payload
    <- case storeEntry of
          SuperpositionEntry left_term right_term
            -> return (left_term, right_term)
          EmptyEntryBecauseGotDeleted
            -> throwException
            $ Error_EmptyEntryBecauseGotDeleted
                pointer
          unexpectedEntry
            -> throwException
            $ Error_Expected_certailType_for_x_but_got_x
                pointer
                unexpectedEntry

  return wanted_payload


----


data Error_GeneratePointers
  = Error_GeneratePointers Error_RawLookup
  | Error_Expected_Constructor_but_got_x_by_following_x
      SclerotizedEntry
      OuterPointer
  | Error_ThereIsNoEntry OuterPointer
  deriving Show

generatePointersToChildrenOfConstructor
  :: Pointer_To_Children
  -> ExceptT Error_GeneratePointers (State RuntimeStore) [SubPointer]
generatePointersToChildrenOfConstructor
  (Pointer_To_Children outerPointer)
  = do
  payload
    <- wrapException Error_GeneratePointers
    $ lookupStoreEntry
    $ outerPointer

  ChildHeaders children
    <- case payload of
          CombEntry Constructor _debug_variable children
            -> return children
          EmptyEntryBecauseGotDeleted
            -> throwException
            $ Error_ThereIsNoEntry
                outerPointer
          unexpectedEntry
            -> throwException
            $ Error_Expected_Constructor_but_got_x_by_following_x
                unexpectedEntry
                outerPointer

  let number_of_children = genericLength children

  let innerPointers
        = map InnerPointer
        $ map Address
        $ [0..number_of_children-1]

  let pointers = map (SubPointer outerPointer) innerPointers

  return pointers



---


data Error_GeneratePointer
  = Error_GeneratePointer_LookingUpEntry Error_RawLookup
  | Error_Expected_Lambda_but_got_x_by_following_x
      SclerotizedEntry
      OuterPointer
  deriving Show

generatePointerToBodyOfLambda
  :: Pointer_To_LambdaPayload
  -> ExceptT Error_GeneratePointer (State RuntimeStore) SubPointer
generatePointerToBodyOfLambda
  (Pointer_To_LambdaPayload outerPointer)
  = do
  payload
    <- wrapException Error_GeneratePointer_LookingUpEntry
    $ lookupStoreEntry
    $ outerPointer

  _
    <- case payload of
          LambdaEntry _
            -> return ()
          unexpectedEntry
            -> throwException
            $ Error_Expected_Lambda_but_got_x_by_following_x
                unexpectedEntry
                outerPointer

  let innerPointers = InnerPointer layout_position_lambda_body

  let pointer = SubPointer outerPointer innerPointers

  return pointer




---






data Error_ReplaceVariableByLeafConstructor
  = Error_LookingUpTargets TwinCase Error_LookupRawSubEntry
  | Error_WhileReplacingWithLeafs Error_ReplacingSubEntry
  | Error_ExpectedLeftTwinButGot_x_by_following_x
      TypedReentranceIntoTheStore
      SubPointer
  | Error_ExpectedRightTwinButGot_x_by_following_x
      TypedReentranceIntoTheStore
      SubPointer
  deriving Show



replaceTwinsByLeafConstructor
  :: UsageOfVariables
  -> WellKnownName
  -> ExceptT
      Error_ReplaceVariableByLeafConstructor
      (State RuntimeStore)
      ()
replaceTwinsByLeafConstructor
  (BothUsed leftPointer rightPointer)
  leafName
  = do

  subEntry_left
    <- wrapException (Error_LookingUpTargets LeftTwin)
    $ lookupStoreSubEntry
    $ leftPointer

  subEntry_right
    <- wrapException (Error_LookingUpTargets RightTwin)
    $ lookupStoreSubEntry
    $ rightPointer

  -- TODO compile out in Release Mode
  case subEntry_left of
    TwinVariableOccurence LeftTwin _ _ _ -> return ()
    unexpected_header
      -> throwException
      $ Error_ExpectedLeftTwinButGot_x_by_following_x
          unexpected_header
          leftPointer

  case subEntry_right of
    TwinVariableOccurence RightTwin _ _ _ -> return ()
    unexpected_header
      -> throwException
      $ Error_ExpectedRightTwinButGot_x_by_following_x
          unexpected_header
          rightPointer


  wrapException Error_WhileReplacingWithLeafs
    $ replaceTypedReentranceIntoTheStoreOccurence
        leftPointer
        leafHeaderPointer

  wrapException Error_WhileReplacingWithLeafs
    $ replaceTypedReentranceIntoTheStoreOccurence
        rightPointer
        leafHeaderPointer


  where
    leafHeaderPointer = ConstructorLeafHeader leafName


replaceTwinsByLeafConstructor _ _ = error "todo"



----




data Error_replaceSoloVariableByProvidedReentrance
  = Error_replaceSoloVariableByProvidedReentrance
      Error_ReplacingSubEntry
  deriving Show

replaceSoloVariableByProvidedReentrance
  :: WhereIsMyBoundedSoloVariable
  -> TypedReentranceIntoTheStore
  -> ExceptT
      Error_replaceSoloVariableByProvidedReentrance
      (State RuntimeStore)
      ()
replaceSoloVariableByProvidedReentrance
  (WhereIsMyBoundedSoloVariable subPointer _variableName)
  replacement
  = do
  wrapException Error_replaceSoloVariableByProvidedReentrance
    $ replaceTypedReentranceIntoTheStoreOccurence
        subPointer
        replacement





--







data Error_DeleteDuplicationEntry
  = Error_DeleteDuplicationEntry Error_RawLookup
  | Error_WhileReplacing Error_ReplaceEntry
  | Error_ExpectedDuplication_but_got_x_by_following_x
      SclerotizedEntry
      OuterPointer
  deriving Show

deleteDuplicationEntry
  :: Pointer_To_Duplication
  -> ExceptT Error_DeleteDuplicationEntry (State RuntimeStore) ()
deleteDuplicationEntry (Pointer_To_Duplication outerPointer) = do

  --TODO do not do this in release mode
  payload
    <- wrapException Error_DeleteDuplicationEntry
    $ lookupStoreEntry outerPointer

  _children
    <- case payload of
          DuplicationEntry to_be_duplicated
            -> return to_be_duplicated
          unexpectedEntry
            -> throwException
            $ Error_ExpectedDuplication_but_got_x_by_following_x
                unexpectedEntry
                outerPointer

  wrapException Error_WhileReplacing
    $ rawReplaceEntry outerPointer
    $ EmptyEntryBecauseGotDeleted




data Error_DeleteApplicationEntry
  = Error_DeleteApplicationEntry Error_RawLookup
  | Error_WhileReplacingApplicationEntry Error_ReplaceEntry
  | Error_ExpectedApplicationEntry_but_got_x_by_following_x
      SclerotizedEntry
      OuterPointer
  deriving Show

deleteApplicationEntry
  :: Pointer_To_Application
  -> ExceptT Error_DeleteApplicationEntry (State RuntimeStore) ()
deleteApplicationEntry (Pointer_To_Application outerPointer) = do

  --TODO do not do this in release mode
  payload
    <- wrapException Error_DeleteApplicationEntry
    $ lookupStoreEntry outerPointer

  _children
    <- case payload of
          ApplicationEntry to_be_duplicated
            -> return to_be_duplicated
          unexpectedEntry
            -> throwException
            $ Error_ExpectedApplicationEntry_but_got_x_by_following_x
                unexpectedEntry
                outerPointer

  wrapException Error_WhileReplacingApplicationEntry
    $ rawReplaceEntry outerPointer
    $ EmptyEntryBecauseGotDeleted






data Error_DeleteLambdaEntry
  = Error_DeleteLambdaEntry Error_RawLookup
  | Error_WhileReplacingLambdaEntry Error_ReplaceEntry
  | Error_ExpectedLambdaEntry_but_got_x_by_following_x
      SclerotizedEntry
      OuterPointer
  deriving Show

deleteLambdaEntry
  :: Pointer_To_LambdaPayload
  -> ExceptT Error_DeleteLambdaEntry (State RuntimeStore) ()
deleteLambdaEntry (Pointer_To_LambdaPayload outerPointer) = do

  --TODO do not do this in release mode
  payload
    <- wrapException Error_DeleteLambdaEntry
    $ lookupStoreEntry outerPointer

  _children
    <- case payload of
          LambdaEntry to_be_duplicated
            -> return to_be_duplicated
          unexpectedEntry
            -> throwException
            $ Error_ExpectedLambdaEntry_but_got_x_by_following_x
                unexpectedEntry
                outerPointer

  wrapException Error_WhileReplacingLambdaEntry
    $ rawReplaceEntry outerPointer
    $ EmptyEntryBecauseGotDeleted






data Error_DeleteChildrenEntry
  = Error_DeleteChildrenEntry Error_RawLookup
  | Error_WhileReplacingChildrenEntry Error_ReplaceEntry
  | Error_ExpectedChildrenEntry_but_got_x_by_following_x
      SclerotizedEntry
      OuterPointer
  deriving Show

deleteChildrenEntry
  :: Pointer_To_Children
  -> ExceptT Error_DeleteChildrenEntry (State RuntimeStore) ()
deleteChildrenEntry (Pointer_To_Children outerPointer) = do

  --TODO do not do this in release mode
  payload
    <- wrapException Error_DeleteChildrenEntry
    $ lookupStoreEntry outerPointer

  _children
    <- case payload of
          CombEntry Constructor _debugConstructorName to_be_duplicated
            -> return to_be_duplicated
          unexpectedEntry
            -> throwException
            $ Error_ExpectedChildrenEntry_but_got_x_by_following_x
                unexpectedEntry
                outerPointer

  wrapException Error_WhileReplacingChildrenEntry
    $ rawReplaceEntry outerPointer
    $ EmptyEntryBecauseGotDeleted






data Error_DeleteIntrinsicOperationEntry
  = Error_DeleteIntrinsicOperationEntry Error_RawLookup
  | Error_WhileReplacingIntrinsicOperationEntry Error_ReplaceEntry
  | Error_ExpectedIntrinsicOperationEntry_but_got_x_by_following_x
      SclerotizedEntry
      OuterPointer
  deriving Show

deleteIntrinsicOperationEntry
  :: Pointer_To_IntrinsicOperation
  -> ExceptT Error_DeleteIntrinsicOperationEntry (State RuntimeStore) ()
deleteIntrinsicOperationEntry (Pointer_To_IntrinsicOperation outerPointer) = do

  --TODO do not do this in release mode
  payload
    <- wrapException Error_DeleteIntrinsicOperationEntry
    $ lookupStoreEntry outerPointer

  _
    <- case payload of
          IntrinsicOperationEntry _left_argument _right_argument
            -> return ()
          unexpectedEntry
            -> throwException
            $ Error_ExpectedIntrinsicOperationEntry_but_got_x_by_following_x
                unexpectedEntry
                outerPointer

  wrapException Error_WhileReplacingIntrinsicOperationEntry
    $ rawReplaceEntry outerPointer
    $ EmptyEntryBecauseGotDeleted





data Error_DeleteSuperpositionEntry
  = Error_DeleteSuperpositionEntry Error_RawLookup
  | Error_WhileReplacingSuperpositionEntry Error_ReplaceEntry
  | Error_ExpectedSuperpositionEntry_but_got_x_by_following_x
      SclerotizedEntry
      OuterPointer
  deriving Show

deleteSuperpositionEntry
  :: Pointer_To_Superposition
  -> ExceptT Error_DeleteSuperpositionEntry (State RuntimeStore) ()
deleteSuperpositionEntry (Pointer_To_Superposition outerPointer) = do

  --TODO do not do this in release mode
  payload
    <- wrapException Error_DeleteSuperpositionEntry
    $ lookupStoreEntry outerPointer

  _
    <- case payload of
          SuperpositionEntry _left_argument _right_argument
            -> return ()
          unexpectedEntry
            -> throwException
            $ Error_ExpectedSuperpositionEntry_but_got_x_by_following_x
                unexpectedEntry
                outerPointer

  wrapException Error_WhileReplacingSuperpositionEntry
    $ rawReplaceEntry outerPointer
    $ EmptyEntryBecauseGotDeleted





---



allocateNewEntry
  :: State RuntimeStore OuterPointer
allocateNewEntry = do
  RuntimeStore offset entries <- get

  put $ RuntimeStore offset $ entries ++ [FreshlyAllocatedEntry]

  return $ bedizen_translation offset $ genericLength entries



---


data Error_WhileRepairingBackReference
  = Error_WhileGettingEntryWithBackReference
      (Error_Lookup Pointer_To_LambdaPayload)

  | Error_WhileReplacingWithUpdatedLambda
      Error_ReplaceEntry

  deriving Show

data TheMoovedVariable
  = TheMoovedVariable
      WellKnownName
      WhereIsTheVariableBound

newtype WhereGotItMovedTo
  = WhereGotItMovedTo
      SubPointer

tellBackReferenceThatOccurrenceChanged
  :: TheMoovedVariable
  -> WhereGotItMovedTo
  -> ExceptT
      Error_WhileRepairingBackReference
      (State RuntimeStore)
      ()
tellBackReferenceThatOccurrenceChanged
  (TheMoovedVariable _variableName (WhereIsTheVariableBound pointer_To_LambdaPayload))
  (WhereGotItMovedTo newSubPointer)
  = do

  lambda_payload
    <- wrapException Error_WhileGettingEntryWithBackReference
    $ lookupLambdaPayload
    $ pointer_To_LambdaPayload

  case lambda_payload of
    TermWithPlaceholderVariable
      (WhereIsMyBoundedSoloVariable _oldSubPointer variableName)
      whereIsTheLambdaBody
      -> do
          let updated_lambda_playload
                = TermWithPlaceholderVariable
                    (WhereIsMyBoundedSoloVariable newSubPointer variableName)
                    whereIsTheLambdaBody

          let Pointer_To_LambdaPayload raw_pointer = pointer_To_LambdaPayload

          wrapException Error_WhileReplacingWithUpdatedLambda
            $ rawReplaceEntry raw_pointer
            $ LambdaEntry
            $ updated_lambda_playload

          return ()

