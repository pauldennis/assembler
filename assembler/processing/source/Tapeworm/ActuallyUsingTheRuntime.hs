module Tapeworm.ActuallyUsingTheRuntime where

import Control.Monad.Trans.State.Strict
import Control.Monad.Trans.Except
import Data.Either.Combinators (mapLeft)

import Assembler.Decompiler
import Assembler.Links.UntrustedExpression.UntrustedExpression
import Tapeworm.Evaluate
import Assembler.Links.DroppedMobile_to_Fragment.Fragment
import Assembler.Links.DroppedMobile_to_Fragment.RuntimeStore
import Assembler.CompileDecompile



data Error_Drive
  = Error_Drive Error_Forcing RuntimeStore
  deriving Show

drive
  :: Fragment
  -> Either Error_Drive Fragment
drive (Fragment root store)
  = tryRepackage
  $ runState (runExceptT (forceToNormalForm_withEndDebugTrace root)) store
  where
    tryRepackage (Right reentrance, got_store)
      = Right $ Fragment reentrance got_store
    tryRepackage (Left reentrance, got_store)
      = Left $ Error_Drive reentrance got_store

data Error_ForceTerm
  = Error_WhileDriving Error_Drive
  | Error_WhileDecompiling Error_Decompile
  deriving Show

tryForceTerm
  :: UntrustedExpression
  -> Either
      Error_ForceTerm
      UntrustedExpression
tryForceTerm term = do
  let object = compile term

  forced_object
    <- mapLeft Error_WhileDriving
        $ drive
        $ object

  mapLeft Error_WhileDecompiling
    $ decompile
    $ forced_object

forceTerm
  :: UntrustedExpression
  -> UntrustedExpression
forceTerm term
  = id
  $ either errorCase id
  $ tryForceTerm
  $ term
  where
    errorCase errorData
      = error
      $ "forceTerm failed with: "
      ++ show errorData
