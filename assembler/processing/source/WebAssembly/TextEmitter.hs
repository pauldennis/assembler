module WebAssembly.TextEmitter where

import Assembler.Links.DroppedMobile_to_Fragment.IndexOffsetting

import Data.Word


indentation :: String
indentation = "  "

addOneIndention :: [String] -> [String]
addOneIndention = map (indentation++)


newtype FunctionName
  = FunctionName String

newtype ReturnValues
  = ReturnValues [WebAssembly_ValueType]

noReturnValues :: ReturnValues
noReturnValues = ReturnValues []

oneReturnValue :: WebAssembly_ValueType -> ReturnValues
oneReturnValue x = ReturnValues [x]

function
  :: FunctionName
  -> ReturnValues
  -> [String]
  -> [String]
function
  (FunctionName functionName)
  (ReturnValues returnValues)
  body
  = result
  where
    result = []
      ++ ["(func $"++functionName++""]
      ++ addOneIndention (map resultStatement returnValues)
      ++ addOneIndention body
      ++ [")"]

    resultStatement returnType
      = ""
      ++ "(result "
      ++ toWebAssemblyNotation returnType
      ++ ")"




newtype VariableName
  = VariableName String

data ScopeType
  = Local
  | Global


data WebAssembly_ValueType
  = WebAssembly_Word32
  | WebAssembly_Word64

toWebAssemblyNotation :: WebAssembly_ValueType -> String
toWebAssemblyNotation WebAssembly_Word32 = "i32"
toWebAssemblyNotation WebAssembly_Word64 = "i64"



render_set_i64
  :: Word64
  -> ScopeType
  -> VariableName
  -> [String]
render_set_i64
  word64
  scopeType
  variableName
  = render_set
      WebAssembly_Word64
      (ValueLiteral $ show word64)
      scopeType
      variableName


render_set_local_i64
  :: Word64
  -> VariableName
  -> [String]
render_set_local_i64
  word64
  = render_set_global
      WebAssembly_Word64
      (ValueLiteral $ show word64)


render_set_global_i64
  :: Word64
  -> VariableName
  -> [String]
render_set_global_i64
  word64
  = render_set_global
      WebAssembly_Word64
      (ValueLiteral $ show word64)


render_set_global_i32
  :: Word32
  -> VariableName
  -> [String]
render_set_global_i32
  word32
  = render_set_global
      WebAssembly_Word32
      (ValueLiteral $ show word32)


newtype ValueLiteral
  = ValueLiteral String

render_set_global
  :: WebAssembly_ValueType
  -> ValueLiteral
  -> VariableName
  -> [String]
render_set_global
  valueType
  (ValueLiteral literal)
  (VariableName variableName)
  = [ "(global.set $"++variableName++""
    , "  ("++iN++".const "++literal++")"
    , ")"
    ]
  where
    iN = toWebAssemblyNotation valueType

render_set
  :: WebAssembly_ValueType
  -> ValueLiteral
  -> ScopeType
  -> VariableName
  -> [String]
render_set
  valueType
  (ValueLiteral literal)
  scopeType
  (VariableName variableName)
  = [ "("++showedScopeType++".set $"++variableName++""
    , "  ("++iN++".const "++literal++")"
    , ")"
    ]
  where
    iN = toWebAssemblyNotation valueType

    showedScopeType
      = case scopeType of
          Local -> "local"
          Global -> "global"





newtype AbsoluteLocation
  = AbsoluteLocation Word32

place_at_location_Word64
  :: OffsetTranslation
  -> AbsoluteLocation
  -> Word64
  -> [String]
place_at_location_Word64
  (OffsetTranslation offset)
  (AbsoluteLocation word32)
  word64
  = []
  ++ ["(i64.store"]
  ++ ["  (i32.const "++show word32++")" ++ " ;; position: " ++ help]
  ++ ["  (i64.const "++show word64++")" ++ " ;; value"]
  ++ [")"]
  where
    (rough_shift, fine_shift) = quotRem without_offset 8

    without_offset = (toInteger word32) - (toInteger offset)

    help = "(" ++ show word32 ++ " == " ++ show offset ++ " + " ++ show rough_shift ++ " * 8 + " ++ show fine_shift ++ ")"




examp :: IO ()
examp
  = putStrLn
  $ unlines
  $ result
  where
    result
      = function
          (FunctionName "functionName")
          noReturnValues
          body
    body = place_at_location_Word64 (OffsetTranslation 0) (AbsoluteLocation 0) maxBound




data WebAssemblyConstant
  = WebAssemblyConstant_I32 Word32
  | WebAssemblyConstant_I64 Word64

render_function_call
  :: FunctionName
  -> [WebAssemblyConstant]
  -> [String]
render_function_call
  (FunctionName name)
  constants
  = []
  ++ ["(call $" ++ name]
  ++ addOneIndention constant_arguments
  ++ [")"]
  where
    constant_arguments = constants >>= render_constant


render_constant
  :: WebAssemblyConstant
  -> [String]
render_constant (WebAssemblyConstant_I32 word) = ["(i32.const "++show word++")"]
render_constant (WebAssemblyConstant_I64 word) = ["(i64.const "++show word++")"]


constant_function
  :: FunctionName
  -> Word64
  -> [String]
constant_function
  name
  integer
  = result
  where
    result
      = function
          name
          (ReturnValues returnValue)
          body

    returnValue = [WebAssembly_Word64]

    body = constStatement integer

constStatement
  :: Word64
  -> [String]
constStatement
  integer
  = [result]
  where
    result = "(i64.const "++show integer++")"
