module TestSuite where

import Trifle.Test

import Tapeworm.PortingDriverTests
import Assembler.CompileDecompile
import Assembler.Links.PointerArrayFragment_to_BitPointerArrayFragment.TagAssignment
import Assembler.Compiler
import HandCompiled.TickTackToe
import HandCompiled.PointCloudFilter
import Assembler.LinkableFragment



mainTestSuite :: IO ()
mainTestSuite = do
  print "it runs therefore it compiles"
  runTestCase test_example
  runTestCase test_compile_fragment
  runTestCase test_compile_module_ball
  runTestCase test_compile_module_squircle
  runTestCase test_compile_is_bijective
  runTestCase test_tag_assignment_consistency

  runTestCase test_development_driver_0_LeafConstructor
  runTestCase test_development_driver_1_OneTimeDubplicate
  runTestCase test_development_driver_2_identity_lambda
  runTestCase test_development_driver_3_nested_lambda
  runTestCase test_development_driver_4_multiple_nested_lambda
  runTestCase test_development_driver_5_multiple_time_duplicate
  runTestCase test_development_driver_6_duplicate_identity_lambda
  runTestCase test_development_driver_7_duplicate_nested_lambda
  runTestCase test_development_driver_8_duplicate_multiple_nested_lambda
  runTestCase test_development_driver_9_doubling_function
  runTestCase test_development_driver_10_shared_computation_in_lambda
  runTestCase test_development_driver_11_corrolary_application_as_child
--   runTestCase test_development_driver_12_shadowing_variable


  runTestCase test_compile_example_PointFilter_Ball
  runTestCase test_compile_example_PointFilter_Squircle
  runTestCase test_compile_example_TickTackToe

  return ()

