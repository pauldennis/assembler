module Word.Word2 where


data Word2
  = Word2_0
  | Word2_1
  deriving (Eq, Show)
