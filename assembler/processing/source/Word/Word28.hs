{-# LANGUAGE DeriveDataTypeable #-}

-- TODO this module is bogus

module Word.Word28 where

import GHC.Generics

import Numeric.Natural
import Data.Data
import Data.Bits


data Word28
  = Word28
      Bool Bool Bool Bool
      Bool Bool Bool Bool
      Bool Bool Bool Bool
      Bool Bool Bool Bool
      Bool Bool Bool Bool
      Bool Bool Bool Bool
      Bool Bool Bool Bool
  deriving (Generic, Eq, Ord, Data, Read)

pretty_show_Word28 :: Word28 -> String
pretty_show_Word28
  (Word28
    bool0 bool1 bool2 bool3
    bool4 bool5 bool6 bool7
    bool8 bool9 bool10 bool11
    bool12 bool13 bool14 bool15

    bool16 bool17 bool18 bool19
    bool20 bool21 bool22 bool23
    bool24 bool25 bool26 bool27
  )
  = map (\c->if c then '1' else '0')
      $ [bool0, bool1, bool2, bool3, bool4, bool5, bool6, bool7, bool8, bool9, bool10, bool11, bool12, bool13, bool14, bool15, bool16, bool17, bool18, bool19, bool20, bool21, bool22, bool23, bool24, bool25, bool26, bool27]

number_of_bits_Word28 :: Natural
number_of_bits_Word28 = 28


number_of_patterns_Word28 :: Int
number_of_patterns_Word28 = 2^number_of_bits_Word28


instance Show Word28 where
  show word28
    = (\string -> "(" ++ string ++ ")")
    $ ("read_LabelPayload " ++)
    $ show
    $ "LabelPayload_"
    ++ pretty_show_Word28 word28

read_LabelPayload
  :: String
  -> Word28
read_LabelPayload
  _
  = error "missing implementation"


instance Enum Word28 where
  toEnum int
    = if 0 <= int && int < number_of_patterns_Word28
        then Word28
              (testBit int 0)
              (testBit int 1)
              (testBit int 2)
              (testBit int 3)
              (testBit int 4)
              (testBit int 5)
              (testBit int 6)
              (testBit int 7)
              (testBit int 8)
              (testBit int 9)
              (testBit int 10)
              (testBit int 11)
              (testBit int 12)
              (testBit int 13)
              (testBit int 14)
              (testBit int 15)
              (testBit int 16)
              (testBit int 17)
              (testBit int 18)
              (testBit int 19)
              (testBit int 20)
              (testBit int 21)
              (testBit int 22)
              (testBit int 23)
              (testBit int 24)
              (testBit int 25)
              (testBit int 26)
              (testBit int 27)
        else error
          $ show
          $
          ( "number to big for enum: "
          , int
          , ("max (exclusive) is", number_of_patterns_Word28)
          , ("minimum inclusive is", 0::Int)
          )
  fromEnum
    ( Word28
      bool0 bool1 bool2 bool3
      bool4 bool5 bool6 bool7
      bool8 bool9 bool10 bool11
      bool12 bool13 bool14 bool15

      bool16 bool17 bool18 bool19
      bool20 bool21 bool22 bool23
      bool24 bool25 bool26 bool27
    )
    = fromBoolList
    [
      bool0, bool1, bool2, bool3, bool4, bool5, bool6, bool7, bool8, bool9, bool10, bool11, bool12, bool13, bool14, bool15, bool16, bool17, bool18, bool19, bool20, bool21, bool22, bool23, bool24, bool25, bool26, bool27
    ]


fromBoolList
  :: [Bool]
  -> Int
fromBoolList
  [
    bool0, bool1, bool2, bool3, bool4, bool5, bool6, bool7, bool8, bool9, bool10, bool11, bool12, bool13, bool14, bool15, bool16, bool17, bool18, bool19, bool20, bool21, bool22, bool23, bool24, bool25, bool26, bool27
  ]
  = zeroBits
  .|. (if bool0 then bit 0 else zeroBits)
  .|. (if bool1 then bit 1 else zeroBits)
  .|. (if bool2 then bit 2 else zeroBits)
  .|. (if bool3 then bit 3 else zeroBits)
  .|. (if bool4 then bit 4 else zeroBits)
  .|. (if bool5 then bit 5 else zeroBits)
  .|. (if bool6 then bit 6 else zeroBits)
  .|. (if bool7 then bit 7 else zeroBits)
  .|. (if bool8 then bit 8 else zeroBits)
  .|. (if bool9 then bit 9 else zeroBits)
  .|. (if bool10 then bit 10 else zeroBits)
  .|. (if bool11 then bit 11 else zeroBits)
  .|. (if bool12 then bit 12 else zeroBits)
  .|. (if bool13 then bit 13 else zeroBits)
  .|. (if bool14 then bit 14 else zeroBits)
  .|. (if bool15 then bit 15 else zeroBits)
  .|. (if bool16 then bit 16 else zeroBits)
  .|. (if bool17 then bit 17 else zeroBits)
  .|. (if bool18 then bit 18 else zeroBits)
  .|. (if bool19 then bit 19 else zeroBits)
  .|. (if bool20 then bit 20 else zeroBits)
  .|. (if bool21 then bit 21 else zeroBits)
  .|. (if bool22 then bit 22 else zeroBits)
  .|. (if bool23 then bit 23 else zeroBits)
  .|. (if bool24 then bit 24 else zeroBits)
  .|. (if bool25 then bit 25 else zeroBits)
  .|. (if bool26 then bit 26 else zeroBits)
  .|. (if bool27 then bit 27 else zeroBits)
fromBoolList
  xs
  = error $ "list has not exact numer of elements: " ++ show xs


instance Bounded Word28 where
  minBound = toEnum 0
  maxBound = toEnum $ number_of_patterns_Word28 - 1


data Error_ConversionOf_Natural_to_Word28
  = Error_Overflow_got_Natural_to_Word28_x_but_lower_bound_is_x_and_upper_bound_is_x
      Natural
      Word28
      Word28

try_convert_Natural_to_Word28
  :: Natural
  -> Either Error_ConversionOf_Natural_to_Word28 Word28
try_convert_Natural_to_Word28
  natural
  = result
  where
    is_representable
      = natural
      <= (fromInteger $ toInteger $ fromEnum (maxBound::Word28))

    result
      = if is_representable
          then Right $ toEnum $ fromEnum natural
          else Left $ Error_Overflow_got_Natural_to_Word28_x_but_lower_bound_is_x_and_upper_bound_is_x natural minBound maxBound
