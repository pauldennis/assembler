module Trifle.Missing where

import Data.Containers.ListUtils
import Data.List.Extra hiding (nubOrd)
import Safe.Partial
import GHC.Stack

import Trifle.Patchwork

isNubbed :: Ord a => [a] -> Bool
isNubbed list_with_maybe_duplicates = result
  where
    result
      = (==) (trivialize nubbedList)
      (trivialize list_with_maybe_duplicates)

    nubbedList = nubOrd list_with_maybe_duplicates

    trivialize = fmap $ const ()


-- TODO what about the ordering of the original list?
duplicatesOrd
  :: (Partial, Ord a)
  => [a]
  -> [EquivalenceClass a]
duplicatesOrd
  occurrences
  = withFrozenCallStack
  $ map equivalenceClass
  $ filter (not . isSingleton)
  $ group
  $ sort
  $ occurrences


equivalenceClass :: [a] -> EquivalenceClass a
equivalenceClass (x:xs) = EquivalenceClass x xs
equivalenceClass _ = undefined



data EquivalenceClass a
  = EquivalenceClass a [a]
  deriving Show

represant :: EquivalenceClass a -> a
represant (EquivalenceClass x _) = x

-- NOTE we loose the information that the list is not empty
equivalenceClassAsList
  :: EquivalenceClass a
  -> [a]
equivalenceClassAsList
  (EquivalenceClass x xs)
  = x
  : xs


isSingletonEquivalenceClass
  :: EquivalenceClass a
  -> Bool
isSingletonEquivalenceClass
  (EquivalenceClass _representative others)
  = null
  $ others

classify
  :: (Partial, Eq a)
  => [a]
  -> [EquivalenceClass a]
classify
  occurrences
  = withFrozenCallStack
  $ classifyBy (==)
  $ occurrences


classifyBy
  :: Partial
  => (a -> a -> Bool)
  -> [a]
  -> [EquivalenceClass a]
classifyBy by occurrence
  = withFrozenCallStack
  $ map equivalenceClass
  $ classifyBy' by
  $ occurrence
  where
    -- https://hackage.haskell.org/package/leancheck-1.0.0/docs/src/Test.LeanCheck.Stats.html#classifyBy
    classifyBy' _ [] = []
    classifyBy' isEqual (x:xs)
      = (x : filter (isEqual x) xs)
      : classifyBy' isEqual (filter (isNotEqual x) xs)
      where
        isNotEqual left right = not (isEqual left right)


lookups
  :: Eq a
  => a
  -> [(a, b)]
  -> [b]
lookups
  key
  mappings
  = map snd
  $ filter ((== key) . fst)
  $ mappings
