module Trifle.Rational where

import Data.Ratio


--https://stackoverflow.com/questions/30931369/how-to-convert-a-rational-into-a-pretty-string

showRationalFinite :: Int -> Rational -> String
showRationalFinite len rat = (if num < 0 then "-" else "") ++ (shows d ("." ++ take len (go next)))
    where
        (d, next) = abs num `quotRem` den
        num = numerator rat
        den = denominator rat

        go 0 = ""
        go x = let (dd, nextnext) = (10 * x) `quotRem` den
               in shows dd (go nextnext)

showRationalInfinit :: Rational -> String
showRationalInfinit
  rat
  = (if num < 0 then "-" else "")
  ++ (shows d ("." ++ (go next)))
    where
        (d, next) = abs num `quotRem` den
        num = numerator rat
        den = denominator rat

        go 0 = ""
        go x = let (dd, nextnext) = (10 * x) `quotRem` den
               in shows dd (go nextnext)
