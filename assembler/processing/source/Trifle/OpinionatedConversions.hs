module Trifle.OpinionatedConversions where

import Numeric.Natural

import Data.Word

try_convert_Integer_to_Word64
  :: Integer
  -> Maybe Word64
try_convert_Integer_to_Word64
  integer
  = result
  where
    result
      = if isRepresentable
          then Just $ fromInteger integer
          else Nothing

    isRepresentable
      = True
      && lower_bound <= integer
      && integer <= upper_bound

    lower_bound = toInteger (minBound :: Word64)
    upper_bound = toInteger (maxBound :: Word64)


try_convert_Integer_to_Word32
  :: Integer
  -> Maybe Word32
try_convert_Integer_to_Word32
  integer
  = result
  where
    result
      = if isRepresentable
          then Just $ fromInteger integer
          else Nothing

    isRepresentable
      = True
      && lower_bound <= integer
      && integer <= upper_bound

    lower_bound = toInteger (minBound :: Word32)
    upper_bound = toInteger (maxBound :: Word32)


try_convert_Natural_to_Word32
  :: Natural
  -> Maybe Word32
try_convert_Natural_to_Word32
  natural
  = result
  where
    result
      = if isRepresentable
          then Just $ fromInteger $ toInteger natural
          else Nothing

    isRepresentable
      = True
      && natural <= upper_bound

    upper_bound = fromInteger $ toInteger (maxBound :: Word32)



try_convert_Natural_to_Word64
  :: Natural
  -> Maybe Word64
try_convert_Natural_to_Word64
  natural
  = result
  where
    result
      = if isRepresentable
          then Just $ fromInteger $ toInteger natural
          else Nothing

    isRepresentable
      = True
      && natural <= upper_bound

    upper_bound = fromInteger $ toInteger (maxBound :: Word64)
