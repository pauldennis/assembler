{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE DataKinds #-}

module Trifle.Ramificable where

import Debug.Trace

import Data.Tree
import Data.Kind

import Numeric.Natural
import Data.List.NonEmpty( NonEmpty( (:|) ) )
import qualified Data.List.NonEmpty as NonEmptyList

import GHC.Generics

import Trifle.Rename_Generic


class Ramificable a where
  ramify :: a -> Tree String

  default ramify
    :: (Generic a, Generalized_ForrestShow (Rep a))
    => a
    -> Tree String

  ramify = ramify_forall_generalized


instance Ramificable a => Ramificable [a] where
  ramify xs = Node "[]" $ map ramify xs

instance Ramificable Natural where
  ramify number = return $ show number

instance Ramificable a => Ramificable (NonEmptyList.NonEmpty a) where
  ramify (x :| xs) = ramify (x : xs)

instance Ramificable Bool where
  ramify = ramify_forall_generalized

instance Ramificable () where
  ramify = ramify_forall_generalized

instance (Ramificable a, Ramificable b) => Ramificable (a,b) where
  ramify = ramify_forall_generalized

instance Ramificable (Tree String) where
  ramify = id




----





ramify_tree_generalized
  :: forall {k} (f :: k -> Type) (a :: k)
  . Generalized_ForrestShow f
  => f a
  -> Tree String
ramify_tree_generalized = theOneTree . ramify_generalized
  where
    theOneTree [x] = x
    theOneTree xs = error $ "not exactly one tree in forest: " ++ show xs

ramify_forall_generalized
  :: (Generalized_ForrestShow (Rep a), Generic a)
    => a
    -> Tree String
ramify_forall_generalized = ramify_tree_generalized . from







class Generalized_ForrestShow f where
  ramify_generalized :: f a -> [Tree String]

instance Generalized_ForrestShow Unit where
  ramify_generalized Unit = []

instance
  (Generalized_ForrestShow a)
  => Generalized_ForrestShow (MetaInfo DatatypeTag c a)
  where
    ramify_generalized (MetaInfo x) = ramify_generalized x

instance
  (Constructor c, Generalized_ForrestShow a)
  => Generalized_ForrestShow (MetaInfo ConstructorTag c a) where
  ramify_generalized xx@(MetaInfo x)
    = return
    $ Node (constructorName xx)
    $ ramify_generalized
    $ x

instance (Generalized_ForrestShow a, Generalized_ForrestShow b) => Generalized_ForrestShow (a :+: b) where
  ramify_generalized (LeftChoice x) = ramify_generalized x
  ramify_generalized (RightChoice x) = ramify_generalized x

instance (Generalized_ForrestShow a, Generalized_ForrestShow b) => Generalized_ForrestShow (a :*: b) where
  ramify_generalized
    (a :*: b)
    = Prelude.concat
        [ ramify_generalized a
        , ramify_generalized b
        ]

instance (Generalized_ForrestShow a) => Generalized_ForrestShow (MetaInfo RecordSelector c a) where
  ramify_generalized (MetaInfo x) = ramify_generalized x

instance (Ramificable a) => Generalized_ForrestShow (BuildingBlock i a) where
  ramify_generalized (BuildingBlock x) = return $ ramify x






---





ramifiedTreeShow
  :: Tree String
  -> [String]
ramifiedTreeShow
  (Node label children) = result
  where
    result = header : body

    header = label

    body = id
      $ map ("  "++)
      $ ramifiedForrestShow
      $ children

ramifiedForrestShow
  :: [Tree String]
  -> [String]
ramifiedForrestShow forrest = forrest >>= ramifiedTreeShow

showRamifyForrest
  :: [Tree String]
  -> String
showRamifyForrest = unlines . ramifiedForrestShow

linesRamificable
  :: Ramificable a
  => a
  -> [String]
linesRamificable
  = ramifiedTreeShow
  . ramify


showRamificable
  :: Ramificable a
  => a
  -> String
showRamificable
  = unlines
  . linesRamificable


printRamificable
  :: Ramificable a
  => a
  -> IO ()
printRamificable x = result
  where
    result = id
      $ mapM_ putStrLn
      $ linesRamificable
      $ x

traceRamificable
  :: Ramificable toBeTraced
  => toBeTraced
  -> returnValue
  -> returnValue
traceRamificable
  toBeTraced
  output
  = trace text output
  where
    text = showRamificable toBeTraced





emptyLine :: [Tree String]
emptyLine = [Node "" []]
