module Trifle.BitList where

import Numeric.Natural
import Data.Bits
import Data.Word
import Data.List


toBitList
  :: FiniteBits a
  => a
  -> [Bool]
toBitList bitFlield
  = result
  where
    result = map (testBit bitFlield) indexes

    number_of_bits = finiteBitSize bitFlield

    indexes = [0..number_of_bits-1]



fromBitList
  :: [Bool]
  -> Natural
fromBitList
  boolList
  = result
  where
    result = foldr (.|.) 0 bitStencils

    bitStencils = zip [0..] boolList >>= maybeBit

    maybeBit (_, False) = []
    maybeBit (i, True) = [bit i]



showFiniteBits
  :: FiniteBits a
  => a
  -> [Char]
showFiniteBits = showBitList . toBitList

showWord64Bits
  :: Word64
  -> [Char]
showWord64Bits = showFiniteBits

showWord32Bits
  :: Word32
  -> [Char]
showWord32Bits = showFiniteBits



showBitList
  :: [Bool]
  -> [Char]
showBitList = map oneLetterBool
  where
    oneLetterBool False = '0'
    oneLetterBool True = '1'

convertBitListRepresentation
  :: [Char]
  -> [Bool]
convertBitListRepresentation = map oneLetterBool
  where
    oneLetterBool '0' = False
    oneLetterBool '1' = True
    oneLetterBool x = error $ "unexpected character: \'" ++  show x ++ "\'"


readBitList
  :: [Char]
  -> Natural
readBitList
  list
  = result
  where
    result = fromBitList boolList

    boolList = map interpret list

    interpret '0' = False
    interpret '1' = True
    interpret x = error $ "expected 0 or 1 as charactor but got: " ++ show x


newtype Error_LengthNot64
  = Error_LengthNot64 Natural
  deriving (Show)

bitList64ToWord64
  :: [Bool]
  -> Either Error_LengthNot64 Word64
bitList64ToWord64 bitList
  = if 64 == length bitList
      then Right $ fromInteger $ toInteger $ fromBitList $ bitList
      else Left $ Error_LengthNot64 $ genericLength bitList


newtype Error_LengthNot32
  = Error_LengthNot32 Natural
  deriving (Show)

bitList32ToWord32
  :: [Bool]
  -> Either Error_LengthNot32 Word32
bitList32ToWord32 bitList
  = if 32 == length bitList
      then Right $ fromInteger $ toInteger $ fromBitList $ bitList
      else Left $ Error_LengthNot32 $ genericLength bitList


readBitString32ToWord32 :: String -> Either Error_LengthNot32 Word32
readBitString32ToWord32 = bitList32ToWord32 . convertBitListRepresentation


