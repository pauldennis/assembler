module HandCompiled.NumericExpression where


import Assembler.Links.UntrustedExpression.UntrustedExpression
import TermBuildingBlocks.TermBuildingBlocks

import Assembler.Weft.UntrustedModule
import HandCompiled.Represent_as_CPP
-- import qualified Data.ByteString.Lazy
-- import Assembler.Compiler
-- import Data.Aeson
import Assembler.Serialisation.JSON()


curve_simple_module :: IO ()
curve_simple_module = print curve_simple

curve_slightly_more_complicated_module :: IO ()
curve_slightly_more_complicated_module = print curve_slightly_more_complicated

perle_pennant_module :: IO ()
perle_pennant_module = print perle_pennant

perle_cube_module :: IO ()
perle_cube_module = print perle_cube





c_code_ball_filter_root :: IO ()
c_code_ball_filter_root = expression_c_code_root ball_filter
c_code_ball_filter_melee :: IO ()
c_code_ball_filter_melee = expression_c_code_melee ball_filter

c_code_some_curve_root :: IO ()
c_code_some_curve_root = expression_c_code_root simple_curve
c_code_some_curve_melee :: IO ()
c_code_some_curve_melee = expression_c_code_melee simple_curve

c_code_station_platform_root :: IO ()
c_code_station_platform_root = expression_c_code_root station_platform_interface
c_code_station_platform_melee :: IO ()
c_code_station_platform_melee = expression_c_code_melee station_platform_interface






--------










ball_filter :: UntrustedExpression
ball_filter
  = lambda "x"
  $ lambda "y"
  $ lambda "z"
  $ lessThan
      (addition_down
        (addition_down
          (square_down $ placeholder "x")
          (square_down $ placeholder "y")
        )
        (square_down $ placeholder "z")
      )
      (float32 1)




squircle_filter :: UntrustedExpression
squircle_filter
  = lambda "x"
  $ lambda "y"
  $ lambda "z"
  $ lessThan
      (addition_down
        (addition_down
          (square_down $ square_down $ placeholder "x")
          (square_down $ square_down $ placeholder "y")
        )
        (square_down $ square_down $ placeholder "z")
      )
      (float32 1)





--------





simple_curve :: UntrustedExpression
simple_curve
  = lambda "x"
  $ duplicate3 "x"
  $ constructor3 "Vector3"
      (placeholder "x1")
      (square_down $ placeholder "x21")
      (square_down $ square_down $ placeholder "x22")





slightly_more_complicated_curve :: UntrustedExpression
slightly_more_complicated_curve
  = lambda "x"
  $ duplicate3 "x"
  $ constructor3 "Vector3"
      (placeholder "x1")
      (square_down $ square_down $ placeholder "x21")
      (square_down $ square_down $ square_down $ square_down $ square_down $ square_down $ placeholder "x22")




---------





pennant_interface :: UntrustedExpression
pennant_interface
  = lambda "curve"
  $ lambda "epsilon"
  $ lambda "station"
  $ duplicate2 "curve"
  $ duplicate2 "station"
  $ let
      anchor = apply (placeholder "curveL") (placeholder "stationL")
    in id
  $ list . return
  $ duplicate "anchorL" "anchorR" anchor
  $ let
      neighbour_station = addition_down (placeholder "stationR") (placeholder "epsilon")
      neighbor_anchor = apply (placeholder "curveR") neighbour_station
    in id
  $ duplicate "other_anchor" "for_midpoint" neighbor_anchor
  $ let
      midpoint = call "Vector3_Midpoint_floor" (placeholder "for_midpoint") [placeholder "anchorR"]

      a = Vector3_Expression $ placeholder "anchorL"
      b = Vector3_Expression $ placeholder "other_anchor"
      c = Vector3_Expression
        $ call "Vector3_Subtract_floor"
            (midpoint)
            [deleteType $ Vector3 (Float32 0) (Float32 0.0) (Float32 0.6)]
    in id
  $ deleteType (Triangle a b c)








---------








{-
  anchor := curve( station )
  neighbour := curve( station + epsilon )

  vector_minus (Vector3 x0 x1 x2) (Vector3 y0 y1 y2) = Vector3 (x0-y0) (x1-y1) (x2-y2)

  in_direction_of_travel
    := normalize(neighbour - anchor)

  in_direction_of_travel_right
    := cross_product(Vector3(0,0,1), in_direction_of_travel)

  in_direction_of_travel_spiked_helmetephemeral
    := normalize(in_direction_of_travel_right, in_direction_of_travel)

  -- perspective of the train
  corner_DRF
    := in_direction_of_travel
    + in_direction_of_travel_right
    - in_direction_of_travel_spiked_helmet
  -- ...

  -- triangulate
  front := [Triangle URF DRF DLF, Triangle ...]
  -- ...

  result := [front, ...]
-}
station_platform_interface :: UntrustedExpression
station_platform_interface
  = lambda "curve" $ lambda "epsilon" $ lambda "station"

  $ duplicatePlaceholder "curve1" "curve2" "curve"
  $ duplicatePlaceholder "station1" "station2" "station"

  $ let
      anchor = apply (placeholder "curve1") (placeholder "station1")
    in id

  $ duplications ["anchor1", "anchor3", "anchor4", "anchor5", "anchor6"] anchor

  $ let
      neighbour_station = addition_down (placeholder "station2") (placeholder "epsilon")
      neighbor_anchor = apply (placeholder "curve2") neighbour_station
      from_to
        = call1 "Vector3_Normalize_floor"
        $ call "Vector3_Subtract_floor" (neighbor_anchor) [placeholder "anchor3"]
    in id

  $ duplications ["from_to1", "from_to2", "from_to3"] from_to

  $ let
      to_the_right
        = call1 "Vector3_Normalize_floor"
        $ call "Vector3_CrossProduct_floor" (placeholder "from_to1") [vector3 (float32 0) (float32 0) (float32 1)]
    in id

  $ duplications ["to_the_right1", "to_the_right2"] to_the_right

  $ let
      differentiation
        = ($ [float32 0.2]) $ call "Vector3_Scale_flipped"
        $ call1 "Vector3_Normalize_floor"
        $ placeholder "from_to2"

      spiked_helmet
        = ($ [float32 0.2]) $ call "Vector3_Scale_flipped"
        $ call "Vector3_CrossProduct_floor" (placeholder "to_the_right1") [placeholder "from_to3"]

    in id

  $ duplications ["differentiation1", "differentiation2"] differentiation

  $ duplications ["spiked_helmet_LB", "spiked_helmet_RB"] spiked_helmet
    $ duplications ["spiked_helmet1", "spiked_helmet2"] (placeholder "spiked_helmet_LB")
    $ duplications ["spiked_helmet3", "spiked_helmet4"] (placeholder "spiked_helmet_RB")

  $ let
      referenceTriangle = Triangle a b c
        where
          a = Vector3_Expression $ placeholder "anchor1"
          b = Vector3_Expression $ call "Vector3_Add_floor" (placeholder "anchor4") [placeholder "differentiation1"]
          c = Vector3_Expression $ call "Vector3_Add_floor" (placeholder "anchor5") [placeholder "spiked_helmet1"]
    in id

  $ duplications (map (\x->"anchor"++show @Integer x) [7..15+3+3*(4+2+2+2)]) (placeholder "anchor6")

  $ let
      to_right
        = ($ [float32 0.2]) $ call "Vector3_Scale_flipped"
        $ placeholder "to_the_right2"
      to_forward = placeholder "differentiation2"
    in id

  $ duplications ["to_right_LB", "to_right_RB"] to_right
    $ duplications ["to_right_LB_LB", "to_right_LB_RB"] (placeholder "to_right_LB")
      $ duplications ["to_right1", "to_right2"] (placeholder "to_right_LB_LB")
      $ duplications ["to_right3", "to_right4"] (placeholder "to_right_LB_RB")
    $ duplications ["to_right_RB_LB", "to_right_RB_RB"] (placeholder "to_right_RB")
      $ duplications ["to_right5", "to_right6"] (placeholder "to_right_RB_LB")
      $ duplications ["to_right7", "to_right8", "to_right9", "to_right10", "to_right11", "to_right12", "to_right13", "to_right14", "to_right15", "to_right16", "to_right17", "to_right18", "to_right19", "to_right20"] (placeholder "to_right_RB_RB")

  $ duplications ["to_forward_LB", "to_forward_RB"] to_forward
    $ duplications ["to_forward_LB_LB", "to_forward_LB_RB"] (placeholder "to_forward_LB")
      $ duplications ["to_forward1", "to_forward2"] (placeholder "to_forward_LB_LB")
      $ duplications ["to_forward3", "to_forward4"] (placeholder "to_forward_LB_RB")
    $ duplications ["to_forward_RB_LB", "to_forward_RB_RB"] (placeholder "to_forward_RB")
      $ duplications ["to_forward5", "to_forward7"] (placeholder "to_forward_RB_LB")
      $ duplications ["to_forward8", "to_forward9", "to_forward10", "to_forward11", "to_forward12", "to_forward13", "to_forward14", "to_forward15", "to_forward16", "to_forward17", "to_forward18", "to_forward19", "to_forward20", "to_forward21"] (placeholder "to_forward_RB_RB")

  $ duplications ["spiked_helmet2_LB_LB", "spiked_helmet2_LB_RB"] (placeholder "spiked_helmet2")
    $ duplications ["to_up1", "to_up2", "to_up3"] (placeholder "spiked_helmet2_LB_LB")
    $ duplications ["to_up4", "to_up5", "to_up6", "to_up7", "to_up8", "to_up9", "to_up10", "to_up11", "to_up12", "to_up13", "to_up14", "to_up15", "to_up16", "to_up17", "to_up18"] (placeholder "spiked_helmet2_LB_RB")

  $ let
      to_left
        = call1 "Vector3_Negate"
        $ placeholder "to_right2"
      to_backwards
        = call1 "Vector3_Negate"
        $ placeholder "to_forward1"
      to_down
        = call1 "Vector3_Negate"
        $ placeholder "spiked_helmet3"
    in id


  $ duplications ["to_left_LB", "to_left_RB"] to_left
    $ duplications ["to_left1", "to_left2", "to_left3", "to_left4"] (placeholder "to_left_LB")
    $ duplications ["to_left5", "to_left6", "to_left7", "to_left8", "to_left9", "to_left10", "to_left11", "to_left12", "to_left13", "to_left14", "to_left15", "to_left16", "to_left17", "to_left18", "to_left19"] (placeholder "to_left_RB")


  $ duplications ["to_backwards_LB", "to_backwards_RB"] to_backwards
    $ duplications ["to_backwards1", "to_backwards2", "to_backwards3"] (placeholder "to_backwards_LB")
    $ duplications ["to_backwards_RB_LB", "to_backwards_RB_RB"] (placeholder "to_backwards_RB")
      $ duplications ["to_backwards4", "to_backwards5"] (placeholder "to_backwards_RB_LB")
      $ duplications ["to_backwards6", "to_backwards7", "to_backwards8", "to_backwards9", "to_backwards10", "to_backwards11", "to_backwards12", "to_backwards13", "to_backwards14", "to_backwards15", "to_backwards16", "to_backwards17", "to_backwards18", "to_backwards19"] (placeholder "to_backwards_RB_RB")

  $ duplications ["to_down_LB", "to_down_RB"] to_down
    $ duplications ["to_down1", "to_down2", "to_down3"] (placeholder "to_down_LB")
    $ duplications ["to_down4", "to_down5", "to_down6", "to_down7", "to_down8", "to_down9" , "to_down10" , "to_down11" , "to_down12" , "to_down13" , "to_down14" , "to_down15", "to_down16" , "to_down17" , "to_down18" ] (placeholder "to_down_RB")

  $ let
      -- TODO maybe delete later, keep for now so that variables are used for now
      more_directions = Triangle a b c
        where
          a = Vector3_Expression $ placeholder "anchor7"
          b = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "spiked_helmet4")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward2")
                $ placeholder "anchor8"
          c = Vector3_Expression $ call "Vector3_Add_floor" (placeholder "anchor9") [placeholder "to_right1"]
      more_directions_negated = Triangle a b c
        where
          a = Vector3_Expression $ placeholder "anchor10"
          b = Vector3_Expression $ call "Vector3_Add_floor" (placeholder "anchor11") [placeholder "to_left1"]
          c = Vector3_Expression $ call "Vector3_Add_floor" (placeholder "anchor12") [placeholder "to_backwards1"]


      side_BOTTOM_BACK_LEFT = Triangle a b c
        where
          a = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down1")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards3")
                $ call2 "Vector3_Add_floor" (placeholder "to_left3")
                $ (placeholder "anchor13")
          b = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down2")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward3")
                $ call2 "Vector3_Add_floor" (placeholder "to_left2")
                $ (placeholder "anchor14")
          c = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down3")
                $ call2 "Vector3_Add_floor" (placeholder "to_right3")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards2")
                $ (placeholder "anchor15")


      side_LID_BACK_LEFT = Triangle a b c
        where
          a = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up1")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards4")
                $ call2 "Vector3_Add_floor" (placeholder "to_left4")
                $ (placeholder "anchor16")
          b = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up2")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward4")
                $ call2 "Vector3_Add_floor" (placeholder "to_left5")
                $ (placeholder "anchor17")
          c = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up3")
                $ call2 "Vector3_Add_floor" (placeholder "to_right4")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards5")
                $ (placeholder "anchor18")

      --

      side_BOTTOM_FRONT_RIGHT = Triangle a b c
        where
          a = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down6")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward5")
                $ call2 "Vector3_Add_floor" (placeholder "to_right5")
                $ (placeholder "anchor19")
          b = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down4")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward7")
                $ call2 "Vector3_Add_floor" (placeholder "to_left6")
                $ (placeholder "anchor20")
          c = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down5")
                $ call2 "Vector3_Add_floor" (placeholder "to_right6")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards6")
                $ (placeholder "anchor21")


      side_LID_FRONT_RIGHT = Triangle a b c
        where
          a = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up4")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward8")
                $ call2 "Vector3_Add_floor" (placeholder "to_right7")
                $ (placeholder "anchor22")
          b = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up5")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward9")
                $ call2 "Vector3_Add_floor" (placeholder "to_left7")
                $ (placeholder "anchor23")
          c = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up6")
                $ call2 "Vector3_Add_floor" (placeholder "to_right8")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards7")
                $ (placeholder "anchor24")

      --

      side_LEFT_SIDE_FRONT_DOWN = Triangle a b c
        where
          a = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down7")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward10")
                $ call2 "Vector3_Add_floor" (placeholder "to_left8")
                $ (placeholder "anchor25")
          b = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up7")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward11")
                $ call2 "Vector3_Add_floor" (placeholder "to_left9")
                $ (placeholder "anchor26")
          c = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down8")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards8")
                $ call2 "Vector3_Add_floor" (placeholder "to_left10")
                $ (placeholder "anchor27")


      side_LEFT_SIDE_BACK_UP = Triangle a b c
        where
          a = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up8")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards9")
                $ call2 "Vector3_Add_floor" (placeholder "to_left11")
                $ (placeholder "anchor28")
          b = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up9")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward12")
                $ call2 "Vector3_Add_floor" (placeholder "to_left12")
                $ (placeholder "anchor29")
          c = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down9")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards10")
                $ call2 "Vector3_Add_floor" (placeholder "to_left13")
                $ (placeholder "anchor30")

      --

      side_RIGHT_SIDE_FRONT_DOWN = Triangle a b c
        where
          a = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down10")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward13")
                $ call2 "Vector3_Add_floor" (placeholder "to_right9")
                $ (placeholder "anchor31")
          b = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up10")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward14")
                $ call2 "Vector3_Add_floor" (placeholder "to_right10")
                $ (placeholder "anchor32")
          c = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down11")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards11")
                $ call2 "Vector3_Add_floor" (placeholder "to_right11")
                $ (placeholder "anchor33")


      side_RIGHT_SIDE_BACK_UP = Triangle a b c
        where
          a = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up11")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards12")
                $ call2 "Vector3_Add_floor" (placeholder "to_right12")
                $ (placeholder "anchor34")
          b = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up12")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward15")
                $ call2 "Vector3_Add_floor" (placeholder "to_right13")
                $ (placeholder "anchor35")
          c = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down12")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards13")
                $ call2 "Vector3_Add_floor" (placeholder "to_right14")
                $ (placeholder "anchor36")

      --

      side_FRONT_SIDE_LEFT_DOWN = Triangle a b c
        where
          a = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down13")
                $ call2 "Vector3_Add_floor" (placeholder "to_left14")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward16")
                $ (placeholder "anchor37")
          b = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up13")
                $ call2 "Vector3_Add_floor" (placeholder "to_left15")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward17")
                $ (placeholder "anchor38")
          c = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down14")
                $ call2 "Vector3_Add_floor" (placeholder "to_right15")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward18")
                $ (placeholder "anchor39")


      side_FRONT_SIDE_RIGHT_UP = Triangle a b c
        where
          a = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up14")
                $ call2 "Vector3_Add_floor" (placeholder "to_right16")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward19")
                $ (placeholder "anchor40")
          b = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up15")
                $ call2 "Vector3_Add_floor" (placeholder "to_left16")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward20")
                $ (placeholder "anchor41")
          c = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down15")
                $ call2 "Vector3_Add_floor" (placeholder "to_right17")
                $ call2 "Vector3_Add_floor" (placeholder "to_forward21")
                $ (placeholder "anchor42")

      --

      side_BACK_SIDE_LEFT_DOWN = Triangle a b c
        where
          a = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down16")
                $ call2 "Vector3_Add_floor" (placeholder "to_left17")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards14")
                $ (placeholder "anchor43")
          b = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up16")
                $ call2 "Vector3_Add_floor" (placeholder "to_left18")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards15")
                $ (placeholder "anchor44")
          c = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down17")
                $ call2 "Vector3_Add_floor" (placeholder "to_right18")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards16")
                $ (placeholder "anchor45")


      side_BACK_SIDE_RIGHT_UP = Triangle a b c
        where
          a = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up17")
                $ call2 "Vector3_Add_floor" (placeholder "to_right19")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards17")
                $ (placeholder "anchor46")
          b = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_up18")
                $ call2 "Vector3_Add_floor" (placeholder "to_left19")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards18")
                $ (placeholder "anchor47")
          c = Vector3_Expression
                $ call2 "Vector3_Add_floor" (placeholder "to_down18")
                $ call2 "Vector3_Add_floor" (placeholder "to_right20")
                $ call2 "Vector3_Add_floor" (placeholder "to_backwards19")
                $ (placeholder "anchor48")

    in id

  $ list
  $ tail [ undefined
    , deleteType referenceTriangle
    , deleteType more_directions
    , deleteType more_directions_negated

    , deleteType side_BOTTOM_BACK_LEFT
    , deleteType side_LID_BACK_LEFT

    , deleteType side_BOTTOM_FRONT_RIGHT
    , deleteType side_LID_FRONT_RIGHT


    , deleteType side_LEFT_SIDE_FRONT_DOWN
    , deleteType side_LEFT_SIDE_BACK_UP

    , deleteType side_RIGHT_SIDE_FRONT_DOWN
    , deleteType side_RIGHT_SIDE_BACK_UP

    , deleteType side_FRONT_SIDE_LEFT_DOWN
    , deleteType side_FRONT_SIDE_RIGHT_UP

    , deleteType side_BACK_SIDE_LEFT_DOWN
    , deleteType side_BACK_SIDE_RIGHT_UP
    ]





curve_simple :: UntrustedModule
curve_simple = module_ rootRule rules
  where
    rootRule = simple_curve

    rules = []

curve_slightly_more_complicated :: UntrustedModule
curve_slightly_more_complicated = module_ rootRule rules
  where
    rootRule = slightly_more_complicated_curve

    rules = []


perle_pennant :: UntrustedModule
perle_pennant = module_ rootRule rules
  where
    rootRule = pennant_interface

    rules = []
      <> vector_addition
      <> vector_negation
      <> vector_subtraction
      <> vector_halfe
      <> vector_midpoint
      <> vector_length
      <> vector_distance_between_two_points
      <> vector_scale_flipped
      <> vector_normalize
      <> vector_cross_product

perle_cube :: UntrustedModule
perle_cube = module_ rootRule rules
  where
    rootRule = station_platform_interface

    rules = []
      <> vector_addition
      <> vector_negation
      <> vector_subtraction
      <> vector_halfe
      <> vector_midpoint
      <> vector_length
      <> vector_distance_between_two_points
      <> vector_scale_flipped
      <> vector_normalize
      <> vector_cross_product



vector_addition :: [UntrustedRule]
vector_addition = [vector_addition_L, vector_addition_R]
  where
    rule_name = "Vector3_Add_floor"

    vector_addition_L
      = rule rule_name
      $ return
      $ add_vector

    add_vector :: PatternBranch
    add_vector
      = branch "Vector3" ["x0", "x1", "x2"] ["rightVector"]
      $ call (rule_name ++ "_MatchSecondParameter") (placeholder "rightVector")
      $ map placeholder ["x0", "x1", "x2"]


    vector_addition_R
      = rule "Vector3_Add_floor_MatchSecondParameter"
      $ return
      $ add_vector_R


    add_vector_R :: PatternBranch
    add_vector_R
      = branch "Vector3" ["y0", "y1", "y2"] ["x0", "x1", "x2"]
      $ vector3
          (addition_down (placeholder "x0") (placeholder "y0"))
          (addition_down (placeholder "x1") (placeholder "y1"))
          (addition_down (placeholder "x2") (placeholder "y2"))


vector_negation :: [UntrustedRule]
vector_negation = [vector_negation_rule]
  where
    vector_negation_rule
      = rule "Vector3_Negate"
      $ return
      $ negate_vector

    negate_vector :: PatternBranch
    negate_vector
      = branch "Vector3" ["x0", "x1", "x2"] []
      $ vector3
          (float_negate (placeholder "x0"))
          (float_negate (placeholder "x1"))
          (float_negate (placeholder "x2"))



vector_subtraction :: [UntrustedRule]
vector_subtraction = [vector_subtraction_rule]
  where
    vector_subtraction_rule
      = rule "Vector3_Subtract_floor"
      $ return
      $ vector_subtract

    vector_subtract :: PatternBranch
    vector_subtract
      = branch "Vector3" ["x0", "x1", "x2"] ["y"]
      $ call "Vector3_Add_floor"
          --TODO it is unfortunate that x gets pattern matched and then assembled to an vector again?
          (vector3 (placeholder "x0") (placeholder "x1") (placeholder "x2"))
          [negated_vector]
      where
        negated_vector = call "Vector3_Negate" (placeholder "y") []



vector_halfe :: [UntrustedRule]
vector_halfe = [vector_negation_rule]
  where
    vector_negation_rule
      = rule "Vector3_Halfe"
      $ return
      $ halfe_vector

    halfe_vector :: PatternBranch
    halfe_vector
      = branch "Vector3" ["x0", "x1", "x2"] []
      $ vector3
          (multiplication_down (float32 (0.5)) (placeholder "x0"))
          (multiplication_down (float32 (0.5)) (placeholder "x1"))
          (multiplication_down (float32 (0.5)) (placeholder "x2"))


vector_midpoint :: [UntrustedRule]
vector_midpoint = [vector_rule]
  where
    vector_rule
      = rule "Vector3_Midpoint_floor"
      $ return
      $ midpoint

    midpoint :: PatternBranch
    midpoint
      = branch "Vector3" ["x0", "x1", "x2"] ["y"]
      $ call "Vector3_Halfe" added_up []
      where
        added_up = call "Vector3_Add_floor" argument [placeholder "y"]

        argument = vector3 (placeholder "x0") (placeholder "x1") (placeholder "x2")



vector_length :: [UntrustedRule]
vector_length = [vector_rule]
  where
    vector_rule
      = rule "Vector3_Length_floor"
      $ return
      $ norm

    {-
      |(x,y,z)|_2 := sqrt (x^2 + y^2 + z^2)
    -}

    norm :: PatternBranch
    norm
      = branch "Vector3" ["x0", "x1", "x2"] []
      $ square_root_down
      $ addition_down (square_down $ placeholder "x0")
      $ addition_down (square_down $ placeholder "x1")
      $ (square_down $ placeholder "x2")


vector_distance_between_two_points :: [UntrustedRule]
vector_distance_between_two_points = [vector_distance]
  where
    vector_distance
      = rule "Vector3_Distance_floor"
      $ return
      $ vector_euclidian_distance

    vector_euclidian_distance :: PatternBranch
    vector_euclidian_distance
      = branch "Vector3" ["x0", "x1", "x2"] ["y"]
      $ ($ []) $ call "Vector3_Length_floor"
      $ call "Vector3_Subtract_floor"
          (vector3 (placeholder "x0") (placeholder "x1") (placeholder "x2"))
          [placeholder "y"]


-- TODO how to flip parameters?
vector_scale_flipped :: [UntrustedRule]
vector_scale_flipped = [vector_scale_rule]
  where
    vector_scale_rule
      = rule "Vector3_Scale_flipped"
      $ return
      $ scale_vector

    scale_vector :: PatternBranch
    scale_vector
      = branch "Vector3" ["x0", "x1", "x2"] ["scaleFactor"]
      $ duplicate3 "scaleFactor"
      $ vector3
          (multiplication_down (placeholder "scaleFactor1" ) (placeholder "x0"))
          (multiplication_down (placeholder "scaleFactor21") (placeholder "x1"))
          (multiplication_down (placeholder "scaleFactor22") (placeholder "x2"))


vector_normalize :: [UntrustedRule]
vector_normalize = [vector_negation_rule]
  where
    vector_negation_rule
      = rule "Vector3_Normalize_floor"
      $ return
      $ normalize_vector

    normalize_vector :: PatternBranch
    normalize_vector
      = branch "Vector3" ["x0", "x1", "x2"] []

      $ duplicate "x0_left" "x0_right" (placeholder "x0")
      $ duplicate "x1_left" "x1_right" (placeholder "x1")
      $ duplicate "x2_left" "x2_right" (placeholder "x2")

      $ call "Vector3_Scale_flipped"
          (vector3 (placeholder "x0_left") (placeholder "x1_left") (placeholder "x2_left"))
          [factor]
      where
        factor
          = reciprocal_down
          $ call "Vector3_Length_floor"
              (vector3 (placeholder "x0_right") (placeholder "x1_right") (placeholder "x2_right"))
              []


vector_cross_product :: [UntrustedRule]
vector_cross_product = [vector_L, vector_R]
  where
    rule_name = "Vector3_CrossProduct_floor"

    vector_L
      = rule rule_name
      $ return
      $ crossL

    crossL :: PatternBranch
    crossL
      = branch "Vector3" ["a1", "a2", "a3"] ["rightVector"]
      $ call "Vector3_CrossProduct_floor_MatchSecondParameter" (placeholder "rightVector")
      $ map placeholder ["a1", "a2", "a3"]

    vector_R
      = rule (rule_name ++ "_MatchSecondParameter")
      $ return
      $ crossLR

    crossLR :: PatternBranch
    crossLR
      = branch "Vector3" ["b1", "b2", "b3"] ["a1", "a2", "a3"]
      $ let
          a1 = placeholder "a1"
          a2 = placeholder "a2"
          a3 = placeholder "a3"
          b1 = placeholder "b1"
          b2 = placeholder "b2"
          b3 = placeholder "b3"
        in id
      $ duplicateLR "a1" a1
      $ duplicateLR "a2" a2
      $ duplicateLR "a3" a3
      $ duplicateLR "b1" b1
      $ duplicateLR "b2" b2
      $ duplicateLR "b3" b3
      $ let
          a1L = placeholder "a1L"
          a1R = placeholder "a1R"
          a2L = placeholder "a2L"
          a2R = placeholder "a2R"
          a3L = placeholder "a3L"
          a3R = placeholder "a3R"
          b1L = placeholder "b1L"
          b1R = placeholder "b1R"
          b2L = placeholder "b2L"
          b2R = placeholder "b2R"
          b3L = placeholder "b3L"
          b3R = placeholder "b3R"
        in id
      $ vector3
          (a2L *§ b3L  -§  a3L *§ b2L)
          (a3R *§ b1L  -§  a1L *§ b3R)
          (a1R *§ b2R  -§  a2R *§ b1R)
      where

    infixl 7 *§
    infixl 6 -§
    (-§) = subtraction_down
    (*§) = multiplication_down
