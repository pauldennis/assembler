module HandCompiled.TickTackToe where


import Assembler.Weft.UntrustedModule
import Assembler.Links.UntrustedExpression.UntrustedExpression
import TermBuildingBlocks.TermBuildingBlocks
import Assembler.Links.UntrustedExpression.WellKnownName

import TermBuildingBlocks.MagicStrings

import Assembler.Compiler
import Trifle.Test

import Data.List.NonEmpty( NonEmpty( (:|) ) )

replaceHack :: String -> String
replaceHack ('\\':'1':'9':'1':x) = '¿' : replaceHack x
replaceHack (x:xs) = x : replaceHack xs
replaceHack [] = []

toJavascripString :: UntrustedModule -> String
toJavascripString expression = string
  where
    string
      = id
      $ replaceHack
      $ show
      $ expression





----





test_compile_example_TickTackToe :: (String, Bool)
test_compile_example_TickTackToe
  = (,) "test_compile_example_TickTackToe"
  $ forceByShow
  $ compile_module
  $ tick_tack_toe3_module





---------





event_handler_capsule :: String
event_handler_capsule = toJavascripString $ expressionToModule genie_event_handler_capsule


genie_event_handler_capsule :: UntrustedExpression
genie_event_handler_capsule
  = capsule
      tuple0
      "unusedPrivateState"
      "bool"
      interface
  where
    interface
      = id
      $ intrinsic_call2
          "¿BoolNeedsToBeSetTo"
          (placeholder "bool")
      $ returnReturnPrivateDataAndObjectCallingResult
          (placeholder "unusedPrivateState")
      $ true --tuple0
      -- TODO replace by tuple0, true is used here soley for the lack of the corresponding desintegrate function





---------





genie :: String
genie = toJavascripString $ expressionToModule genie_capsule


genie_capsule :: UntrustedExpression
genie_capsule
  = capsule
      tuple0
      "unusedPrivateState"
      "bool"
      interface
  where
    interface
      = id
      $ intrinsic_call2
          "WebSocketMessageTrigger"
          (placeholder "bool")
      $ returnReturnPrivateDataAndObjectCallingResult
          (placeholder "unusedPrivateState")
      $ tuple0




----





tick_tack_toe3 :: String
tick_tack_toe3 = toJavascripString tick_tack_toe3_module


tick_tack_toe3_module
  :: UntrustedModule
tick_tack_toe3_module
  = UntrustedModule
      (RootRule
        tick_tack_toe_implementation
      )
      (Rulebook
        [ UntrustedRule
            (WellKnownName "InterfaceDispatch")
            (
              (PatternBranch
                (PatternMatch (ConstructorName (WellKnownName name_Toggle)))
                toggleInterface
              )
              :|
              [
                (PatternBranch
                  (PatternMatch (ConstructorName (WellKnownName name_SetToTrue)))
                  setTrueInterface
                )
                ,
                (PatternBranch
                  (PatternMatch (ConstructorName (WellKnownName name_SetToFalse)))
                  setFalseInterface
                )
              ]
            )
        , UntrustedRule
            (WellKnownName "MatchTwoPrivateMembers") --TODO rename to MatchTwoPrivateMembers_forToggleInterface
            (
              (PatternBranch
                (PatternMatch (ConstructorName (WellKnownName name_GenieAndBool)))
                matchTwoPrivateMembers
              )
              :|
              []
            )
        , UntrustedRule
            (WellKnownName "MatchTwoPrivateMembers_forSetTrue")
            (
              (PatternBranch
                (PatternMatch (ConstructorName (WellKnownName name_GenieAndBool)))
                matchTwoPrivateMembers_forSetTrue
              )
              :|
              []
            )
        , UntrustedRule
            (WellKnownName "MatchTwoPrivateMembers_forSetFalse")
            (
              (PatternBranch
                (PatternMatch (ConstructorName (WellKnownName name_GenieAndBool)))
                matchTwoPrivateMembers_forSetFalse
              )
              :|
              []
            )
        , UntrustedRule
            (WellKnownName "MatchTwoCapsuleCallResult") --TODO rename to MatchTwoCapsuleApplicationResult?
            (
              (PatternBranch
                (PatternMatch (ConstructorName (WellKnownName magic_CapsuleApplicationResult_String)))
                matchTwoCapsuleCallResult
              )
              :|
              []
            )
        ]
      )

tick_tack_toe_implementation :: UntrustedExpression
tick_tack_toe_implementation
  = id
  $ lambda "genie"
  $ capsule
      (constructor2 name_GenieAndBool (placeholder "genie") false)
      "oldLightSwitchState"
      "interfaceRequest"
      interface
  where
    interface
      = id
      $ apply
          (
            id
            $ call1 "InterfaceDispatch"
            $ placeholder "interfaceRequest"
          )
      $ placeholder "oldLightSwitchState"


toggleInterface :: UntrustedExpression
toggleInterface
  = id

  -- no variables put in scope since Tuple0 has no children

  $ lambda "oldLightSwitchState"

  $ call1 "MatchTwoPrivateMembers"
  $ placeholder "oldLightSwitchState"





matchTwoPrivateMembers :: UntrustedExpression
matchTwoPrivateMembers
  = id

  $ lambda "genie"
  $ lambda "bool"

  $ duplicateLR "bool" (negating $ placeholder "bool")
  $ apply
      (call1 "MatchTwoCapsuleCallResult"
        (apply
          (placeholder "genie")
          (placeholder "boolL")
        )
      )
      (placeholder "boolR")



matchTwoCapsuleCallResult :: UntrustedExpression
matchTwoCapsuleCallResult
  = id

  $ lambda "newGenie"
  $ lambda "genieCurrentlyDoesNotReturnAnything"

  $ lambda "bool"
  $ dropLeaf "genieCurrentlyDoesNotReturnAnything"
  $ duplicatePlaceholderLR "bool"
  $ returnReturnPrivateDataAndObjectCallingResult
      (constructor2 name_GenieAndBool
        (placeholder "newGenie")
        (placeholder "boolL")
      )
      (placeholder "boolR")





------





-- TODO wait for more advanced interface that allows passing bools as event payload (instead of encoding the bool in the function name)
setTrueInterface :: UntrustedExpression
setTrueInterface
  = id

  -- no variables put in scope since Tuple0 has no children

  $ lambda "oldLightSwitchState"

  $ call1 "MatchTwoPrivateMembers_forSetTrue"
  $ placeholder "oldLightSwitchState"
matchTwoPrivateMembers_forSetTrue :: UntrustedExpression
matchTwoPrivateMembers_forSetTrue
  = id

  $ lambda "genie"
  $ lambda "bool"

  $ returnReturnPrivateDataAndObjectCallingResult
      (constructor2 name_GenieAndBool
        (placeholder "genie")
        (true)
      )
      (placeholder "bool")
setFalseInterface :: UntrustedExpression
setFalseInterface
  = id

  -- no variables put in scope since Tuple0 has no children

  $ lambda "oldLightSwitchState"

  $ call1 "MatchTwoPrivateMembers_forSetFalse"
  $ placeholder "oldLightSwitchState"
matchTwoPrivateMembers_forSetFalse :: UntrustedExpression
matchTwoPrivateMembers_forSetFalse
  = id

  $ lambda "genie"
  $ lambda "bool"

  $ returnReturnPrivateDataAndObjectCallingResult
      (constructor2 name_GenieAndBool
        (placeholder "genie")
        (false)
      )
      (placeholder "bool")

