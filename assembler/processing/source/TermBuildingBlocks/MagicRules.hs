module TermBuildingBlocks.MagicRules where

import Numeric.Natural

-- | Every Rule that the runtime knows of is to be prefixed inorder to indicate that you have to look into the source code inorder to find out what the rule is actually doing.
magic_prefix_indicator :: String
magic_prefix_indicator = "¿"



--



magic_Encapsulate_String :: String
magic_Encapsulate_String = "¿Encapsulate"

magic_If_String :: String
magic_If_String = "¿If"

magic_DropNewStateAndJustUseCapsuleCallResult_String :: String
magic_DropNewStateAndJustUseCapsuleCallResult_String = "¿DropNewStateAndJustUseCapsuleCallResult"

magic_DropLeftNullaryConstructorArgumentAndReturnRightArgument_String :: String
magic_DropLeftNullaryConstructorArgumentAndReturnRightArgument_String = "¿DropLeftNullaryConstructorArgumentAndReturnRightArgument"



--


-- TODO eb5a51a60f82abdd0cff09ab1f849fd3
arityAndMagicRulesList :: [(Natural, String)]
arityAndMagicRulesList
  = id
  $ map (\(x,y)->(x,"¿"++y))
  $ tail
  [ undefined

  -- TODO SYNC b5ea6ec776affa300348e7f1be32558c8aaea759544124ae3ec6d9f1a97f28dd
  , (2, tail magic_Encapsulate_String)
  , (3, tail magic_If_String)
  , (2, tail magic_DropLeftNullaryConstructorArgumentAndReturnRightArgument_String)
  , (1, "Float32_Square_RoundingDown")
  , (2, "Float32_Addition_RoundingDown")
  , (2, "Float32_IsLessThan")
  , (2, "BoolNeedsToBeSetTo")
  , (2, "Float32_Multiplicate_RoundingDown")

  -- TODO properly prepare some generated code that can be properly used by the implementation
  , (1, "Float32_Negate")
  , (1, "Float32_Reciprocal_RoundingDown")
      -- TODO magic rule identificatin 4e1556de4aa9da14bcd1ec814df3756d95820fc87906bd9236e65323eed77bc4
  , (1, "Float32_SquareRoot_RoundingDown")
      -- TODO magic rule identificatin 4e1556de4aa9da14bcd1ec814df3756d95820fc87906bd9236e65323eed77bc4

  --


  , (1, "place_holder5")
  , (1, "place_holder6")
  , (1, "place_holder7")
  , (1, "place_holder8")
  , (1, "place_holder9")
  , (1, "place_holder10")
  , (1, "place_holder11")
  , (1, "place_holder12")
  , (1, "place_holder13")

  ]
arityAndHardCodedRulesList :: [(Natural, String)]
arityAndHardCodedRulesList
  = id
  $ map (\(x,y)->(x,y))
  $ tail
  $ (++)
  [ undefined

  -- TODO why is there no ¿ here?
    -- since these could be user provided?
    -- What should the meaning of '¿' be? Magically implemented? In that case every user should be able to define ¿-functions.
    -- maybe they should just mean that usually the runtime implements them itself and that overriding this is kinda lowlevel?
  , (webSocketMessageTrigger_arity, "WebSocketMessageTrigger")
      -- TODO currently we do not have some side_quest function like `seq` in haskell. Therefore the combinard variant
  , (webSocketMessageTrigger_arity, "WebSocketMessageTrigger_InternalUsedRuleId_that_is_different_to_user_used_rule")
      -- TODO have proper sandboxed capabilites with proper management

  ]
  $ map ((,) the_currently_only_allowed_arity_of_user_provided_rules)
  $ tail
  [ undefined

  , ("IsWithinUnitBall")

  , ("MatchTwoPrivateMembers")
  , ("MatchTwoCapsuleCallResult")

  , ("InterfaceDispatch")
  , ("MatchTwoPrivateMembers_forSetTrue")
  , ("MatchTwoPrivateMembers_forSetFalse")


  , ("Vector3_Add_floor")
  , ("Vector3_Add_floor_MatchSecondParameter")
  , ("Vector3_Negate")
  , ("Vector3_Subtract_floor")
  , ("Vector3_Halfe")
  , ("Vector3_Midpoint_floor")
  , ("Vector3_Length_floor")
  , ("Vector3_Distance_floor")
  , ("Vector3_Scale_flipped")
  , ("Vector3_Normalize_floor")
  , ("Vector3_CrossProduct_floor")
  , ("Vector3_CrossProduct_floor_MatchSecondParameter")

  ]
  where
    webSocketMessageTrigger_arity = 2

    -- TODO don't forget to check this requirement in the verificator later when this list is not hardcoded
    the_currently_only_allowed_arity_of_user_provided_rules = 1



lookupRuleName
  :: Natural
  -> Maybe String
lookupRuleName
  number
  = lookup number
  $ zip [0..]
  $ map snd
  $ arityAndMagicRulesList
  ++ arityAndHardCodedRulesList

