module PortingLadder.LightSwitch where


import Assembler.Links.UntrustedExpression.UntrustedExpression
import TermBuildingBlocks.TermBuildingBlocks
import Assembler.Links.UntrustedExpression.WellKnownName
import Assembler.Links.UntrustedExpression.IsItARuleOrConstructor



lightSwitch :: UntrustedExpression
lightSwitch = result
  where
    privateData = "privateData"
    ignoredArguments = "ignoredArguments"

    result = encapsulate false interface

    interface
      = lambda privateData
      $ lambda ignoredArguments
      $ dropNullaryConstructor (placeholder ignoredArguments)
      $ parameterizedReturnTuple

    dropNullaryConstructor
      toBeDroppedTerm
      returnTerm
      = (Comb Rule
          (WellKnownName "¿DropLeftNullaryConstructorArgumentAndReturnRightArgument")
          [toBeDroppedTerm, returnTerm]
        )

    parameterizedReturnTuple
      = tee_for_returning privateData
      $ returnReturnPrivateDataAndObjectCallingResult
          newState
          return_value

    tee_for_returning whatIsToDuplicated
      = DuplicationIn
          (WellKnownName (copy_1 whatIsToDuplicated))
          (WellKnownName (copy_2 whatIsToDuplicated))
          (ToBeDuplicated $ placeholder whatIsToDuplicated)

    copy_1 whatIsToDuplicated = whatIsToDuplicated ++ "0"
    copy_2 whatIsToDuplicated = whatIsToDuplicated ++ "1"

    newState
      = when
          (placeholder (copy_1 privateData))
          false
          true

    return_value
      = placeholder (copy_2 privateData)
