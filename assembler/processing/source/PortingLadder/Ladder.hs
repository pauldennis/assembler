module PortingLadder.Ladder where

import Data.List

import TermBuildingBlocks.TermBuildingBlocks
import Assembler.Links.UntrustedExpression.UntrustedExpression
import Assembler.Links.UntrustedExpression.WellKnownName

import PortingLadder.Rungs

import PortingLadder.Latch
import PortingLadder.LightSwitch




ledderRungs :: [PortingRung]
ledderRungs = tail $
  [ undefined
  , driver_0_LeafConstructor
  , driver_1_OneTimeDubplicate
  , driver_2_IdentityLambda
  , driver_3_NestedLambda -- TODO might be on the same level as driver_2_IdentityLambda
  , driver_4_multiple_nested_lambdas
  -- TODO let x,y := Tuple2 Tuple0 Tuple0 in Tuple x y
  -- TODO 1
  -- TODO (1+2)
  -- TODO let x,y := 1 in x + y
  , driver_5_multiple_time_duplicate
  , driver_6_duplicate_identity_lambda
  , driver_7_dubplicate_nested_lambda
  , driver_8
  , driver_9
  , driver_10
  , driver_11
--   , driver_12_shadowingVariables -- TODO support me?
  , driver_13
  , driver_14_latching_bool
  , driver_15_light_switch
  ]



driver_0_LeafConstructor :: PortingRung
driver_0_LeafConstructor
  = result
  where
    result
      = PortingRung
          (PortingRungName "LeafConstructor")
          (InputExample input)
          (SupposedToBeOutPut want)

    input = leaf "EmptyList"

    want = input




driver_1_OneTimeDubplicate :: PortingRung
driver_1_OneTimeDubplicate
  = result
  where
    result
      = PortingRung
          (PortingRungName "OneTimeDubplicate")
          (InputExample input)
          (SupposedToBeOutPut want)

    want
      = tuple2
          (leaf "EmptyList")
          (leaf "EmptyList")

    input :: UntrustedExpression
    input = result2
      where
        result2
          = DuplicationIn
              x_left
              x_right
              (ToBeDuplicated emptyList)
              pair

        x_left = WellKnownName "x0"
        x_right = WellKnownName "x1"

        pair
          = tuple2
              (PlaceholderVariable x_left)
              (PlaceholderVariable x_right)




driver_2_IdentityLambda :: PortingRung
driver_2_IdentityLambda
  = result
  where
    result
      = PortingRung
          (PortingRungName "IdentityLambda")
          (InputExample input)
          (SupposedToBeOutPut want)

    want = leaf "EmptyList"

    -- (λx(x)) EmptyList
    input = apply identityLambda (leaf "EmptyList")



driver_3_NestedLambda :: PortingRung
driver_3_NestedLambda
  = result
  where
    result
      = PortingRung
          (PortingRungName "NestedLambda")
          (InputExample input)
          (SupposedToBeOutPut want)

    want = tuple2 (leaf "A") (leaf "B")

    -- (λxλy(Tuple2 x y) True False)
    -- ((x |-> (y |-> Tuple2 x y)) 2) 3
    -- (x |-> y |-> Tuple2 x y) 2 3
    input = apply (apply outer (leaf "A")) (leaf "B")
      where
        x = "x"
        y = "y"

        outer = lambda x $ inner
        inner = lambda y $ tuple2 (placeholder x) (placeholder y)






driver_4_multiple_nested_lambdas :: PortingRung
driver_4_multiple_nested_lambdas
  = result
  where
    result
      = PortingRung
          (PortingRungName "MultipleNestedLambdas")
          (InputExample input)
          (SupposedToBeOutPut want)

    want = constructor tupleName $ map leaf $ reverse numbers

    chain_length = 5

    tupleName = "Tuple" ++ show chain_length

    -- (λxλy...(Pair x y ...) 2 3 ...)
    input = apply_chain
    numbers = map (\x -> [x]) $ take chain_length ['A'..]
    leafs = map leaf numbers

    apply_chain = apply_chaining lambda_chain

    applies = map (flip apply) leafs

    apply_chaining = foldr (.) id applies

    lambda_chain = lambda_chaining tuple

    lambda_chaining = foldr (.) id lambdas

    lambdas = map lambda variables

    tuple = constructor tupleName children
    children = map placeholder variables
    variables :: [String]
    variables
      = take chain_length
      $ map return
      $ ['a'..]







driver_5_multiple_time_duplicate :: PortingRung
driver_5_multiple_time_duplicate
  = result
  where
    n :: Integer
--     n = 7
--     n = 5
    n = 5

    result
      = PortingRung
          (PortingRungName "MultipleTimeDuplicate")
          (InputExample input)
          (SupposedToBeOutPut want)

    want
      = tuple2
          suposed_to_be_copied_term
          suposed_to_be_copied_term
      where
        suposed_to_be_copied_term
          = list
          $ map IntrinsicNumber
          $ map (\x -> x+x)
          $ reverse
          $ genericTake n
          $ [1..]

    input :: UntrustedExpression
    input = result2
      where

        result2 = DuplicationIn x_left x_right (ToBeDuplicated $ exampleList n) ( pair)

        pair
          = tuple2
              (PlaceholderVariable x_left)
              (PlaceholderVariable x_right)

        x_left = WellKnownName "x0"
        x_right = WellKnownName "x1"

        n_plus_n :: Integer -> UntrustedExpression
        n_plus_n number = IntrinsicOperator (word32 number) (word32 number)


        exampleList the_length
          = id
          $ list
          $ map n_plus_n
          $ reverse
          $ [1..the_length]






driver_6_duplicate_identity_lambda :: PortingRung
driver_6_duplicate_identity_lambda
  = result
  where
    result
      = PortingRung
          (PortingRungName "DuplicateIdentityLambda")
          (InputExample input)
          (SupposedToBeOutPut want)

    want
      = tuple2
          identityLambda
          identityLambda

    -- let a,b := (x |-> x) in Tuple2 a b
    input :: UntrustedExpression
    input = the_result
      where
        the_result
          = DuplicationIn
              x_left
              x_right
              (ToBeDuplicated $ identityLambda)
              pair

        pair
          = tuple2
              (PlaceholderVariable x_left)
              (PlaceholderVariable x_right)

        x_left = WellKnownName "x0"
        x_right = WellKnownName "x1"



driver_7_dubplicate_nested_lambda :: PortingRung
driver_7_dubplicate_nested_lambda
  = result
  where
    result
      = PortingRung
          (PortingRungName "DubplicateNestedLambda")
          (InputExample input)
          (SupposedToBeOutPut want)

    want = tuple2 outer outer

    input :: UntrustedExpression
    input
      = DuplicationIn
          x_left
          x_right
          (ToBeDuplicated $ outer)
          pair

    x = "x"
    y = "y"


    pair
      = tuple2
          (PlaceholderVariable x_left)
          (PlaceholderVariable x_right)

    x_left = WellKnownName "x0"
    x_right = WellKnownName "x1"

    outer = lambda x $ inner
    inner = lambda y $ tuple2 (placeholder x) (placeholder y)







-- TODO rempove postfix of all drivers
driver_8 :: PortingRung
driver_8
  = result
  where
    chain_length = 8

    result
      = PortingRung
          (PortingRungName "DubplicateMultipleNestedLambda")
          (InputExample input)
          (SupposedToBeOutPut want)

    want = tuple2 lambda_chain lambda_chain

    input :: UntrustedExpression
    input
      = DuplicationIn
          x_left
          x_right
          (ToBeDuplicated $ lambda_chain)
          pair

    pair
      = tuple2
          (PlaceholderVariable x_left)
          (PlaceholderVariable x_right)

    x_left = WellKnownName "x0"
    x_right = WellKnownName "x1"

    lambda_chain = lambda_chaining tuple

    lambda_chaining = foldr (.) id lambdas

    lambdas = map lambda variables

    tupleName = "Tuple" ++ show chain_length

    tuple = constructor tupleName children
    children = map placeholder variables
    variables :: [String]
    variables
      = take chain_length
      $ map return
      $ ['a'..]









driver_9 :: PortingRung
driver_9
  = result
  where
    result
      = PortingRung
          (PortingRungName "DoublingFunction")
          (InputExample input)
          (SupposedToBeOutPut want)

    want = IntrinsicNumber 6

    input :: UntrustedExpression
    input = result2
      where
        x = "x"

        result2 = apply double (IntrinsicNumber 3)

        double = lambda x double_expression_of

        double_expression_of
          = DuplicationIn
              (WellKnownName "x1")
              (WellKnownName "x2")
              (ToBeDuplicated $ PlaceholderVariable $ WellKnownName x)
              (IntrinsicOperator
                (PlaceholderVariable $ WellKnownName "x1")
                (PlaceholderVariable $ WellKnownName "x2")
            )





driver_10 :: PortingRung
driver_10
  = result
  where
    result
      = PortingRung
          (PortingRungName "SharedComputationInLambda")
          (InputExample input)
          (SupposedToBeOutPut want)

    want
      = binaryConstrucor "Tuple2"
          (tuple2 (IntrinsicNumber 4) (IntrinsicNumber 10))
          (tuple2 (IntrinsicNumber 4) (IntrinsicNumber 20))

    input :: UntrustedExpression
    input = result2
      where
        x = "x"
        y = "y"

        x_left = WellKnownName "f"
        x_right = WellKnownName "g"

        result2
          = DuplicationIn
              x_left
              x_right
              (ToBeDuplicated $ apply outer (IntrinsicNumber 2))
              pair

        pair
          = binaryConstrucor "Tuple2"
              (apply (PlaceholderVariable x_left) (IntrinsicNumber 10))
              (apply (PlaceholderVariable x_right) (IntrinsicNumber 20))

        outer = lambda x inner

        inner
          = lambda y
          $ tuple2
              double
              (PlaceholderVariable $ WellKnownName y)

        double
          = DuplicationIn
              (WellKnownName "x1")
              (WellKnownName "x2")
              (ToBeDuplicated $ PlaceholderVariable $ WellKnownName "x")
              (IntrinsicOperator
                (PlaceholderVariable $ WellKnownName "x1")
                (PlaceholderVariable $ WellKnownName "x2")
              )



driver_11 :: PortingRung
driver_11
  = result
  where
    result
      = PortingRung
          (PortingRungName "CorrolaryApplicationAsChild")
          (InputExample input)
          (SupposedToBeOutPut want)

    want = wrapper "Tuple1" $ leaf "EmptyList"

    input :: UntrustedExpression
    input = wrapper "Tuple1" $ apply identityLambda $ leaf "EmptyList"


-- TODO this testcase is currently not supported
     -- TODO why?
driver_12_shadowingVariables :: PortingRung
driver_12_shadowingVariables
  = result
  where
    result
      = PortingRung
          (PortingRungName "ShadowingVariable")
          (InputExample input)
          (SupposedToBeOutPut want)

    -- ((x -> (x-> x)) 1) 0
    want = word32 0

    input :: UntrustedExpression
    input
      = apply (apply constOfidentityLambda (word32 1)) (word32 0)

    constOfidentityLambda = lambda "x" identityLambda


driver_13 :: PortingRung
driver_13
  = result
  where
    result
      = PortingRung
          (PortingRungName "IdentityLambdaWithUnusedState")
          (InputExample input)
          (SupposedToBeOutPut want)

    want = tuple0

    input :: UntrustedExpression
    input
      = apply trivialStateIdentity tuple0

    trivialStateIdentity :: UntrustedExpression
    trivialStateIdentity = encapsulate false interface
      where
        privateData = "privateData"
        argument = "argument"

        interface
          = lambda privateData
          $ lambda argument
          $ parameterizedReturnTuple

        parameterizedReturnTuple
          = returnReturnPrivateDataAndObjectCallingResult
              (placeholder privateData)
              (placeholder argument)



driver_14_latching_bool :: PortingRung
driver_14_latching_bool
  = result
  where
    result
      = PortingRung
          (PortingRungName "LatchingBool")
          (InputExample input)
          (SupposedToBeOutPut want)

    want = tuple0

    input :: UntrustedExpression
    input = latching_bool



driver_15_light_switch :: PortingRung
driver_15_light_switch
  = result
  where
    result
      = PortingRung
          (PortingRungName "LightSwitch")
          (InputExample input)
          (SupposedToBeOutPut want)

    want = tuple0

    input :: UntrustedExpression
    input = lightSwitch

