module PortingLadder.Latch where

import Assembler.Links.UntrustedExpression.UntrustedExpression
import TermBuildingBlocks.TermBuildingBlocks



latching_bool :: UntrustedExpression
latching_bool = result
  where
    privateData = "privateData"
    ignoredArguments = "ignoredArguments"

    result = encapsulate false interface

    interface
      = lambda privateData
      $ lambda ignoredArguments
      $ dropNullaryConstructor (placeholder ignoredArguments)
      $ parameterizedReturnTuple

    dropNullaryConstructor
      toBeDroppedTerm
      returnTerm
      = intrinsic_call2 "¿DropLeftNullaryConstructorArgumentAndReturnRightArgument"
          toBeDroppedTerm
          returnTerm

    parameterizedReturnTuple
      = returnReturnPrivateDataAndObjectCallingResult
          true
          (placeholder privateData)
