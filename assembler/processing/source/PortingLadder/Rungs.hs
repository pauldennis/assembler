module PortingLadder.Rungs where

import Data.Tree

import Trifle.Ramificable

import Assembler.Links.UntrustedExpression.UntrustedExpression


newtype InputExample = InputExample UntrustedExpression

instance Ramificable InputExample where
  ramify (InputExample x)
    = Node "{InputExample}" [ramify x]


newtype SupposedToBeOutPut = SupposedToBeOutPut UntrustedExpression

instance Ramificable SupposedToBeOutPut where
  ramify (SupposedToBeOutPut x)
    = Node "{SupposedToBeOutPut}" [ramify x]


newtype PortingRungName
  = PortingRungName
      String

instance Ramificable PortingRungName where
  ramify (PortingRungName string)
    = Node "{PortingRungName}" [return string]


data PortingRung
  = PortingRung
      PortingRungName
      InputExample
      SupposedToBeOutPut

instance Ramificable PortingRung where
  ramify (PortingRung portingRungName inputExample supposedToBeOutPut)
    = Node "{PortingRung}"
    [ ramify portingRungName
    , ramify inputExample
    , ramify supposedToBeOutPut
    ]

instance Show PortingRung where
  show x = showRamificable x

extractInput :: PortingRung -> UntrustedExpression
extractInput (PortingRung _ (InputExample input) _) = input

extractName :: PortingRung -> String
extractName (PortingRung (PortingRungName name) _ _) = name

---

-- where to put?
data MountingPortingRung
  = ClouldMount
  | OutputIsSupposedToBe_x_but_got_x
      UntrustedExpression
      UntrustedExpression

instance Ramificable MountingPortingRung where
  ramify ClouldMount = return "{ClouldMount}"
  ramify (OutputIsSupposedToBe_x_but_got_x x y)
    = Node "{OutputIsSupposedToBe_x_but_got_x}"
    [ ramify x
    , ramify y
    ]



