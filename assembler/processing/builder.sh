echo "START###############################################"

source $stdenv/setup

function safe_copy()
{
  # TODO this approach feels wrong. How to do this properly?

  rsync $1 $2 --no-owner --no-p --chmod=ugo=rwX -r --ignore-existing --info=skip2 1> sync_log_1 2> sync_log_2 || true

  # try to detect conflicting files
  if [ -s sync_log_1 ] || [ -s sync_log_2 ]; then
    echo "rsync log has something in the its output"
    cat sync_log_1
    cat sync_log_2
    exit 17
  fi
}





temporary=$TMPDIR

testSuiteTesting="testSuiteTesting"
precompileExportPrepare="precompileExportPrepare"
# TODO maybe
assembler_output="assembler_libraries"


mkdir $temporary -p
mkdir $temporary/$testSuiteTesting -p
mkdir $temporary/$precompileExportPrepare -p


echo "testing"
safe_copy $source/ $temporary/$testSuiteTesting/

pushd $testSuiteTesting
  runhaskell --ghc-arg="-main-is" --ghc-arg="mainTestSuite" TestSuite.hs
popd



echo "precompiling"
safe_copy $source/ $temporary/$precompileExportPrepare

pushd $precompileExportPrepare
  ghc SelectivePrecompileExport.hs
popd

mkdir -p $out/

safe_copy $temporary/$precompileExportPrepare/ $out/$assembler_output/



echo "stonefish injection code"
mkdir -p $out/webassemblyInitialisationCode/

pushd $testSuiteTesting
  runhaskell --ghc-arg="-main-is" --ghc-arg="printInitialisationCode" DevelopStonefish.hs > $out/webassemblyInitialisationCode/memoryInitialisation.wat
popd





echo "runtime develop example"
mkdir -p $out/embeddingDevelopCase/

pushd $testSuiteTesting
  runhaskell --ghc-arg="-main-is" --ghc-arg="pointcloud_filter_root" DevelopStonefish.hs > $out/embeddingDevelopCase/pointcloud_filter_root.inc
  runhaskell --ghc-arg="-main-is" --ghc-arg="pointcloud_filter_melee" DevelopStonefish.hs > $out/embeddingDevelopCase/pointcloud_filter_melee.inc

  runhaskell --ghc-arg="-main-is" --ghc-arg="curve_simple_module" DevelopStonefish.hs > $out/embeddingDevelopCase/curve_simple.read.fuchs
  runhaskell --ghc-arg="-main-is" --ghc-arg="curve_slightly_more_complicated_module" DevelopStonefish.hs > $out/embeddingDevelopCase/curve_slightly_more_complicated.read.fuchs

  runhaskell --ghc-arg="-main-is" --ghc-arg="perle_pennant_module" DevelopStonefish.hs > $out/embeddingDevelopCase/perle_pennant.read.fuchs
  runhaskell --ghc-arg="-main-is" --ghc-arg="perle_cube_module" DevelopStonefish.hs > $out/embeddingDevelopCase/perle_cube.read.fuchs
popd









echo "verify_module"
mkdir -p $out/verify_module/

pushd $testSuiteTesting
  ls -al

  ghc \
    DevelopStonefish.hs \
    -main-is DevelopStonefish.verify_module \
    -o $out/verify_module/verify_module \

popd





echo "STOP################################################"
