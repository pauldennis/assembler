{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}

, stdenv ? pkgs.stdenv

, ghc ? import ./glasgowHaskellCompiler.nix {}

, rsync ? pkgs.rsync
}:


stdenv.mkDerivation {
  name = "local_interchangeable_capsule_assembler";
  builder = ./builder.sh;

  source = ./source;

  buildInputs = [
    ghc
    rsync
  ];

  # excruciating unicode and whatnot
  LOCALE_ARCHIVE="${pkgs.glibcLocales}/lib/locale/locale-archive";
  LANG="en_GB.UTF8";
}
