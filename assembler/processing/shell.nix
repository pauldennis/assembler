{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}
, glibcLocales ? pkgs.glibcLocales
, ghc ? import ./glasgowHaskellCompiler.nix {}
}:

pkgs.mkShell {
  buildInputs = [
    ghc
    pkgs.eza
  ];

  shellHook = ''
    echo this is a nix shell; alias l="ls -al"
    cd source
    ghci -fdefer-type-errors
  '';

  # excruciating unicode and whatnot
  LOCALE_ARCHIVE="${pkgs.glibcLocales}/lib/locale/locale-archive";
  LANG="en_GB.UTF8";

  MY_ENVIRONMENT_VARIABLE = "world";
}
